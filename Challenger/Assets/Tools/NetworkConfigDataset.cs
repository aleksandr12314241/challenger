﻿using UnityEngine;

[CreateAssetMenu( fileName = "ConfigDataset", menuName = "Challenger/ConfigDataset", order = 1 )]
public class NetworkConfigDataset : ScriptableObject {
    [SerializeField]
    int indexNetworkConfig;
    [SerializeField]
    NetworkConfig[] networkConfig;
    [SerializeField]
    MappingStringByEnum[] mappingStringByEnums;

    internal NetworkConfig GetNetworkConfig () {
        return networkConfig[indexNetworkConfig];
    }

    internal MappingStringByEnum[] GetMappingStringByEnums () {
        return mappingStringByEnums;
    }

    [System.Serializable]
    internal class NetworkConfig {
        [SerializeField]
        private string url;
        [SerializeField]
        private string uploadPath;

        public string GetUrl () {
            return url;
        }
        public string GetUploadPath () {
            return uploadPath;
        }
    }

    [System.Serializable]
    internal class MappingStringByEnum {
        [SerializeField]
        private MappingEnums mappingEnum;
        [SerializeField]
        private string mappingString;

        public MappingEnums GetMappingEnum () {
            return mappingEnum;
        }
        public string GetMappingString () {
            return mappingString;
        }
    }
}
