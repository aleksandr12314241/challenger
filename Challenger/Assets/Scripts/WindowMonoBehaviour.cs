﻿using UnityEngine;
using UnityEngine.UI;

public class WindowMonoBehaviour : MonoBehaviour {
    [SerializeField]
    protected RectTransform contentRectTransform;
    [SerializeField]
    protected Image backBlockImage;

    protected float timeShowWindow = 0.15f;
    protected Vector2 sizeDeltaScreen;

    protected bool isShow;

    protected void Init() {
        Canvas mainCanvas = FindObjectOfType<Canvas>();
        RectTransform canvasRectTransform = mainCanvas.GetComponent<RectTransform>();
        sizeDeltaScreen = new Vector2( canvasRectTransform.rect.width, canvasRectTransform.rect.height );
    }

    internal virtual void StartShow() {
        Show();
    }
    internal void Show() {
        isShow = true;
        contentRectTransform.gameObject.SetActive( isShow );
        backBlockImage.gameObject.SetActive( isShow );
    }

    internal void Hide() {
        isShow = false;
        contentRectTransform.gameObject.SetActive( isShow );
        backBlockImage.gameObject.SetActive( isShow );
    }
    internal virtual void StartHide() {
        Hide();
    }
}
