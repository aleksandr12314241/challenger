﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class ChallengeFriendWindow : WindowMonoBehaviour {
    public event Action OnCancelClcik = () => { };
    public event Action OnAcceptClcik = () => { };

    [SerializeField]
    Button acceptButton;
    [SerializeField]
    Button cancelButton;

    void Start () {
        Init();

        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, 0 );
        timeShowWindow = 0.15f;

        cancelButton.onClick.AddListener( CancelClcik );
        acceptButton.onClick.AddListener( AcceptClcik );

        base.StartHide();
    }

    internal override void StartShow () {
        base.StartShow();
        backBlockImage.gameObject.SetActive( true );

        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );
    }


    internal override void StartHide () {

        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            base.StartHide();
            backBlockImage.gameObject.SetActive( false );
        } );
    }

    void CancelClcik () {
        StartHide();
        OnCancelClcik();
    }

    void AcceptClcik () {
        StartHide();
        OnAcceptClcik();
    }
}
