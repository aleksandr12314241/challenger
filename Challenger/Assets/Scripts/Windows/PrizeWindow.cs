﻿using UnityEngine;
using UnityEngine.UI;

public class PrizeWindow : WindowMonoBehaviour {
    [SerializeField]
    Button backBlockButton;

    void Start() {
        backBlockButton.onClick.AddListener( StartHide );

        StartHide();
    }
}
