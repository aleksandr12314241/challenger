﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class LoginWindow : WindowMonoBehaviour {
    [SerializeField]
    InputField usernameInputField;
    [SerializeField]
    InputField passwordInputField;
    [SerializeField]
    Button loginButton;
    [SerializeField]
    Button registrationWindowButton;
    [SerializeField]
    GameObject errorPanel;
    [SerializeField]
    Text errortext;

    [Inject]
    RegistrationWindow registrationWindow;
    [Inject]
    SignalBus signalBus;
    [Inject]
    NetworkController networkController;

    void Start() {
        contentRectTransform.gameObject.SetActive( true );
        backBlockImage.gameObject.SetActive( true );

        loginButton.onClick.AddListener( Login );
        registrationWindowButton.onClick.AddListener( ShowRegistrationWindow );

        HideErrorPanel();
    }

    public void AutoLogin( OnAutoLogin onAutoLogin ) {
        usernameInputField.text = onAutoLogin.username;
        usernameInputField.text = onAutoLogin.password;

        LoginAction();
    }

    public void LoginAfterRegistration( OnRegistrationSucces onRegistrationSucces ) {
        usernameInputField.text = onRegistrationSucces.username;
        passwordInputField.text = onRegistrationSucces.password;

        LoginAction();
    }

    void Login() {
        LoginAction();
    }

    public void LoginAction() {
        if( usernameInputField.text == "" || usernameInputField.text == "" ) {
            ShowErrorPanel( "введите имя пользователя и пароль" );
            return;
        }

        HideErrorPanel();

        LoginRequestDto loginRequestDto = new LoginRequestDto() {
            username = usernameInputField.text,
            password = passwordInputField.text
        };

        networkController.Post( "api/v1/auth/login", loginRequestDto,
        action: ( requese ) => {
            ResponseEntity responseEntity = JsonUtility.FromJson<ResponseEntity>( requese );

            signalBus.Fire( new OnLoginSuccess() {
                token = responseEntity.token
            } );

            signalBus.Fire( new OnSetProfile() {
                id = responseEntity.id,
                username = responseEntity.username,
                token = responseEntity.token
            } );
        },
        actionError: ( responseError ) => {
            ShowErrorPanel( "неверный логин или пароль" );
        } );
    }

    void ShowRegistrationWindow() {
        registrationWindow.StartShow();
    }

    void ShowErrorPanel( string text ) {
        errorPanel.gameObject.SetActive( true );
        errortext.text = text;
    }

    void HideErrorPanel() {
        errorPanel.gameObject.SetActive( false );
    }

    public void StartSurfChallenger() {
        StartHide();
    }
}
