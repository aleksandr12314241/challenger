﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class PhotoEditorWindow : WindowMonoBehaviour {
    internal Action<bool, float, float> OnShowMoreStikerClick = ( bool isShow, float time, float offsetHeight ) => { };
    internal Action<bool> OnChoosePhoto = ( bool isChoosePhoto ) => { };
    internal Action<StickerOnPhotoFieldEditor[], Sprite> OnHideWindow = ( StickerOnPhotoFieldEditor[] arg1, Sprite arg2 ) => { };

    [SerializeField]
    Button backButton;
    [SerializeField]
    Button changeAddPhotoButton;
    [SerializeField]
    Button inShopStickerButton;
    [SerializeField]
    Image frameButtonOutlone;
    [SerializeField]
    GameObject photoMaskGameObject;
    [SerializeField]
    Image photoImage;
    [SerializeField]
    PhotoField photoField;
    [SerializeField]
    TrashField trashField;
    [SerializeField]
    RectTransform dragStickerParentContainer;
    [SerializeField]
    DragSticker dragStickerPrf;
    [SerializeField]
    RectTransform upperDragContainer;
    [SerializeField]
    Button[] stickerTabButtons;

    RectTransform inShopStickerRectTransform;

    ChoosePhotoAndVideoWindow choosePhotoAndVideoWindow;
    List<DragSticker> dragStickers;
    StickerOnPhotoFieldEditor[] stickersOnPhotoFieldEditor;

    StickerDataSet stickerDataSet;
    string [][] stickersId;

    bool isShowMoreStickers;
    float stickerScrollViewsHeight;
    float inShopStickerPosY;
    float timeAnimSticker;

    bool isOverOnPhotoField;
    bool isOverTrash;

    int currentStikersIndex = 0;

    List<PhotoEditorStickerData> photoEditorStickerDatas;

    private void Start() {
        InitS();
    }

    private void OnDestroy() {
        for( int i = 0; i < dragStickers.Count; i++ ) {
            dragStickers[i].OnBeginDragAction -= BeginDragAction;
            dragStickers[i].OnEndDragAction -= EndDragAction;
        }
    }

    internal void InitS() {
        base.Init();

        choosePhotoAndVideoWindow = FindObjectOfType<ChoosePhotoAndVideoWindow>();
        stickerDataSet = FindObjectOfType<MenuController>().StickerDataSet;
        inShopStickerRectTransform = inShopStickerButton.GetComponent<RectTransform>();

        stickersOnPhotoFieldEditor = new StickerOnPhotoFieldEditor[10];

        changeAddPhotoButton.onClick.AddListener( ShowChoosePhotoAndVideoWindow );

        stickerScrollViewsHeight = 160f;
        inShopStickerPosY = inShopStickerRectTransform.anchoredPosition.y;
        timeAnimSticker = 0.20f;

        photoField.OnMouseEnterAction += MouseEnterStickAction;
        photoField.OnMouseExitAction += MouseExitStickAction;

        trashField.OnMouseEnterAction += MouseEnterThrowSticker;
        trashField.OnMouseExitAction += MouseExitThrowSticker;
        trashField.HideTrashField();

        //dragSticker.OnEndDragAction += EndDragAction;

        timeShowWindow = 0.15f;
        //timeAnimSV = 0.2f;
        //timeShowExecutorsPanel = 0.2f;

        backButton.onClick.AddListener( StartHide );

        backBlockImage.gameObject.SetActive( false );
        backButton.gameObject.SetActive( false );

        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, 0 );

        for( int i = 0; i < stickerTabButtons.Length; i++ ) {
            int buttonIndexTemp = i;

            stickerTabButtons[i].onClick.AddListener( () => {
                SetCurrentStickeIndex( buttonIndexTemp );
            } );
        }

        dragStickers = new List<DragSticker>();
        stickersId = new string[4][];
        stickersId[0] = new string[14] {
            "sticker_picture_0", "sticker_picture_1", "sticker_picture_2", "sticker_picture_3", "sticker_picture_4",
            "sticker_text_0", "sticker_text_1", "sticker_text_2", "sticker_text_3", "sticker_text_4", "sticker_text_5",
            "sticker_interactive_0", "sticker_interactive_1", "sticker_interactive_2"
        };
        stickersId[1] = new string[5] {
            "sticker_picture_0", "sticker_picture_1", "sticker_picture_2", "sticker_picture_3", "sticker_picture_4"
        };
        stickersId[2] = new string[6] {
            "sticker_text_0", "sticker_text_1", "sticker_text_2", "sticker_text_3", "sticker_text_4", "sticker_text_5",
        };
        stickersId[3] = new string[3] {
            "sticker_interactive_0", "sticker_interactive_1", "sticker_interactive_2"
        };

        frameButtonOutlone.gameObject.SetActive( true );
        photoMaskGameObject.gameObject.SetActive( false );
        photoImage.sprite = null;

        base.StartHide();
    }

    internal override void StartShow() {
        base.StartShow();

        backButton.transform.SetAsLastSibling();

        backButton.gameObject.SetActive( true );
        backBlockImage.gameObject.SetActive( true );
        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );

        CreateStikers();
        ShowCurrentStickeIndex();

        photoEditorStickerDatas = new List<PhotoEditorStickerData>();
    }

    void CreateStikers() {
        for( int i = 0; i < ProfileClientData.instance.stickers.Count; i++ ) {
            DragSticker dragStickerFind = dragStickers.Find( x => x.StickerId == ProfileClientData.instance.stickers[i] );
            if( dragStickerFind != null ) {
                continue;
            }
            GameObject dragContainerGameObject = new GameObject();
            RectTransform dragContainerRectTransform = dragContainerGameObject.AddComponent<RectTransform>();
            dragContainerRectTransform.SetParent( dragStickerParentContainer, false );

            DragSticker dragStickerTemp = Instantiate( dragStickerPrf, dragContainerRectTransform );
            dragStickerTemp.Init( stickerDataSet.GetStickerGameObjectById( ProfileClientData.instance.stickers[i] ) );
            dragStickers.Add( dragStickerTemp );
            dragStickerTemp.OnBeginDragAction += BeginDragAction;
            dragStickerTemp.OnEndDragAction += EndDragAction;
        }
    }

    internal override void StartHide() {
        backButton.gameObject.SetActive( false );

        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            base.StartHide();
            backBlockImage.gameObject.SetActive( false );
        } );

        OnHideWindow( stickersOnPhotoFieldEditor, photoImage.sprite );

        OnHideWindow = ( StickerOnPhotoFieldEditor[] arg1, Sprite arg2 ) => { };
    }

    void SetCurrentStickeIndex( int newStickeIndex ) {
        currentStikersIndex = newStickeIndex;
        ShowCurrentStickeIndex();
    }

    void ShowCurrentStickeIndex() {
        for( int i = 0; i < dragStickers.Count; i++ ) {
            dragStickers[i].transform.parent.gameObject.SetActive( false );
        }

        for( int i = 0; i < stickersId[currentStikersIndex].Length; i++ ) {
            DragSticker dragStickerFind = dragStickers.Find( x => x.StickerId == stickersId[currentStikersIndex][i] );
            if( dragStickerFind == null ) {
                continue;
            }

            dragStickerFind.transform.parent.gameObject.SetActive( true );
        }
    }

    void ShowChoosePhotoAndVideoWindow() {
        choosePhotoAndVideoWindow.StartShow();

        choosePhotoAndVideoWindow.OnChoosePhoto -= ChoosePhoto;
        choosePhotoAndVideoWindow.OnChoosePhoto += ChoosePhoto;
    }

    void ChoosePhoto( Sprite photoSprite ) {
        if( photoSprite == null ) {
            frameButtonOutlone.gameObject.SetActive( true );
            photoMaskGameObject.gameObject.SetActive( false );
            photoImage.sprite = null;
        }
        else {
            frameButtonOutlone.gameObject.SetActive( false );
            photoMaskGameObject.gameObject.SetActive( true );
            photoImage.sprite = photoSprite;
            photoImage.SetNativeSize();
        }
    }

    void MouseEnterStickAction() {
        isOverOnPhotoField = true;
    }

    void MouseExitStickAction() {
        isOverOnPhotoField = false;
    }

    void MouseEnterThrowSticker() {
        isOverTrash = true;
    }

    void MouseExitThrowSticker() {
        isOverTrash = false;

    }
    void BeginDragAction( DragSticker dragSticker ) {
        dragSticker.gameObject.transform.SetParent( upperDragContainer, false );
    }

    int GetEmptyIndexForSticker() {
        for( int i = 0; i < stickersOnPhotoFieldEditor.Length; i++ ) {
            if( stickersOnPhotoFieldEditor[i] == null ) {
                return i;
            }
        }

        return -1;
    }

    void EndDragAction( DragSticker dragSticker ) {
        if( !isOverOnPhotoField ) {
            return;
        }

        isOverOnPhotoField = false;

        int indexForSticker = GetEmptyIndexForSticker();
        if( indexForSticker == -1 ) {
            return;
        }

        Vector2 stickerPos = Camera.main.ScreenToWorldPoint( new Vector2( Input.mousePosition.x, Input.mousePosition.y ) );

        StickerOnPhotoFieldEditor stickerOnPhotoFieldEditor = photoField.AddSticker( dragSticker, stickerPos );
        stickersOnPhotoFieldEditor[indexForSticker] = stickerOnPhotoFieldEditor;
        stickerOnPhotoFieldEditor.DragStickerOnPhotoField.OnBeginDragAction += BeginDragStickerOnFieldAction;
        stickerOnPhotoFieldEditor.DragStickerOnPhotoField.OnEndDragAction += EndDragStickerOnFieldAction;


        photoEditorStickerDatas.Add( new PhotoEditorStickerData() {
            stickerId = dragSticker.StickerId,
            stickerPos = stickerPos,
            stickerRotation = 0f
        } );
    }

    void BeginDragStickerOnFieldAction( DragStickerOnPhotoField dragSticker ) {
        trashField.ShowTrashField();
    }

    void EndDragStickerOnFieldAction( DragStickerOnPhotoField dragSticker ) {
        trashField.HideTrashField();

        if( !isOverTrash ) {
            return;
        }

        isOverTrash = false;

        bool stickerWasRemoved = false;

        for( int i = 0; i < stickersOnPhotoFieldEditor.Length; i++ ) {
            if( stickerWasRemoved ) {
                stickersOnPhotoFieldEditor[i - 1] = stickersOnPhotoFieldEditor[i];
                stickersOnPhotoFieldEditor[i] = null;

                continue;
            }

            if( stickersOnPhotoFieldEditor[i].DragStickerOnPhotoField.StickerGameObjectData.id == dragSticker.StickerGameObjectData.id ) {
                stickerWasRemoved = true;

                stickersOnPhotoFieldEditor[i].DragStickerOnPhotoField.OnBeginDragAction -= BeginDragStickerOnFieldAction;
                stickersOnPhotoFieldEditor[i].DragStickerOnPhotoField.OnEndDragAction -= EndDragStickerOnFieldAction;

                Destroy( stickersOnPhotoFieldEditor[i].transform.parent.gameObject );
                stickersOnPhotoFieldEditor[i] = null;
            }
        }
    }
}
