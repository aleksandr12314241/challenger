﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using Zenject;

public class ChangeProfileWindow : WindowMonoBehaviour {
    [SerializeField]
    Button doneButton;
    [SerializeField]
    Button cancelButton;
    [SerializeField]
    Button addPhotoButton;
    [SerializeField]
    Button crossAddPhotoButton;
    [SerializeField]
    InputField aboutMeInputField;
    [SerializeField]
    Image avatarPhotoImage;
    [SerializeField]
    Image photoIconImage;
    [SerializeField]
    Button moreFrameButton;
    [SerializeField]
    Button inShopFrameButton;
    [SerializeField]
    RectTransform frameScrollViewsRectTransform;
    [SerializeField]
    RectTransform inShopStickerRectTransform;
    [SerializeField]
    RectTransform profileNamePanelRectTransform;
    [SerializeField]
    RectTransform aboutMePanelRectTransform;
    [SerializeField]
    RectTransform changeProfileContent;

    [Inject]
    NetworkController networkController;
    [Inject]
    Profile profile;
    [Inject]
    SignalBus signalBus;
    [Inject]
    SpriteLoader spriteLoader;

    ChoosePhotoAndVideoWindow choosePhotoAndVideoWindow;

    float timeAnimFrame;

    bool isShowMoreFrames;

    float frameScrollViewsHeight;
    float inShopStickerPosY;
    float profileNamePanelPosY;
    float aboutMePanelPosY;
    float changeProfileContentHeight;

    void Start() {
        Init();

        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, 0 );

        choosePhotoAndVideoWindow = FindObjectOfType<ChoosePhotoAndVideoWindow>();

        doneButton.onClick.AddListener( DoneClick );
        cancelButton.onClick.AddListener( StartHide );
        addPhotoButton.onClick.AddListener( ShowChoosePhotoAndVideoWindow );
        crossAddPhotoButton.onClick.AddListener( CrossAddPhotoClick );
        moreFrameButton.onClick.AddListener( MoreFrameClick );

        backBlockImage.gameObject.SetActive( false );

        timeShowWindow = 0.15f;
        frameScrollViewsHeight = 160f;
        timeAnimFrame = 0.20f;
        profileNamePanelPosY = profileNamePanelRectTransform.anchoredPosition.y;
        aboutMePanelPosY = aboutMePanelRectTransform.anchoredPosition.y;
        inShopStickerPosY = inShopStickerRectTransform.anchoredPosition.y;
        changeProfileContentHeight = changeProfileContent.sizeDelta.y;

        base.StartHide();
    }

    internal void StartShow( UserServ userServ ) {
        base.StartShow();
        backBlockImage.gameObject.SetActive( true );

        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );
        backBlockImage.DOColor(
            new Color( backBlockImage.color.r, backBlockImage.color.g, backBlockImage.color.b, 1f ), timeShowWindow );

        CrossAddPhotoClick();

        spriteLoader.LoadSpriteAvatar( userServ.filename, ( sprite ) => {
            avatarPhotoImage.gameObject.SetActive( true );
            avatarPhotoImage.sprite = sprite;
        } );
    }

    internal override void StartHide() {
        backBlockImage.DOColor( new Color( backBlockImage.color.r, backBlockImage.color.g, backBlockImage.color.b, 0f ), timeShowWindow );
        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            base.StartHide();
            backBlockImage.gameObject.SetActive( false );
        } );
    }

    void DoneClick() {
        Texture2D texture = avatarPhotoImage.sprite.texture;

        byte[] bytes = texture.EncodeToJPG();

        WWWForm form = new WWWForm();
        form.AddField( "text", aboutMeInputField.text );
        form.AddBinaryData( "file", bytes );

        networkController.Post( "profile/" + profile.Id, form, ( response ) => {
            UserResponseDto userResponseDto = JsonUtility.FromJson<UserResponseDto>( response );

            StartHide();

            spriteLoader.AddLocalSprite( userResponseDto.filename, avatarPhotoImage.sprite );

            signalBus.Fire( new OnUpdateProfile() {
                userResponseDto = userResponseDto
            } );
        } );
    }

    void ShowChoosePhotoAndVideoWindow() {
        //choosePhotoAndVideoWindow.StartShow();

        //choosePhotoAndVideoWindow.OnChoosePhoto -= OnChoosePhoto;
        //choosePhotoAndVideoWindow.OnChoosePhoto += OnChoosePhoto;

        NativeGallery.GetImageFromGallery( ( path ) => {
            if( path != null ) {

                Texture2D texture = NativeGallery.LoadImageAtPath( path );

                OnChoosePhoto( Sprite.Create( texture, new Rect( 0, 0, texture.width, texture.height ), Vector2.zero ) );
            }
        } );
    }

    void CrossAddPhotoClick() {
        photoIconImage.gameObject.SetActive( true );
        crossAddPhotoButton.gameObject.SetActive( false );
        avatarPhotoImage.sprite = null;
        avatarPhotoImage.gameObject.SetActive( false );
    }

    void OnChoosePhoto( Sprite photoSprite ) {
        if( photoSprite == null ) {
            CrossAddPhotoClick();
        }
        else {
            photoIconImage.gameObject.SetActive( false );
            crossAddPhotoButton.gameObject.SetActive( true );

            avatarPhotoImage.gameObject.SetActive( true );
            avatarPhotoImage.sprite = photoSprite;
            //avatarPhotoImage.SetNativeSize();
        }
    }

    void MoreFrameClick() {
        if( isShowMoreFrames ) {
            isShowMoreFrames = false;

            frameScrollViewsRectTransform.DOKill();
            frameScrollViewsRectTransform.DOSizeDelta(
                new Vector2( frameScrollViewsRectTransform.sizeDelta.x, frameScrollViewsHeight ), timeAnimFrame );
            inShopStickerRectTransform.DOAnchorPosY( inShopStickerPosY, timeAnimFrame );
            profileNamePanelRectTransform.DOAnchorPosY( profileNamePanelPosY, timeAnimFrame );
            aboutMePanelRectTransform.DOAnchorPosY( aboutMePanelPosY, timeAnimFrame );

            changeProfileContent.sizeDelta = new Vector2( changeProfileContent.sizeDelta.x, changeProfileContentHeight );
        }
        else {
            isShowMoreFrames = true;

            frameScrollViewsRectTransform.DOKill();
            frameScrollViewsRectTransform.DOSizeDelta(
                new Vector2( frameScrollViewsRectTransform.sizeDelta.x, frameScrollViewsHeight * 3f ), timeAnimFrame );
            inShopStickerRectTransform.DOAnchorPosY( inShopStickerPosY - frameScrollViewsHeight * 2, timeAnimFrame );
            profileNamePanelRectTransform.DOAnchorPosY( profileNamePanelPosY - frameScrollViewsHeight * 2, timeAnimFrame );
            aboutMePanelRectTransform.DOAnchorPosY( aboutMePanelPosY - frameScrollViewsHeight * 2, timeAnimFrame );

            changeProfileContent.sizeDelta =
                new Vector2( changeProfileContent.sizeDelta.x, changeProfileContentHeight + frameScrollViewsHeight * 3 );
        }
    }
}
