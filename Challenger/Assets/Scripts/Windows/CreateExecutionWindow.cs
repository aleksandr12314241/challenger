﻿using UnityEngine;
using DG.Tweening;
using UnityEngine.UI;
using System.Collections.Generic;
using Zenject;

public class CreateExecutionWindow : WindowMonoBehaviour {
    [SerializeField]
    Button backButton;
    [SerializeField]
    Text descriptionChallengeText;
    [SerializeField]
    Button addPhotoButton;
    [SerializeField]
    InputField descriptionInputField;
    [SerializeField]
    Text challengLeftTimeText;
    [SerializeField]
    Image frameButtonOutlone;
    [SerializeField]
    GameObject photoMaskGameObject;
    [SerializeField]
    Image photoMask;
    [SerializeField]
    Image photoImage;
    [SerializeField]
    Toggle allFriengsToggle;
    [SerializeField]
    Toggle noneFriengsToggle;
    [SerializeField]
    Toggle chooseFriengsToggle;
    [SerializeField]
    RectTransform photoFieldPanel;
    [SerializeField]
    Button startExecutionButton;

    ChallengeFriendWindow challengeFriendWindow;

    PhotoEditorWindow photoEditorWindow;
    List<StickerOnPhotoField> stickerGameObjects;

    float timeAnimSticker;
    long challengeId;

    [Inject]
    SignalBus signalBus;
    [Inject]
    NetworkController networkController;

    void Start() {
        Init();

        photoEditorWindow = FindObjectOfType<PhotoEditorWindow>();
        stickerGameObjects = new List<StickerOnPhotoField>();

        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, 0 );

        challengeFriendWindow = FindObjectOfType<ChallengeFriendWindow>();

        backButton.onClick.AddListener( StartHide );
        addPhotoButton.onClick.AddListener( PhotoEditorWindowStartShow );
        startExecutionButton.onClick.AddListener( StartExecution );

        chooseFriengsToggle.onValueChanged.AddListener( ChooseFriengsToggleClick );
        challengeFriendWindow.OnCancelClcik += OnCancelClcik;
        challengeFriendWindow.OnAcceptClcik += OnAcceptClcik;

        backBlockImage.gameObject.SetActive( false );
        backButton.gameObject.SetActive( false );

        timeShowWindow = 0.15f;
        timeAnimSticker = 0.20f;

        base.StartHide();
    }

    private void OnDestroy() {
        photoEditorWindow.OnHideWindow -= OnHidePhotoEditorWindowWindow;
    }

    internal void StartShow( long challengeId, string challengeDescription ) {
        base.StartShow();
        backBlockImage.gameObject.SetActive( true );
        backButton.gameObject.SetActive( true );
        backButton.transform.SetAsLastSibling();

        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );

        frameButtonOutlone.gameObject.SetActive( true );
        photoMaskGameObject.gameObject.SetActive( false );
        photoImage.sprite = null;

        descriptionChallengeText.text = "Челлендж: " + challengeDescription;

        this.challengeId = challengeId;
    }

    internal override void StartHide() {
        backButton.gameObject.SetActive( false );

        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            base.StartHide();
            backBlockImage.gameObject.SetActive( false );
        } );

        photoEditorWindow.OnHideWindow -= OnHidePhotoEditorWindowWindow;
    }

    void OnChoosePhoto( Sprite photoSprite ) {
        if( photoSprite == null ) {
            frameButtonOutlone.gameObject.SetActive( true );
            photoMaskGameObject.gameObject.SetActive( false );
            photoImage.sprite = null;
        }
        else {
            frameButtonOutlone.gameObject.SetActive( false );
            photoMaskGameObject.gameObject.SetActive( true );
            photoImage.sprite = photoSprite;
            photoImage.SetNativeSize();
        }
    }

    void ChooseFriengsToggleClick( bool isOn ) {
        if( !isOn ) {
            return;
        }

        backButton.gameObject.SetActive( false );
        challengeFriendWindow.StartShow();
    }

    void OnCancelClcik() {
        backButton.gameObject.SetActive( true );
        noneFriengsToggle.isOn = true;
    }

    void OnAcceptClcik() {
        backButton.gameObject.SetActive( true );
    }

    void PhotoEditorWindowStartShow() {
        //photoEditorWindow.StartShow();
        //photoEditorWindow.OnHideWindow += OnHidePhotoEditorWindowWindow;

        NativeGallery.GetImageFromGallery( ( path ) => {
            if( path != null ) {

                Texture2D texture = NativeGallery.LoadImageAtPath( path );

                OnHidePhotoEditorWindowWindow( new StickerOnPhotoFieldEditor[0],
                                               Sprite.Create( texture, new Rect( 0, 0, texture.width, texture.height ), Vector2.zero ) );
            }
        } );
    }

    void OnHidePhotoEditorWindowWindow( StickerOnPhotoFieldEditor[] stickers, Sprite photoSprite ) {
        photoEditorWindow.OnHideWindow -= OnHidePhotoEditorWindowWindow;

        for( int i = 0; i < stickerGameObjects.Count; i++ ) {
            Destroy( stickerGameObjects[i].gameObject );
        }

        stickerGameObjects = new List<StickerOnPhotoField>();

        for( int i = 0; i < stickers.Length; i++ ) {
            if( stickers[i] == null ) {
                break;
            }

            StickerOnPhotoField stickerGameObject =
                Instantiate( stickers[i].DragStickerOnPhotoField.StickerGameObjectData.stickerOnPhotoField, photoFieldPanel.transform );

            stickerGameObject.Init( stickers[i].GetStickerData() );

            stickerGameObject.transform.position = stickers[i].transform.position;
            stickerGameObject.MyRectTransform.sizeDelta = stickers[i].MyRectTransform.sizeDelta;
            stickerGameObject.transform.localScale = stickers[i].transform.localScale;
            stickerGameObject.transform.rotation = stickers[i].transform.rotation;

            stickerGameObjects.Add( stickerGameObject );
        }

        if( photoSprite == null ) {
            photoMask.gameObject.SetActive( false );
            return;
        }

        photoMask.gameObject.SetActive( true );
        photoImage.sprite = photoSprite;
    }

    void StartExecution() {
        Texture2D texture = photoImage.sprite.texture;

        byte[] bytes = texture.EncodeToJPG();

        WWWForm form = new WWWForm();
        form.AddField( "id", challengeId.ToString() );
        form.AddField( "text", descriptionInputField.text );
        form.AddBinaryData( "file", bytes );

        networkController.Post( "execution", form, ( response ) => {
            StartHide();

            ExecutionResponseDto executionResponseDto = JsonUtility.FromJson<ExecutionResponseDto>( response );

            signalBus.Fire( new OnAddNewExecution() {
                executionResponseDto = executionResponseDto
            } );
        } );
    }
}
