﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using Zenject;

public class ExecutionInfoWindow : WindowMonoBehaviour {
    [SerializeField]
    RectTransform executorInfoContent;
    [SerializeField]
    Button backButton;
    [SerializeField]
    ExecutionInfoWithChallengeInfo executorInfo;
    [SerializeField]
    Button challengeButton;
    [SerializeField]
    Image challengePhotoImage;
    [SerializeField]
    Text challengeDescription;
    [SerializeField]
    RectTransform executorInfoRectTransform;
    [SerializeField]
    RectTransform commentRectTransform;
    [SerializeField]
    Animator arrowWaitAnimator;
    [SerializeField]
    Text noCommentText;

    ChallengeInfoWindow challengeInfoWindow;
    List<Comment> comments = new List<Comment>();

    [Inject]
    readonly Comment.Factory commentPrf;

    ExecutionServ execution;

    void Start() {
        Init();
        backButton.onClick.AddListener( StartHide );

        backBlockImage.gameObject.SetActive( false );
        backButton.gameObject.SetActive( false );

        timeShowWindow = 0.15f;

        challengeButton.onClick.AddListener( ShowChallengeInfoWindow );

        challengeInfoWindow = FindObjectOfType<ChallengeInfoWindow>();

        base.StartHide();
    }

    internal void StartShow( ExecutionServ execution ) {
        DestroyComments();

        this.execution = execution;
        ShowAnim();

        executorInfo.Init( execution );
        float additionalOffsetY = 10f;

        commentRectTransform.anchoredPosition =
            new Vector2( commentRectTransform.anchoredPosition.x,
                         executorInfoRectTransform.anchoredPosition.y - executorInfoRectTransform.sizeDelta.y - additionalOffsetY );

        ResizeContentSizeY();
    }

    void ResizeContentSizeY() {
        executorInfoContent.sizeDelta =
            new Vector2( executorInfoContent.sizeDelta.x,
                         -commentRectTransform.anchoredPosition.y + commentRectTransform.sizeDelta.y + 100f + 130f + 10f );
    }

    internal void ShowAnim() {
        transform.SetAsLastSibling();
        backButton.transform.SetAsLastSibling();

        base.StartShow();
        backBlockImage.gameObject.SetActive( true );
        backButton.gameObject.SetActive( true );

        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, 0 );

        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );

        noCommentText.gameObject.SetActive( false );
        arrowWaitAnimator.gameObject.SetActive( true );

        comments = new List<Comment>();
        Invoke( "ShowComments", 2f );
    }

    internal override void StartHide() {
        backButton.gameObject.SetActive( false );
        CancelInvoke( "ShowComments" );

        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            base.StartHide();
            backBlockImage.gameObject.SetActive( false );
        } );
    }

    void ShowChallengeInfoWindow() {
        challengeInfoWindow.StartShow( execution.challenge );
    }

    void CreateComment( CommentServ commentServ ) {
        Comment commentTemp = commentPrf.Create();
        commentTemp.transform.SetParent( commentRectTransform, false );

        commentTemp.Init( commentServ );
        comments.Add( commentTemp );
    }

    void ShowComments() {
        comments = new List<Comment>();
        arrowWaitAnimator.gameObject.SetActive( false );

        //TODO: убрать, когда будет готов сервер
        int randomNumber = UnityEngine.Random.Range( 0, 5 );
        if( randomNumber == 0 ) {
            noCommentText.gameObject.SetActive( true );
            return;
        }

        for( int i = 0; i < execution.comments.Count; i++ ) {
            CreateComment( execution.comments[i] );
        }

        ResizeCommentSizeY();
        ResizeContentSizeY();
    }

    void DestroyComments() {
        for( int i = 0; i < comments.Count; i++ ) {
            Destroy( comments[i].gameObject );
        }
    }

    void ResizeCommentSizeY() {
        float commentsSizeY = 50f;
        for( int i = 0; i < comments.Count; i++ ) {

            commentsSizeY += comments[i].MyRectTransform.sizeDelta.y;
        }

        commentRectTransform.sizeDelta = new Vector2( commentRectTransform.sizeDelta.x, commentsSizeY );
    }

    internal void OnAddNewExecutionComment( CommentServ commentServ ) {
        if( !isShow ) {
            return;
        }

        if( commentServ == null || execution.id != commentServ.postId || commentServ.postType != PostType.execution ) {
            return;
        }

        CreateComment( commentServ );
        ResizeCommentSizeY();
        ResizeContentSizeY();
    }
}
