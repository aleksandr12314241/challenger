﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class ShopItemsWindow : WindowMonoBehaviour {
    [SerializeField]
    Button backButton;
    [SerializeField]
    GameObject [] tabs;
    [SerializeField]
    TabButton[] tabsButtons;

    void Start () {
        Init();
        backButton.onClick.AddListener( StartHide );

        backBlockImage.gameObject.SetActive( false );
        backButton.gameObject.SetActive( false );

        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, 0 );

        for ( int i = 0; i < tabsButtons.Length; i++ ) {
            int index = i;
            tabsButtons[i].tabButton.onClick.AddListener( () => {
                ShowTab( index );
            } );
        }

        timeShowWindow = 0.15f;

        base.StartHide();
    }

    internal override void StartShow () {
        base.StartShow();
        backBlockImage.gameObject.SetActive( true );
        backButton.gameObject.SetActive( true );

        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );

        ShowTab( 0 );
    }

    internal override void StartHide () {
        backButton.gameObject.SetActive( false );

        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            base.StartHide();
            backBlockImage.gameObject.SetActive( false );
        } );
    }

    void ShowTab ( int index ) {
        for ( int i = 0; i < tabs.Length; i++ ) {
            tabs[i].SetActive( false );
            tabsButtons[i].HideTab();
        }
        tabs[index].SetActive( true );
        tabsButtons[index].ShowTab();
    }
}
