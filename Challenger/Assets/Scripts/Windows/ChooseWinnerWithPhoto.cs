﻿using UnityEngine;

public class ChooseWinnerWithPhoto : MonoBehaviour {
    [SerializeField]
    RectTransform myRectTransform;
    [SerializeField]
    RectTransform executionContainer;
    [SerializeField]
    RectTransform lineRectTransform;

    internal RectTransform MyRectTransform {
        get {
            return myRectTransform;
        }
    }

    float offset = 300f;

    internal void AddExecutorEntreySmallWin( ExecutorEntreySmallWin executorEntreySmallWin ) {
        executorEntreySmallWin.transform.SetParent( executionContainer, false );
    }

    internal void ResizeSizeY() {
        int childCount = executionContainer.childCount;

        if( childCount <= 0 ) {
            myRectTransform.sizeDelta = new Vector2( myRectTransform.sizeDelta.x, 0f );
            return;
        }

        ExecutorEntreySmallWin executorEntreySmallWin = executionContainer.GetChild( childCount - 1 ).GetComponent<ExecutorEntreySmallWin>();
        myRectTransform.sizeDelta = new Vector2( myRectTransform.sizeDelta.x, -executorEntreySmallWin.MyRectTransform.anchoredPosition.y + offset );
    }
}
