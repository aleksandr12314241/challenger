﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using Zenject;

public class ChooseWinnerWindow : WindowMonoBehaviour {
    [SerializeField]
    RectTransform frameRectTransform;
    [SerializeField]
    Button[] crownButtons;
    [SerializeField]
    Button backButton;
    [SerializeField]
    ExecutorInfoPanelWin executorInfoPanelWin;
    [SerializeField]
    RectTransform executorsInfoPanelWinRectTransform;
    [SerializeField]
    RectTransform executorsSVRectTransform;
    [SerializeField]
    GameObject executorsMenuPanel;
    [SerializeField]
    RectTransform executorsContent;
    [SerializeField]
    ChooseWinnerWithPhoto[] chooseWinnerWithPhotos;
    [SerializeField]
    Button endChallengeButton;
    [SerializeField]
    RectTransform endChallengeRectTransform;
    [SerializeField]
    RectTransform endChallengeTextRectTransform;

    RectTransform[] crownRectTransform;
    ExecutorEntreySmallWin currentExecutorEntreySmallWin;

    List<ExecutorEntreySmallWin>[] executorEntreySmalls;
    List<ExecutionServ>[] executionsPlace;

    [Inject]
    NetworkController networkController;
    [Inject]
    readonly ExecutorEntreySmallWin.Factory executorEntreySmallWinFactory;
    [Inject]
    SignalBus signalBus;

    int actionIndex;

    float timeFrameAnim;

    float timeAnimSV;
    float timeShowExecutorsPanel;
    float startExecutorsInfoPanelY = 700f;
    float endExecutorsInfoPanelY = -207f;

    float startExecutorsSVY;
    float endExecutorsSVY = -336f;

    ChallengeServ challengeServ;

    void Start() {
        Init();
        executorInfoPanelWin.Init();

        backButton.onClick.AddListener( StartHide );
        endChallengeButton.onClick.AddListener( EndChallengeClick );

        timeShowWindow = 0.15f;
        timeFrameAnim = 0.2f;
        timeAnimSV = 0.2f;
        timeShowExecutorsPanel = 0.2f;

        startExecutorsInfoPanelY = 700f;
        endExecutorsInfoPanelY = -207f;

        startExecutorsSVY = 0;
        endExecutorsSVY = -336f;

        backButton.gameObject.SetActive( false );
        crownRectTransform = new RectTransform[crownButtons.Length];

        for( int i = 0; i < crownButtons.Length; i++ ) {
            int indexCrown = i;
            crownButtons[i].onClick.AddListener( () => {
                ChooseCrown( indexCrown );
            } );

            crownRectTransform[i] = crownButtons[i].GetComponent<RectTransform>();
        }

        executorInfoPanelWin.OnSetAcceptIndex += OnSetAcceptIndex;

        actionIndex = 0;
        frameRectTransform.anchoredPosition =
            new Vector2( crownRectTransform[actionIndex].anchoredPosition.x, frameRectTransform.anchoredPosition.y );

        backBlockImage.gameObject.SetActive( false );
        backButton.gameObject.SetActive( false );
        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, 0 );

        StartHide();
    }

    internal void StartShow( ChallengeServ challengeServ ) {
        this.challengeServ = challengeServ;

        base.StartShow();
        backButton.gameObject.SetActive( true );

        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );

        executorsInfoPanelWinRectTransform.anchoredPosition =
            new Vector2( executorsInfoPanelWinRectTransform.anchoredPosition.x, startExecutorsInfoPanelY );
        executorsInfoPanelWinRectTransform.gameObject.SetActive( false );
        executorsMenuPanel.SetActive( true );
        executorsSVRectTransform.anchoredPosition = new Vector2( executorsSVRectTransform.anchoredPosition.x, startExecutorsSVY );

        DestroyExecutorEntreySmalls();

        executorEntreySmalls = new List<ExecutorEntreySmallWin>[3] {
            new List<ExecutorEntreySmallWin>(),
            new List<ExecutorEntreySmallWin>(),
            new List<ExecutorEntreySmallWin>()
        };

        executionsPlace = new List<ExecutionServ>[3];
        executionsPlace[0] = challengeServ.executions.FindAll( x => x.tempWinPlace == 0 );
        executionsPlace[1] = challengeServ.executions.FindAll( x => x.tempWinPlace == 1 );
        executionsPlace[2] = challengeServ.executions.FindAll( x => x.tempWinPlace == 2 );

        for( int i = 0; i < executionsPlace.Length; i++ ) {
            for( int j = 0; j < executionsPlace[i].Count; j++ ) {
                CreateExecutorEntreySmall( executionsPlace[i][j], i );
            }
        }

        ResizeContent();
    }

    void CreateExecutorEntreySmall( ExecutionServ executionServ, int tempWinPlace ) {
        ExecutorEntreySmallWin executorEntreySmallWinTemp = executorEntreySmallWinFactory.Create();
        executorEntreySmallWinTemp.Init( executionServ );

        chooseWinnerWithPhotos[tempWinPlace].AddExecutorEntreySmallWin( executorEntreySmallWinTemp );

        executorEntreySmallWinTemp.OnEntreyPanelSmallWinClick += OnEntreyPanelSmallWinClick;

        executorEntreySmalls[tempWinPlace].Add( executorEntreySmallWinTemp );
    }

    void ResizeContent() {
        for( int i = 0; i < chooseWinnerWithPhotos.Length; i++ ) {
            chooseWinnerWithPhotos[i].ResizeSizeY();
        }

        float offset = 260f;

        for( int i = 1; i < chooseWinnerWithPhotos.Length; i++ ) {
            chooseWinnerWithPhotos[i].MyRectTransform.anchoredPosition =
                new Vector2(
                chooseWinnerWithPhotos[i].MyRectTransform.anchoredPosition.x,
                -chooseWinnerWithPhotos[i - 1].MyRectTransform.sizeDelta.y + chooseWinnerWithPhotos[i - 1].MyRectTransform.anchoredPosition.y + ( -offset ) );
        }

        ChooseWinnerWithPhoto chooseWinnerWithPhoto = chooseWinnerWithPhotos[chooseWinnerWithPhotos.Length - 1];
        float newPositionY =
            -chooseWinnerWithPhoto.MyRectTransform.sizeDelta.y + chooseWinnerWithPhoto.MyRectTransform.anchoredPosition.y + ( -offset );

        endChallengeRectTransform.anchoredPosition = new Vector2( endChallengeRectTransform.anchoredPosition.x, newPositionY );
        endChallengeTextRectTransform.anchoredPosition = new Vector2( endChallengeTextRectTransform.anchoredPosition.x, newPositionY );

        executorsSVRectTransform.sizeDelta = new Vector2( executorsSVRectTransform.sizeDelta.x, -newPositionY + offset );
    }

    internal override void StartHide() {
        backButton.gameObject.SetActive( false );

        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            base.StartHide();
            backBlockImage.gameObject.SetActive( false );

            DestroyExecutorEntreySmalls();
        } );
    }

    void OnEntreyPanelSmallWinClick( ExecutorEntreySmallWin executorEntreySmallWin ) {
        switch( actionIndex ) {
        case 0:
            TryShowExecutorInfoPanelWin( executorEntreySmallWin );
            break;
        case 1:
            SetCrown( executorEntreySmallWin );
            break;
        case 2:
            RemoveExecutor( executorEntreySmallWin );
            break;
        }
    }

    void ChooseCrown( int indexCrown ) {
        frameRectTransform.DOAnchorPosX( crownRectTransform[indexCrown].anchoredPosition.x, timeFrameAnim );

        this.actionIndex = indexCrown;
    }

    void OnSetAcceptIndex( int acceptIndex ) {
        if( currentExecutorEntreySmallWin == null ) {
            return;
        }

        switch( acceptIndex ) {
        case 0:
            currentExecutorEntreySmallWin.RemoveCrownType();
            break;
        case 1:
            SetCrown( currentExecutorEntreySmallWin );
            break;
        case 2:
            RemoveExecutor( currentExecutorEntreySmallWin );
            break;
        }

        HideExecutorInfoPanelWin();
    }

    void SetCrown( ExecutorEntreySmallWin executorEntreySmallWin ) {
        int tempWinPlace = executorEntreySmallWin.Execution.tempWinPlace;
        for( int i = 0; i < executionsPlace[tempWinPlace].Count; i++ ) {
            executorEntreySmallWin.RemoveCrownType();
        }

        executorEntreySmallWin.SetCrownType();
    }

    void RemoveExecutor( ExecutorEntreySmallWin esxecutorEntreySmallWin ) {
        esxecutorEntreySmallWin.transform.DOScale( new Vector3( 1, 1, 1 ), timeShowExecutorsPanel ).OnComplete( () => {
            esxecutorEntreySmallWin.transform.DOScale( new Vector3( 0, 0, 1 ), timeShowExecutorsPanel ).OnComplete( () => {
                esxecutorEntreySmallWin.gameObject.SetActive( false );
            } );
        } );
    }

    void TryShowExecutorInfoPanelWin( ExecutorEntreySmallWin executorEntreySmallWin ) {
        if( currentExecutorEntreySmallWin == null ) {
            executorsMenuPanel.SetActive( false );
            executorsInfoPanelWinRectTransform.DOKill();
            executorsInfoPanelWinRectTransform.gameObject.SetActive( true );
            executorsInfoPanelWinRectTransform.DOAnchorPosY( endExecutorsInfoPanelY, timeShowExecutorsPanel );

            executorInfoPanelWin.SetDefaultExecutorInfoPanelWin();

            executorEntreySmallWin.ShowChoose();
            currentExecutorEntreySmallWin = executorEntreySmallWin;

            executorsContent.DOAnchorPosY( endExecutorsSVY, timeShowExecutorsPanel );

            float executorsContentOffset = startExecutorsInfoPanelY - endExecutorsInfoPanelY;
            executorsSVRectTransform.sizeDelta =
                new Vector2( executorsSVRectTransform.sizeDelta.x, executorsSVRectTransform.sizeDelta.y + executorsContentOffset );
            executorsSVRectTransform.anchoredPosition =
                new Vector2( executorsSVRectTransform.anchoredPosition.x, executorEntreySmallWin.MyRectTransform.anchoredPosition.y / 2f );
        }
        else if( currentExecutorEntreySmallWin == executorEntreySmallWin ) {
            HideExecutorInfoPanelWin();
        }
        else {
            executorsInfoPanelWinRectTransform.DOKill();
            executorInfoPanelWin.SetDefaultExecutorInfoPanelWin();

            executorsInfoPanelWinRectTransform.DOAnchorPosY( startExecutorsInfoPanelY, timeShowExecutorsPanel ).OnComplete( () => {
                executorsInfoPanelWinRectTransform.DOAnchorPosY( endExecutorsInfoPanelY, timeShowExecutorsPanel );
            } );

            currentExecutorEntreySmallWin.HideChoose();
            currentExecutorEntreySmallWin = executorEntreySmallWin;
            currentExecutorEntreySmallWin.ShowChoose();
        }
    }

    void HideExecutorInfoPanelWin() {
        executorsInfoPanelWinRectTransform.DOKill();
        currentExecutorEntreySmallWin.HideChoose();

        executorsInfoPanelWinRectTransform.DOAnchorPosY( startExecutorsInfoPanelY, timeShowExecutorsPanel ).OnComplete( () => {
            executorsInfoPanelWinRectTransform.gameObject.SetActive( false );
        } );
        executorsMenuPanel.SetActive( true );

        executorsContent.DOAnchorPosY( startExecutorsSVY, timeShowExecutorsPanel );

        float executorsContentOffset = startExecutorsInfoPanelY - endExecutorsInfoPanelY;
        executorsSVRectTransform.sizeDelta =
            new Vector2( executorsSVRectTransform.sizeDelta.x, executorsSVRectTransform.sizeDelta.y - executorsContentOffset );
        executorsSVRectTransform.anchoredPosition =
            new Vector2( executorsSVRectTransform.anchoredPosition.x, currentExecutorEntreySmallWin.MyRectTransform.anchoredPosition.y / 2f );

        currentExecutorEntreySmallWin = null;
    }

    void DestroyExecutorEntreySmalls() {
        currentExecutorEntreySmallWin = null;

        if( executorEntreySmalls == null ) {
            return;
        }

        for( int i = 0; i < executorEntreySmalls.Length; i++ ) {
            for( int j = 0; j < executorEntreySmalls[i].Count; j++ ) {
                executorEntreySmalls[i][j].OnEntreyPanelSmallWinClick -= OnEntreyPanelSmallWinClick;
                Destroy( executorEntreySmalls[i][j].gameObject );
            }
            executorEntreySmalls[i].Clear();
        }

        executorEntreySmalls = null;
    }

    void EndChallengeClick() {
        long[] tempWinPlaces = {
            -1, -1, -1
        };

        int tempWinPlacesCount = 0;

        for( int i = 0; i < executorEntreySmalls.Length; i++ ) {
            for( int j = 0; j < executorEntreySmalls[i].Count; j++ ) {
                if( executorEntreySmalls[i][j].IsWin ) {
                    tempWinPlacesCount++;
                    tempWinPlaces[i] = executorEntreySmalls[i][j].Execution.id;
                    break;
                }
            }
        }

        if( tempWinPlacesCount >= 3 ) {
            EndChallengeRequestDto endChallengeRequestDto = new EndChallengeRequestDto() {
                first = tempWinPlaces[0],
                second = tempWinPlaces[1],
                third = tempWinPlaces[2]
            };

            networkController.Post( "challenge/" + challengeServ.id + "/end", endChallengeRequestDto, ( response ) => {
                EndChallengeResponseDto endChallengeResponseDto = JsonUtility.FromJson<EndChallengeResponseDto>( response );

                Debug.Log( "on end challenge" );

                StartHide();

                signalBus.Fire( new OnEndChallenge() {
                    endChallengeResponseDto = endChallengeResponseDto
                } );
            } );
        }
        else {
            Debug.Log( "выберете победителя" );
        }
    }
}
