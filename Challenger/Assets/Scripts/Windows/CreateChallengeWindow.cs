﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using System;
using Zenject;

public class CreateChallengeWindow : WindowMonoBehaviour {
    [SerializeField]
    Button backButton;
    [SerializeField]
    GameObject photoMask;
    [SerializeField]
    Image photoImage;
    [SerializeField]
    InputField description;
    [SerializeField]
    InputField specialCondition;
    [SerializeField]
    Button startChallengeButton;
    [SerializeField]
    TopPlaceEntry[] topPlaces;
    [SerializeField]
    RectTransform challengePanelRectTransform;
    [SerializeField]
    RectTransform createChallengeContent;
    [SerializeField]
    Button photoEditorWindowButton;
    [SerializeField]
    RectTransform photoFieldPanel;

    Vector2 startStartChallengPanelPos;
    float challengeContentHeight;
    float challengePanelPosY;

    float topPlacesPosY;
    float starPanelPosY;

    PhotoEditorWindow photoEditorWindow;
    List<StickerOnPhotoField> stickerGameObjects;

    [Inject]
    SignalBus signalBus;
    [Inject]
    NetworkController networkController;

    internal void Start () {
        Init();

        photoEditorWindow = FindObjectOfType<PhotoEditorWindow>();
        stickerGameObjects = new List<StickerOnPhotoField>();

        backButton.onClick.AddListener( StartHide );

        timeShowWindow = 0.15f;
        challengeContentHeight = createChallengeContent.sizeDelta.y;
        challengePanelPosY = challengePanelRectTransform.anchoredPosition.y;

        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, contentRectTransform.anchoredPosition.y );

        backButton.gameObject.SetActive( false );

        createChallengeContent.sizeDelta = new Vector2( createChallengeContent.sizeDelta.x,
                -challengePanelRectTransform.anchoredPosition.y + challengePanelRectTransform.sizeDelta.y + 10f );

        for ( int i = 0; i < topPlaces.Length; i++ ) {
            topPlaces[i].Init( i );
        }

        Hide();

        photoEditorWindowButton.onClick.AddListener( PhotoEditorWindowStartShow );
        startChallengeButton.onClick.AddListener( StartChallenge );
    }

    private void OnDestroy () {
        photoEditorWindow.OnHideWindow -= OnHidePhotoEditorWindowWindow;
    }

    internal override void StartShow () {
        Show();

        contentRectTransform.DOKill();
        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );

        backButton.transform.SetAsLastSibling();
        backButton.gameObject.SetActive( true );

        createChallengeContent.anchoredPosition = new Vector2( createChallengeContent.anchoredPosition.x, 0f );

        photoMask.gameObject.SetActive( false );

        for ( int i = 0; i < stickerGameObjects.Count; i++ ) {
            Destroy( stickerGameObjects[i].gameObject );
        }

        stickerGameObjects = new List<StickerOnPhotoField>();
    }

    internal override void StartHide () {
        backButton.gameObject.SetActive( false );

        contentRectTransform.DOKill();
        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            Hide();
        } );

        photoEditorWindow.OnHideWindow -= OnHidePhotoEditorWindowWindow;
    }

    internal void ShowMoreStickerClick ( bool isShowSticker, float timeAnimSticker, float offsetHeight ) {
        float newChallengePanelRectPosY = challengePanelPosY;

        if ( isShowSticker ) {
            newChallengePanelRectPosY = challengePanelPosY - offsetHeight;

            challengePanelRectTransform.DOKill();
            challengePanelRectTransform.DOAnchorPosY( newChallengePanelRectPosY, timeAnimSticker );

            createChallengeContent.sizeDelta =
                new Vector2( createChallengeContent.sizeDelta.x,
                             -newChallengePanelRectPosY + challengePanelRectTransform.sizeDelta.y + 10f );
        }
        else {
            challengePanelRectTransform.DOKill();
            challengePanelRectTransform.DOAnchorPosY( newChallengePanelRectPosY, timeAnimSticker );

            createChallengeContent.sizeDelta = new Vector2( createChallengeContent.sizeDelta.x,
                    -newChallengePanelRectPosY + challengePanelRectTransform.sizeDelta.y + 10f );
        }
    }

    void PhotoEditorWindowStartShow () {
        //photoEditorWindow.StartShow();
        //photoEditorWindow.OnHideWindow += OnHidePhotoEditorWindowWindow;

        NativeGallery.GetImageFromGallery( ( path ) => {
            if ( path != null ) {

                Texture2D texture = NativeGallery.LoadImageAtPath( path );

                OnHidePhotoEditorWindowWindow( new StickerOnPhotoFieldEditor[0],
                                               Sprite.Create( texture, new Rect( 0, 0, texture.width, texture.height ), Vector2.zero ) );
            }
        } );
    }

    void OnHidePhotoEditorWindowWindow ( StickerOnPhotoFieldEditor[] stickers, Sprite photoSprite ) {
        photoEditorWindow.OnHideWindow -= OnHidePhotoEditorWindowWindow;

        for ( int i = 0; i < stickerGameObjects.Count; i++ ) {
            Destroy( stickerGameObjects[i].gameObject );
        }

        stickerGameObjects = new List<StickerOnPhotoField>();

        for ( int i = 0; i < stickers.Length; i++ ) {
            if ( stickers[i] == null ) {
                break;
            }

            StickerOnPhotoField stickerGameObject =
                Instantiate( stickers[i].DragStickerOnPhotoField.StickerGameObjectData.stickerOnPhotoField, photoFieldPanel.transform );

            stickerGameObject.Init( stickers[i].GetStickerData() );

            stickerGameObject.transform.position = stickers[i].transform.position;
            stickerGameObject.MyRectTransform.sizeDelta = stickers[i].MyRectTransform.sizeDelta;
            stickerGameObject.transform.localScale = stickers[i].transform.localScale;
            stickerGameObject.transform.rotation = stickers[i].transform.rotation;

            stickerGameObjects.Add( stickerGameObject );
        }

        if ( photoSprite == null ) {
            photoMask.gameObject.SetActive( false );
            return;
        }

        photoMask.gameObject.SetActive( true );
        photoImage.sprite = photoSprite;
    }

    private void StartChallenge () {
        Texture2D texture = photoImage.sprite.texture;

        byte[] bytes = texture.EncodeToJPG();

        WWWForm form = new WWWForm();
        form.AddField( "text", description.text );
        form.AddBinaryData( "file", bytes );

        networkController.Post( "challenge", form, ( response ) => {
            StartHide();

            ChallengeResponseDto challenge = JsonUtility.FromJson<ChallengeResponseDto>( response );

            signalBus.Fire( new OnAddNewChallenge() {
                challenge = challenge
            } );
        } );
    }
}
