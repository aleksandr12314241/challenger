﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;
using Zenject;
using System.Linq;

public class ChallengeInfoWindow : WindowMonoBehaviour {
    [SerializeField]
    RectTransform challengeInfoSV;
    [SerializeField]
    RectTransform challengeInfoContent;
    [SerializeField]
    RectTransform executionsRectTransform;
    [SerializeField]
    RectTransform executorsContent;
    [SerializeField]
    RectTransform executorsMenuPanelRectTransform;
    [SerializeField]
    Button goToExecutorsButtons;
    [SerializeField]
    Button goToChallengeInfoButtons;
    [SerializeField]
    Button backButton;
    [SerializeField]
    ChallengeInfo challengeInfo;
    [SerializeField]
    RectTransform executorsInfoPanelRectTransform;
    [SerializeField]
    RectTransform executorsSVRectTransform;
    [SerializeField]
    RectTransform challengeInfoRectTransform;
    [SerializeField]
    RectTransform topPlaceRectTransform;
    [SerializeField]
    RectTransform commentRectTransform;
    [SerializeField]
    Animator commentsArrowWaitAnimator;
    [SerializeField]
    Text noCommentsText;
    [SerializeField]
    Text challengeDescription;
    [SerializeField]
    RectTransform challengeDescriptionRectTransform;
    [SerializeField]
    RectTransform goToChallengeInfoRectTransform;
    [SerializeField]
    RectTransform executorsPanelRectTransform;
    [SerializeField]
    GridLayoutGroup executorsPanelGridLayoutGroup;
    [SerializeField]
    Text noExecutorsText;
    [SerializeField]
    Animator executorsArrowWaitAnimator;
    [SerializeField]
    ExecutorInfoPanelInChallenge executorInfoPanelInChallenge;

    [Inject]
    readonly ExecutionSmallInChallengeInfo.Factory executorSmallInChallengeInfo;
    [Inject]
    readonly Comment.Factory commentPrf;

    ExecutionSmallInChallengeInfo[] executorEntreySmalls;
    ExecutionSmallInChallengeInfo currentExecutorEntreySmall;

    DateTime time;

    float timeAnimSV;
    float timeShowExecutorsPanel;
    float startExecutorsInfoPanelY;
    float endExecutorsInfoPanelY;
    float startExecutorsSVY;
    float endExecutorsSVY;

    List<Comment> comments = new List<Comment>();
    List<ExecutionSmallInChallengeInfo> executions = new List<ExecutionSmallInChallengeInfo>();

    ChallengeServ challenge;

    void Start() {
        Init();
        executorInfoPanelInChallenge.Init();
        executorInfoPanelInChallenge.OnCloseExecutorInfoPanelInChallenge += OnCloseExecutorInfoPanelInChallenge;

        goToExecutorsButtons.onClick.AddListener( GoToExecutorsClick );
        goToChallengeInfoButtons.onClick.AddListener( GoToChallengeInfoClick );

        backButton.onClick.AddListener( StartHide );

        backBlockImage.gameObject.SetActive( false );
        backButton.gameObject.SetActive( false );

        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, 0 );
        challengeInfoSV.anchoredPosition = new Vector2( 0, 0 );
        executionsRectTransform.anchoredPosition = new Vector2( 0, -sizeDeltaScreen.y );

        timeShowWindow = 0.15f;
        timeAnimSV = 0.2f;
        timeShowExecutorsPanel = 0.2f;

        startExecutorsInfoPanelY = 850f;
        endExecutorsInfoPanelY = -80f;

        startExecutorsSVY = 0;
        endExecutorsSVY = -747f;

        executorsInfoPanelRectTransform.anchoredPosition =
            new Vector2( executorsInfoPanelRectTransform.anchoredPosition.x, startExecutorsInfoPanelY );
        executorsSVRectTransform.anchoredPosition = new Vector2( executorsSVRectTransform.anchoredPosition.y, startExecutorsSVY );

        //TODO: это всё создаётся динамически и присваивается динамически
        executorEntreySmalls = GetComponentsInChildren<ExecutionSmallInChallengeInfo>( true );
        for( int i = 0; i < executorEntreySmalls.Length; i++ ) {
            executorEntreySmalls[i].Init();
            executorEntreySmalls[i].OnExecutionEntityPanelSmallClick += OnEntreyPanelSmallClick;
        }

        base.StartHide();
    }

    internal void StartShow( ChallengeServ challenge ) {
        DestroyExecutions();
        DestroyComments();

        this.challenge = challenge;
        //GetPostDataByPostId( postId );

        ShowAnim();
        SetChallengeInfo( challenge );
        SetDescriptionChallengeSize( challenge.description );
        ResizeContentSizeY();
    }

    void ResizeContentSizeY() {
        challengeInfoContent.sizeDelta =
            new Vector2( challengeInfoContent.sizeDelta.x,
                         -commentRectTransform.anchoredPosition.y + commentRectTransform.sizeDelta.y + 100f + 130f + 10f );
    }

    void SetChallengeInfo( ChallengeServ challenge ) {
        challengeInfo.Init( challenge );
        float additionalOffsetY = 50f;

        topPlaceRectTransform.anchoredPosition =
            new Vector2( topPlaceRectTransform.anchoredPosition.x,
                         challengeInfo.MyRectTransform.anchoredPosition.y - challengeInfo.MyRectTransform.sizeDelta.y - additionalOffsetY );
        commentRectTransform.anchoredPosition =
            new Vector2( commentRectTransform.anchoredPosition.x,
                         topPlaceRectTransform.anchoredPosition.y - topPlaceRectTransform.sizeDelta.y - additionalOffsetY );
    }

    void SetDescriptionChallengeSize( string challengeDescriptionString ) {
        float additionalOffsetY = 5f;

        challengeDescription.text = challengeDescriptionString;
        challengeDescriptionRectTransform.sizeDelta =
            new Vector2( challengeDescriptionRectTransform.sizeDelta.x, challengeDescription.preferredHeight + additionalOffsetY );

        goToChallengeInfoRectTransform.sizeDelta = new Vector2( goToChallengeInfoRectTransform.sizeDelta.x,
                -challengeDescriptionRectTransform.anchoredPosition.y + challengeDescriptionRectTransform.sizeDelta.y + additionalOffsetY );
        executorsMenuPanelRectTransform.sizeDelta =
            new Vector2( executorsMenuPanelRectTransform.sizeDelta.x, goToChallengeInfoRectTransform.sizeDelta.y );
        executorsPanelRectTransform.anchoredPosition = new Vector2( executorsPanelRectTransform.anchoredPosition.x,
                executorsMenuPanelRectTransform.anchoredPosition.y - executorsMenuPanelRectTransform.sizeDelta.y - additionalOffsetY * 2f );
    }

    void ShowAnim() {
        transform.SetAsLastSibling();
        backButton.transform.SetAsLastSibling();

        base.StartShow();

        backBlockImage.gameObject.SetActive( true );
        backButton.gameObject.SetActive( true );

        challengeInfoSV.anchoredPosition = Vector2.zero;
        challengeInfoContent.anchoredPosition = Vector2.zero;
        executionsRectTransform.anchoredPosition = new Vector2( 0, -sizeDeltaScreen.y );
        executorsContent.anchoredPosition = Vector2.zero;

        executorsInfoPanelRectTransform.anchoredPosition =
            new Vector2( executorsInfoPanelRectTransform.anchoredPosition.x, startExecutorsInfoPanelY );
        executorsInfoPanelRectTransform.gameObject.SetActive( false );
        executorsMenuPanelRectTransform.gameObject.SetActive( true );
        executorsSVRectTransform.anchoredPosition = new Vector2( executorsSVRectTransform.anchoredPosition.x, startExecutorsSVY );

        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );

        noCommentsText.gameObject.SetActive( false );
        commentsArrowWaitAnimator.gameObject.SetActive( true );
        executorsArrowWaitAnimator.gameObject.SetActive( true );
        executorsPanelGridLayoutGroup.childAlignment = TextAnchor.UpperCenter;

        Invoke( "ShowComments", 2f );
        Invoke( "ShowExecutors", 5f );
    }

    internal override void StartHide() {
        CancelInvoke( "ShowComments" );
        CancelInvoke( "ShowExecutors" );

        backButton.gameObject.SetActive( false );

        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            base.StartHide();
            backBlockImage.gameObject.SetActive( false );
        } );
    }

    void CreateComment( CommentServ commentServ ) {
        Comment commentTemp = commentPrf.Create();
        commentTemp.transform.SetParent( commentRectTransform, false );

        commentTemp.Init( commentServ );
        comments.Add( commentTemp );
    }

    void ShowComments() {
        commentsArrowWaitAnimator.gameObject.SetActive( false );

        //TODO: убрать, когда будет готов сервер
        int randomNumber = UnityEngine.Random.Range( 0, 5 );
        if( challenge.comments.Count <= 0 ) {
            noCommentsText.gameObject.SetActive( true );
            return;
        }

        for( int i = 0; i < challenge.comments.Count; i++ ) {
            CreateComment( challenge.comments[i] );
        }

        ResizeCommentSizeY();
        ResizeContentSizeY();
    }

    internal void OnAddNewChallengeComment( CommentServ commentServ ) {
        if( !isShow ) {
            return;
        }

        if( commentServ == null || challenge.id != commentServ.postId || commentServ.postType != PostType.challenge ) {
            return;
        }

        CreateComment( commentServ );
        ResizeCommentSizeY();
        ResizeContentSizeY();
    }

    void ResizeCommentSizeY() {
        float commentsSizeY = 50f;
        for( int i = 0; i < comments.Count; i++ ) {

            commentsSizeY += comments[i].MyRectTransform.sizeDelta.y;
        }

        commentRectTransform.sizeDelta = new Vector2( commentRectTransform.sizeDelta.x, commentsSizeY );
    }

    void DestroyComments() {
        for( int i = 0; i < comments.Count; i++ ) {
            Destroy( comments[i].gameObject );
        }
    }

    void ShowExecution() {
        ShowExecution( challenge.executions );
    }

    void CreateExecution( ExecutionServ executionServ ) {
        Debug.Log( "11111" );
        ExecutionSmallInChallengeInfo executorSmallInChallengeInfoTemp = this.executions.Find( x => x.Execution.id == executionServ.id );
        if( executorSmallInChallengeInfoTemp != null ) {
            Debug.Log( "22222" );
            return;
        }
        Debug.Log( "33333" );
        ExecutionSmallInChallengeInfo executionTemp = executorSmallInChallengeInfo.Create();
        executionTemp.transform.SetParent( executorsPanelRectTransform, false );

        executionTemp.Init( executionServ );
        this.executions.Add( executionTemp );
        executionTemp.OnExecutionEntityPanelSmallClick += OnEntreyPanelSmallClick;
    }

    void ShowExecution( List<ExecutionServ> executions ) {
        executorsArrowWaitAnimator.gameObject.SetActive( false );

        if( executions.Count <= 0 ) {
            noExecutorsText.gameObject.SetActive( true );
            return;
        }

        executorsPanelGridLayoutGroup.childAlignment = TextAnchor.UpperLeft;
        noExecutorsText.gameObject.SetActive( false );
        for( int i = 0; i < executions.Count; i++ ) {
            CreateExecution( executions[i] );
        }
    }

    void DestroyExecutions() {
        for( int i = 0; i < executions.Count; i++ ) {
            executions[i].OnExecutionEntityPanelSmallClick -= OnEntreyPanelSmallClick;
            Destroy( executions[i].gameObject );
        }

        executions.Clear();
    }

    void GoToExecutorsClick() {
        challengeInfoSV.DOAnchorPosY( sizeDeltaScreen.y, timeAnimSV );
        executionsRectTransform.DOAnchorPosY( 0, timeAnimSV ).OnComplete( () => {
            ShowExecution();
        } );
    }

    void GoToChallengeInfoClick() {
        challengeInfoSV.DOAnchorPosY( 0, timeAnimSV );
        executionsRectTransform.DOAnchorPosY( -sizeDeltaScreen.y, timeAnimSV );
    }

    void OnCloseExecutorInfoPanelInChallenge() {
        executorsInfoPanelRectTransform.DOKill();
        currentExecutorEntreySmall.HideChoose();

        executorsInfoPanelRectTransform.DOAnchorPosY( startExecutorsInfoPanelY, timeShowExecutorsPanel ).OnComplete( () => {
            executorsInfoPanelRectTransform.gameObject.SetActive( false );
        } );
        executorsMenuPanelRectTransform.gameObject.SetActive( true );

        executorsSVRectTransform.DOAnchorPosY( startExecutorsSVY, timeShowExecutorsPanel );

        float executorsContentOffset = startExecutorsInfoPanelY - endExecutorsInfoPanelY;
        executorsContent.sizeDelta =
            new Vector2( executorsContent.sizeDelta.x, executorsContent.sizeDelta.y - executorsContentOffset );
        executorsContent.anchoredPosition =
            new Vector2( executorsContent.anchoredPosition.x, currentExecutorEntreySmall.MyRectTransform.anchoredPosition.y / 2f );

        currentExecutorEntreySmall = null;

        //executorsContent.DOSizeDelta( new Vector2( executorsContent.sizeDelta.x, executorsContent.sizeDelta.y - executorsContentOffset ),
        //                              timeShowExecutorsPanel );
        //executorsContent.DOAnchorPosY( executorsContent.sizeDelta.y - executorsContentOffset, timeShowExecutorsPanel );
    }

    void OnEntreyPanelSmallClick( ExecutionSmallInChallengeInfo executorEntreySmall ) {
        if( currentExecutorEntreySmall == null ) {
            executorsMenuPanelRectTransform.gameObject.SetActive( false );
            executorsInfoPanelRectTransform.DOKill();
            executorsInfoPanelRectTransform.gameObject.SetActive( true );
            executorsInfoPanelRectTransform.DOAnchorPosY( endExecutorsInfoPanelY, timeShowExecutorsPanel );

            executorEntreySmall.ShowChoose();
            currentExecutorEntreySmall = executorEntreySmall;

            executorsSVRectTransform.DOAnchorPosY( endExecutorsSVY, timeShowExecutorsPanel );

            float executorsContentOffset = startExecutorsInfoPanelY - endExecutorsInfoPanelY;
            executorsContent.sizeDelta =
                new Vector2( executorsContent.sizeDelta.x, executorsContent.sizeDelta.y + executorsContentOffset );
            executorsContent.anchoredPosition =
                new Vector2( executorsContent.anchoredPosition.x, currentExecutorEntreySmall.MyRectTransform.anchoredPosition.y / 2f );
            executorInfoPanelInChallenge.StartShow( executorEntreySmall );

            //executorsContent.DOSizeDelta( new Vector2( executorsContent.sizeDelta.x, executorsContent.sizeDelta.y + executorsContentOffset ),
            //                              timeShowExecutorsPanel );
            //executorsContent.DOAnchorPosY( executorsContent.sizeDelta.y + executorsContentOffset, timeShowExecutorsPanel );
        }
        else if( currentExecutorEntreySmall == executorEntreySmall ) {
            OnCloseExecutorInfoPanelInChallenge();
        }
        else {
            executorsInfoPanelRectTransform.DOKill();

            executorsInfoPanelRectTransform.DOAnchorPosY( startExecutorsInfoPanelY, timeShowExecutorsPanel ).OnComplete( () => {
                executorInfoPanelInChallenge.StartShow( executorEntreySmall );
                executorsInfoPanelRectTransform.DOAnchorPosY( endExecutorsInfoPanelY, timeShowExecutorsPanel );
            } );

            currentExecutorEntreySmall.HideChoose();
            currentExecutorEntreySmall = executorEntreySmall;
            currentExecutorEntreySmall.ShowChoose();
        }
    }
}
