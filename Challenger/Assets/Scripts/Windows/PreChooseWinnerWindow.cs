﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Collections.Generic;
using Zenject;
using System;

public class PreChooseWinnerWindow : WindowMonoBehaviour {
    [SerializeField]
    Button choseWinnerButton;
    [SerializeField]
    RectTransform frameRectTransform;
    [SerializeField]
    Button[] crownButtons;
    [SerializeField]
    Button backButton;
    [SerializeField]
    ExecutorInfoPanelPreWin executorInfoPanelPreWin;
    [SerializeField]
    RectTransform executorsInfoPanelWinRectTransform;
    [SerializeField]
    RectTransform executorsSVRectTransform;
    [SerializeField]
    GameObject executorsMenuPanel;
    [SerializeField]
    RectTransform executorsContent;
    [SerializeField]
    Image challengePhotoImage;
    [SerializeField]
    Text challengeDescription;

    [Inject]
    SpriteLoader spriteLoader;
    [Inject]
    readonly ExecutionEntreySmallPreWin.Factory executionEntreySmallPreWinFactory;

    RectTransform[] crownRectTransform;
    List<ExecutionEntreySmallPreWin> executorEntreySmalls = new List<ExecutionEntreySmallPreWin>();
    ExecutionEntreySmallPreWin currentExecutorEntreySmallWin;

    ChooseWinnerWindow chooseWinnerWindow;

    int indexCrown;

    float timeFrameAnim;

    float timeAnimSV;
    float timeShowExecutorsPanel;
    float startExecutorsInfoPanelY = 700f;
    float endExecutorsInfoPanelY = -100f;

    float startExecutorsSVY;
    float endExecutorsSVY = -230f;

    ChallengeServ challengeServ;

    void Start() {
        Init();
        executorInfoPanelPreWin.Init();

        chooseWinnerWindow = FindObjectOfType<ChooseWinnerWindow>();

        backButton.onClick.AddListener( StartHide );
        choseWinnerButton.onClick.AddListener( ShowChooseWinnerWindow );

        timeShowWindow = 0.15f;
        timeFrameAnim = 0.2f;
        timeAnimSV = 0.2f;
        timeShowExecutorsPanel = 0.2f;

        startExecutorsInfoPanelY = 700f;
        endExecutorsInfoPanelY = -100f;

        startExecutorsSVY = 0;
        endExecutorsSVY = -230f;

        backButton.gameObject.SetActive( false );
        crownRectTransform = new RectTransform[crownButtons.Length];

        for( int i = 0; i < crownButtons.Length; i++ ) {
            int indexCrown = i;
            crownButtons[i].onClick.AddListener( () => {
                ChooseCrown( indexCrown );
            } );

            crownRectTransform[i] = crownButtons[i].GetComponent<RectTransform>();
        }

        executorInfoPanelPreWin.OnSetCrown += OnSetCrown;

        indexCrown = 0;
        frameRectTransform.anchoredPosition =
            new Vector2( crownRectTransform[indexCrown].anchoredPosition.x, frameRectTransform.anchoredPosition.y );

        backBlockImage.gameObject.SetActive( false );
        backButton.gameObject.SetActive( false );
        contentRectTransform.anchoredPosition = new Vector2( sizeDeltaScreen.x, 0 );

        StartHide();
    }

    internal void StartShow( ChallengeServ challengeServ ) {
        this.challengeServ = challengeServ;

        if( challengeServ.endChallengeDate < challengeServ.nowServerTime ) {
            chooseWinnerWindow.gameObject.SetActive( true );
        }
        else {
            chooseWinnerWindow.gameObject.SetActive( false );
        }

        DestroyExecutorEntreySmalls();

        base.StartShow();
        backButton.gameObject.SetActive( true );

        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );

        executorsInfoPanelWinRectTransform.anchoredPosition =
            new Vector2( executorsInfoPanelWinRectTransform.anchoredPosition.x, startExecutorsInfoPanelY );
        executorsInfoPanelWinRectTransform.gameObject.SetActive( false );
        executorsMenuPanel.SetActive( true );
        executorsSVRectTransform.anchoredPosition = new Vector2( executorsSVRectTransform.anchoredPosition.x, startExecutorsSVY );

        executorsContent.gameObject.SetActive( true );

        challengeDescription.text = challengeServ.description;
        spriteLoader.LoadSpriteContent( challengeServ.contents[0].filename, ( sprite ) => {
            challengePhotoImage.sprite = sprite;
        } );

        for( int i = 0; i < challengeServ.executions.Count; i++ ) {
            CreateExecutionEntreySmallPreWin( challengeServ.executions[i] );
        }
    }

    void CreateExecutionEntreySmallPreWin( ExecutionServ executionServ ) {
        ExecutionEntreySmallPreWin executionEntreySmallPreWinTemp = executionEntreySmallPreWinFactory.Create();
        executionEntreySmallPreWinTemp.Init( executionServ );

        executionEntreySmallPreWinTemp.transform.SetParent( executorsSVRectTransform, false );
        executionEntreySmallPreWinTemp.OnEntreyPanelSmallWinClick += OnEntreyPanelSmallWinClick;

        executorEntreySmalls.Add( executionEntreySmallPreWinTemp );
    }

    internal override void StartHide() {

        backButton.gameObject.SetActive( false );

        contentRectTransform.DOAnchorPosX( sizeDeltaScreen.x, timeShowWindow ).OnComplete( () => {
            DestroyExecutorEntreySmalls();

            base.StartHide();
            backBlockImage.gameObject.SetActive( false );
        } );
    }

    internal void OnEndChallenge() {
        StartHide();
    }

    void OnEntreyPanelSmallWinClick( ExecutionEntreySmallPreWin executorEntreySmallWin ) {
        if( indexCrown == 0 ) {
            TryShowExecutorInfoPanelPreWin( executorEntreySmallWin );

            return;
        }

        executorEntreySmallWin.SetCrownType( indexCrown - 1 );
    }

    void ChooseCrown( int indexCrown ) {
        frameRectTransform.DOAnchorPosX( crownRectTransform[indexCrown].anchoredPosition.x, timeFrameAnim );

        this.indexCrown = indexCrown;
    }

    void OnSetCrown( int currentIndex ) {
        if( currentExecutorEntreySmallWin == null ) {
            return;
        }

        if( currentIndex == 0 ) {
            currentExecutorEntreySmallWin.RemoveCrownType();
        }

        currentExecutorEntreySmallWin.SetCrownType( currentIndex - 1 );
    }

    void TryShowExecutorInfoPanelPreWin( ExecutionEntreySmallPreWin executorEntreySmallWin ) {
        if( currentExecutorEntreySmallWin == null ) {
            executorsMenuPanel.SetActive( false );
            executorsInfoPanelWinRectTransform.DOKill();
            executorsInfoPanelWinRectTransform.gameObject.SetActive( true );
            executorsInfoPanelWinRectTransform.DOAnchorPosY( endExecutorsInfoPanelY, timeShowExecutorsPanel );

            executorInfoPanelPreWin.SetExecutionInfo( executorEntreySmallWin.Execution );

            executorEntreySmallWin.ShowChoose();
            currentExecutorEntreySmallWin = executorEntreySmallWin;

            executorsContent.DOAnchorPosY( endExecutorsSVY, timeShowExecutorsPanel );

            float executorsContentOffset = startExecutorsInfoPanelY - endExecutorsInfoPanelY;
            executorsSVRectTransform.sizeDelta =
                new Vector2( executorsSVRectTransform.sizeDelta.x, executorsSVRectTransform.sizeDelta.y + executorsContentOffset );
            executorsSVRectTransform.anchoredPosition =
                new Vector2( executorsSVRectTransform.anchoredPosition.x, executorEntreySmallWin.MyRectTransform.anchoredPosition.y / 2f );
        }
        else if( currentExecutorEntreySmallWin == executorEntreySmallWin ) {
            executorsInfoPanelWinRectTransform.DOKill();
            executorEntreySmallWin.HideChoose();

            executorsInfoPanelWinRectTransform.DOAnchorPosY( startExecutorsInfoPanelY, timeShowExecutorsPanel ).OnComplete( () => {
                executorsInfoPanelWinRectTransform.gameObject.SetActive( false );
            } );
            executorsMenuPanel.SetActive( true );

            executorsContent.DOAnchorPosY( startExecutorsSVY, timeShowExecutorsPanel );

            currentExecutorEntreySmallWin = null;

            float executorsContentOffset = startExecutorsInfoPanelY - endExecutorsInfoPanelY;
            executorsSVRectTransform.sizeDelta =
                new Vector2( executorsSVRectTransform.sizeDelta.x, executorsSVRectTransform.sizeDelta.y - executorsContentOffset );
            executorsSVRectTransform.anchoredPosition =
                new Vector2( executorsSVRectTransform.anchoredPosition.x, executorEntreySmallWin.MyRectTransform.anchoredPosition.y / 2f );
        }
        else {
            executorsInfoPanelWinRectTransform.DOKill();

            executorInfoPanelPreWin.SetExecutionInfo( executorEntreySmallWin.Execution );

            executorsInfoPanelWinRectTransform.DOAnchorPosY( startExecutorsInfoPanelY, timeShowExecutorsPanel ).OnComplete( () => {
                executorsInfoPanelWinRectTransform.DOAnchorPosY( endExecutorsInfoPanelY, timeShowExecutorsPanel );
            } );

            currentExecutorEntreySmallWin.HideChoose();
            currentExecutorEntreySmallWin = executorEntreySmallWin;
            currentExecutorEntreySmallWin.ShowChoose();
        }
    }

    void ShowChooseWinnerWindow() {
        chooseWinnerWindow.StartShow( challengeServ );
    }

    void DestroyExecutorEntreySmalls() {
        currentExecutorEntreySmallWin = null;

        for( int i = 0; i < executorEntreySmalls.Count; i++ ) {
            executorEntreySmalls[i].OnEntreyPanelSmallWinClick -= OnEntreyPanelSmallWinClick;
            Destroy( executorEntreySmalls[i].gameObject );
        }
        executorEntreySmalls.Clear();
    }
}
