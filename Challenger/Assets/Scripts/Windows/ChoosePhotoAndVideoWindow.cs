﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class ChoosePhotoAndVideoWindow : WindowMonoBehaviour {
    public event Action<Sprite> OnChoosePhoto = ( Sprite arg ) => { };

    [SerializeField]
    Button backBlockButton;
    [SerializeField]
    Button downArrowButton;
    [SerializeField]
    ChoosePhotoPanel[] photoButton;

    float contentSizeY;

    ChoosePhotoPanel choosePhotoPanel;

    private void Start () {
        contentSizeY = contentRectTransform.rect.height;
        contentRectTransform.anchoredPosition = new Vector2( 0, -contentSizeY );

        backBlockButton.onClick.AddListener( StartHide );
        downArrowButton.onClick.AddListener( StartHide );

        base.StartHide();
        backBlockButton.gameObject.SetActive( false );

        for ( int i = 0; i < photoButton.Length; i++ ) {
            photoButton[i].Init();
            photoButton[i].OnPhotoClick += OnPhotoClick;
        }
    }

    internal override void StartShow () {
        base.StartShow();
        contentRectTransform.DOKill();
        contentRectTransform.DOAnchorPosY( 0f, timeShowWindow );
        backBlockButton.gameObject.SetActive( true );
    }

    internal override void StartHide () {
        backBlockButton.gameObject.SetActive( false );

        contentRectTransform.DOKill();
        contentRectTransform.DOAnchorPosY( -contentSizeY, timeShowWindow ).OnComplete( () => {
            base.StartHide();
        } );
    }

    void OnPhotoClick ( ChoosePhotoPanel choosePhotoPanel ) {
        if ( this.choosePhotoPanel == choosePhotoPanel ) {
            this.choosePhotoPanel.UnChoosePhoto();
            this.choosePhotoPanel = null;

            OnChoosePhoto( null );
        }
        else if ( this.choosePhotoPanel != null ) {
            this.choosePhotoPanel.UnChoosePhoto();
            choosePhotoPanel.ChoosePhoto();
            this.choosePhotoPanel = choosePhotoPanel;

            OnChoosePhoto( this.choosePhotoPanel.GetSpritePhoto() );
        }
        else {
            choosePhotoPanel.ChoosePhoto();
            this.choosePhotoPanel = choosePhotoPanel;

            OnChoosePhoto( this.choosePhotoPanel.GetSpritePhoto() );
        }
    }
}
