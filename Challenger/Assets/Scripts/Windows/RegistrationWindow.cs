﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;
using DG.Tweening;

public class RegistrationWindow : WindowMonoBehaviour {
    [SerializeField]
    InputField usernameInputField;
    [SerializeField]
    InputField passworInputFieldd;
    [SerializeField]
    Button registrationButton;
    [SerializeField]
    Button backButton;
    [SerializeField]
    GameObject errorPanel;
    [SerializeField]
    Text errortext;

    [Inject]
    SignalBus signalBus;
    [Inject]
    NetworkController networkController;

    float contentSizeX;

    void Start() {
        contentSizeX = contentRectTransform.rect.width;
        contentRectTransform.anchoredPosition = new Vector2( contentSizeX, 0 );

        backButton.onClick.AddListener( StartHide );
        registrationButton.onClick.AddListener( Registration );

        HideErrorPanel();

        base.StartHide();
    }

    void Registration() {
        if( usernameInputField.text == "" || passworInputFieldd.text == "" ) {
            ShowErrorPanel( "введите имя пользователя и пароль" );
            return;
        }

        WWWForm form = new WWWForm();
        form.AddField( "username", usernameInputField.text );
        form.AddField( "password", passworInputFieldd.text );

        networkController.Post( "api/v1/auth/registration", form,
        action: ( response ) => {
            StartHide();

            RegistrationResponseDto requese = JsonUtility.FromJson<RegistrationResponseDto>( response );

            signalBus.Fire( new OnRegistrationSucces() {
                username = requese.username,
                password = requese.password
            } );
        },
        actionError: ( responseError ) => {
            ShowErrorPanel( "пользователь с таким именем уже существует" );
        } );
    }

    internal override void StartShow() {
        HideErrorPanel();
        base.StartShow();
        contentRectTransform.DOKill();
        contentRectTransform.DOAnchorPosX( 0f, timeShowWindow );
        backBlockImage.gameObject.SetActive( true );
    }

    internal override void StartHide() {
        backBlockImage.gameObject.SetActive( false );

        contentRectTransform.DOKill();
        contentRectTransform.DOAnchorPosX( contentSizeX, timeShowWindow ).OnComplete( () => {
            base.StartHide();
        } );
    }

    void ShowErrorPanel( string text ) {
        errorPanel.gameObject.SetActive( true );
        errortext.text = text;
    }

    void HideErrorPanel() {
        errorPanel.gameObject.SetActive( false );
    }
}
