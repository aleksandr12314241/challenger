﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ShopTab : Tab {
    [SerializeField]
    Button moreFramesButton;
    [SerializeField]
    Button moreStickersButton;
    [SerializeField]
    ShopItemsWindow shopFramesWindow;
    [SerializeField]
    ShopItemsWindow shopStickersWindow;

    //TODO: временные, что бы открыть челлендж
    [SerializeField]
    Button buttonApple;
    [SerializeField]
    Button buttonTravle;
    [Inject]
    ServController servController;
    ChallengeInfoWindow challengeEntreyInfoWindow;

    internal override void Init() {
        moreFramesButton.onClick.AddListener( MoreFramesClick );
        moreStickersButton.onClick.AddListener( MoreStickersClick );

        challengeEntreyInfoWindow = FindObjectOfType<ChallengeInfoWindow>();

        buttonApple.onClick.AddListener( OpneChallenge );
        buttonTravle.onClick.AddListener( OpneChallenge );
    }

    protected override void ApplyActionShowTab() {
    }

    protected override void ApplyActionHideTab() {
    }

    void MoreFramesClick() {
        shopFramesWindow.StartShow();
    }
    void MoreStickersClick() {
        shopStickersWindow.StartShow();
    }

    void OpneChallenge() {
        ChallengeServ challengeServ = servController.GetFirstChallengeServ();

        if( challengeServ == null ) {
            return;
        }
        challengeEntreyInfoWindow.StartShow( challengeServ );
    }
}
