﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class CreateChallengeTab : Tab {
    [SerializeField]
    Button createChallengeButton;
    [SerializeField]
    ExecutionEntry executionEntryPrf;
    [SerializeField]
    CreateChallengeWindow createChallengeSubTab;
    [SerializeField]
    RectTransform executionContent;
    [SerializeField]
    GameObject waitArrow;
    [SerializeField]
    Text tittleNoChallengeText;

    List<ExecutionEntry> executionsData;

    internal override void Init() {
        createChallengeButton.onClick.AddListener( ShowCreateChallengeSubTab );
    }

    protected override void ApplyActionShowTab() {
        ShowChallengeList_Temp();
    }

    protected override void ApplyActionHideTab() {
        CancelInvoke( "GetChallengeList" );
    }

    void ShowCreateChallengeSubTab() {
        createChallengeSubTab.StartShow();
    }

    void GetChallengeList() {
        //TODO: добавить получение понравившихся челленджей с сервера
        //ShowChallengeList();
    }

    void ShowChallengeList( ExecutionServ[] executionServ ) {
        waitArrow.gameObject.SetActive( false );

        if( executionsData == null || executionServ.Length <= 0 ) {
            tittleNoChallengeText.gameObject.SetActive( true );
        }

        tittleNoChallengeText.gameObject.SetActive( false );
        this.executionsData = new List<ExecutionEntry>();
        for( int i = 0; i < executionServ.Length; i++ ) {
            ExecutionEntry executionEntryTemp = Instantiate( executionEntryPrf, executionContent );
            executionEntryTemp.Init( executionServ[i] );
            this.executionsData.Add( executionEntryTemp );
        }
    }

    //TODO:
    void ShowChallengeList_Temp() {
        if( executionsData != null ) {
            return;
        }

        waitArrow.gameObject.SetActive( true );
        Invoke( "GetChallengeList", 4 );
    }
}
