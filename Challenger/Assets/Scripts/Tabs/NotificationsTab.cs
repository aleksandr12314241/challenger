﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class NotificationsTab : Tab {
    [SerializeField]
    RectTransform contentRectTransform;
    [SerializeField]
    NotificationLikeEntity notificationLikeEntryPrf;
    [SerializeField]
    NotificationCommentEntity notificationCommentEntryPrf;
    [SerializeField]
    NotificationSubscribeEntity notificationSubscribeEntityPrf;
    [SerializeField]
    NotificationCallOnChallengeEntity notificationCallOnChallengeEntryPrf;
    [SerializeField]
    NotificationWinChallengeEntity notificationWinEntityPrf;

    List<NotificationEntity> notifications;

    internal override void Init() {
    }

    protected override void ApplyActionShowTab() {
        //ShowActualNaotification_Temp();
    }

    protected override void ApplyActionHideTab() {
    }

    void ShowActualNaotification( NotificationDatas notificationDatas ) {
        List<NotificationEntity> notificationsTemp = new List<NotificationEntity>();

        for( int i = 0; i < notificationDatas.notificationLikeDatas.Length; i++ ) {
            NotificationLikeEntity notificationLikeEntityTemp = Instantiate( notificationLikeEntryPrf );
            notificationLikeEntityTemp.Init( notificationDatas.notificationLikeDatas[i] );

            notificationsTemp.Add( notificationLikeEntityTemp );
        }

        for( int i = 0; i < notificationDatas.notificationCommentDatas.Length; i++ ) {
            NotificationCommentEntity notificationCommentEntityTemp = Instantiate( notificationCommentEntryPrf );
            notificationCommentEntityTemp.Init( notificationDatas.notificationCommentDatas[i] );

            notificationsTemp.Add( notificationCommentEntityTemp );
        }

        for( int i = 0; i < notificationDatas.notificationSubscriberDatas.Length; i++ ) {
            NotificationSubscribeEntity notificationSubscribeEntityTemp = Instantiate( notificationSubscribeEntityPrf );
            notificationSubscribeEntityTemp.Init( notificationDatas.notificationSubscriberDatas[i] );
            notificationSubscribeEntityTemp.name = notificationSubscribeEntityPrf + "_" + i;

            notificationsTemp.Add( notificationSubscribeEntityTemp );
        }

        for( int i = 0; i < notificationDatas.notificationCallOnChallengeDatas.Length; i++ ) {
            NotificationCallOnChallengeEntity notificationCallOnChallengeEntityTemp = Instantiate( notificationCallOnChallengeEntryPrf );
            notificationCallOnChallengeEntityTemp.Init( notificationDatas.notificationCallOnChallengeDatas[i] );
            notificationCallOnChallengeEntityTemp.name = notificationCallOnChallengeEntryPrf.name + "_" + i;

            notificationsTemp.Add( notificationCallOnChallengeEntityTemp );
        }

        for( int i = 0; i < notificationDatas.notificationWinChallengeDatas.Length; i++ ) {
            NotificationWinChallengeEntity notificationWinEntityTemp = Instantiate( notificationWinEntityPrf );
            notificationWinEntityTemp.Init( notificationDatas.notificationWinChallengeDatas[i] );
            notificationWinEntityTemp.name = notificationWinEntityPrf.name + "_" + i;

            notificationsTemp.Add( notificationWinEntityTemp );
        }

        notifications = notificationsTemp.OrderByDescending( x => x.NotificationData.notoficationTime ).ToList();
        for( int i = 0; i < notifications.Count; i++ ) {
            notifications[i].transform.SetParent( contentRectTransform, false );
        }
    }

    void AddNotificationLike( NotificationLikeServ notificationLikeData ) {
        NotificationLikeEntity notificationLikeEntityTemp = Instantiate( notificationLikeEntryPrf );
        notificationLikeEntityTemp.Init( notificationLikeData );
        notificationLikeEntityTemp.transform.SetParent( contentRectTransform, false );

        notifications.Insert( 0, notificationLikeEntityTemp );
    }

    void AddNotificationComment() {

    }

    void AddNotificationSubscribe() {

    }

    void AddNotificationCallOnChallenge() {

    }

    void AddNotificationWin() {

    }
}
