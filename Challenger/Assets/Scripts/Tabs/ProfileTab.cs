﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ProfileTab : Tab {
    [SerializeField]
    Button editorButton;
    [SerializeField]
    Image avatarImage;
    [SerializeField]
    Button followButton;
    [SerializeField]
    Image followDoneImage;
    [SerializeField]
    Text profileNameText;
    [SerializeField]
    Text profileDescriptionText;
    [SerializeField]
    GridLayoutGroup postsGridLayoutGroup;
    //[SerializeField]
    //ChallengeEntitySmall challengeEntitySmallPrf;
    //[SerializeField]
    //ExecutorEntitySmall executorEntitySmallPrf;
    [SerializeField]
    GameObject arrowAvatarGameObject;
    [SerializeField]
    GameObject arrowPostGameObject;

    [Inject]
    NetworkController networkController;
    [Inject]
    ServController servController;
    [Inject]
    Profile profile;
    [Inject]
    SpriteLoader spriteLoader;
    [Inject]
    readonly ChallengeEntitySmall.Factory challengeEntitySmall;
    [Inject]
    readonly ExecutionEntitySmall.Factory executionEntitySmall;

    ChangeProfileWindow changeProfileWindow;
    ChallengeInfoWindow challengeInfoWindow;
    ExecutionInfoWindow executorInfoWindow;

    List<EntityPanelSmall> entityPanelSmall;

    UserServ userServ;

    internal override void Init() {
        editorButton.onClick.AddListener( EditorClick );
        followButton.onClick.AddListener( FollowClick );

        changeProfileWindow = FindObjectOfType<ChangeProfileWindow>();
        challengeInfoWindow = FindObjectOfType<ChallengeInfoWindow>();
        executorInfoWindow = FindObjectOfType<ExecutionInfoWindow>();
    }

    protected override void ApplyActionShowTab() {
        if( entityPanelSmall != null ) {
            return;
        }

        entityPanelSmall = new List<EntityPanelSmall>();

        arrowPostGameObject.gameObject.SetActive( true );
        arrowAvatarGameObject.gameObject.SetActive( true );

        postsGridLayoutGroup.childAlignment = TextAnchor.UpperCenter;

        profileNameText.text = "";
        profileDescriptionText.text = "";

        editorButton.gameObject.SetActive( false );

        networkController.Get( "profile/" + profile.Id, ( response ) => {
            ProfileResponseDto profileResponseDto =
                JsonUtility.FromJson<ProfileResponseDto>( response );

            UserServ userServ = servController.GetUserServ( profileResponseDto.userResponseDto );

            ShowProfile( userServ );

            List<ChallengeServ> challengeServs = new List<ChallengeServ>();
            for( int i = 0; i < profileResponseDto.challengesAndExecutionsResponseDto.challenges.Count; i++ ) {
                ChallengeServ challengeServ = servController.GetChallengeServ( profileResponseDto.challengesAndExecutionsResponseDto.challenges[i] );
                challengeServs.Add( challengeServ );
            }

            List<ExecutionServ> executionServs = new List<ExecutionServ>();
            for( int i = 0; i < profileResponseDto.challengesAndExecutionsResponseDto.executions.Count; i++ ) {
                ExecutionServ executionServ = servController.GetExecutionServ( profileResponseDto.challengesAndExecutionsResponseDto.executions[i] );
                executionServs.Add( executionServ );
            }

            ShowPosts( challengeServs, executionServs );
        } );
    }

    protected override void ApplyActionHideTab() {
    }

    void EditorClick() {
        changeProfileWindow.StartShow( userServ );
    }

    void FollowClick() {
        followButton.gameObject.SetActive( false );
        followDoneImage.gameObject.SetActive( true );
    }

    void ShowProfile( UserServ userServ ) {
        this.userServ = userServ;

        editorButton.gameObject.SetActive( true );
        arrowAvatarGameObject.gameObject.SetActive( true );

        spriteLoader.LoadSpriteAvatar( userServ.filename, ( sprite ) => {
            avatarImage.sprite = sprite;

            arrowAvatarGameObject.gameObject.SetActive( false );
        } );

        profileNameText.text = "@" + userServ.username;
        //profileDescriptionText.text = profileData.aboutMeText;

        followButton.gameObject.SetActive( false );
    }

    void ShowPosts( List<ChallengeServ> challenges, List<ExecutionServ> executions ) {
        arrowPostGameObject.gameObject.SetActive( false );

        postsGridLayoutGroup.childAlignment = TextAnchor.UpperLeft;

        int postsCount = challenges.Count + executions.Count;
        int indexChallenge = 0;
        int indexExecutor = 0;
        float additionalSizeEntry = 0;

        for( int i = 0; i < postsCount; i++ ) {
            if( indexChallenge >= challenges.Count ) {
                ExecutionEntitySmall executeEntryTemp = CreateExecutePost( executions[indexExecutor], true );
                executeEntryTemp.transform.SetAsFirstSibling();
                additionalSizeEntry += executeEntryTemp.MyRectTransform.sizeDelta.y;

                indexExecutor++;
                continue;
            }
            else if( indexExecutor >= executions.Count ) {
                ChallengeEntitySmall challengeEntreyTemp = CreateChallengePost( challenges[indexChallenge], true );
                challengeEntreyTemp.transform.SetAsFirstSibling();
                additionalSizeEntry += challengeEntreyTemp.MyRectTransform.sizeDelta.y;

                indexChallenge++;
                continue;
            }

            if( challenges[indexChallenge].creationDate < executions[indexExecutor].creationDate ) {
                ChallengeEntitySmall challengeEntreyTemp = CreateChallengePost( challenges[indexChallenge], true );
                challengeEntreyTemp.transform.SetAsFirstSibling();
                additionalSizeEntry += challengeEntreyTemp.MyRectTransform.sizeDelta.y;

                indexChallenge++;
            }
            else {
                ExecutionEntitySmall executeEntryTemp = CreateExecutePost( executions[indexExecutor], true );
                executeEntryTemp.transform.SetAsFirstSibling();
                additionalSizeEntry += executeEntryTemp.MyRectTransform.sizeDelta.y;

                indexExecutor++;
            }
        }
    }

    internal void OnUpdateProfile( UserResponseDto userResponseDto ) {
        UserServ userServ = servController.GetUserServ( userResponseDto );

        ShowProfile( userServ );
    }

    ChallengeEntitySmall CreateChallengePost( ChallengeServ challenge, bool setAsFirst = false ) {
        ChallengeEntitySmall challengeEntityTemp = challengeEntitySmall.Create();
        challengeEntityTemp.transform.SetParent( postsGridLayoutGroup.transform, false );

        challengeEntityTemp.Init( challenge );

        challengeEntityTemp.OnClickChallengeEntreySmall += OnClickChallengeEntreySmall;

        if( setAsFirst ) {
            entityPanelSmall.Add( challengeEntityTemp );
        }
        else {
            entityPanelSmall.Insert( 0, challengeEntityTemp );
        }

        return challengeEntityTemp;
    }

    ExecutionEntitySmall CreateExecutePost( ExecutionServ execution, bool setAsFirst = false ) {
        ExecutionEntitySmall executeEntityTemp = executionEntitySmall.Create();
        executeEntityTemp.transform.SetParent( postsGridLayoutGroup.transform, false );

        executeEntityTemp.Init( execution );

        executeEntityTemp.OnExecutionEntityPanelSmallClick += OnEntreyPanelSmallClick;

        if( setAsFirst ) {
            entityPanelSmall.Add( executeEntityTemp );
        }
        else {
            entityPanelSmall.Insert( 0, executeEntityTemp );
        }

        return executeEntityTemp;
    }

    void OnClickChallengeEntreySmall( ChallengeEntitySmall challengeEntitySmall ) {
        challengeInfoWindow.StartShow( challengeEntitySmall.challenge );
    }

    void OnEntreyPanelSmallClick( ExecutionSmallInChallengeInfo executorEntreySmall ) {
        executorInfoWindow.StartShow( executorEntreySmall.Execution );
    }
}
