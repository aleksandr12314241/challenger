﻿using UnityEngine;

public abstract class Tab: MonoBehaviour {

    internal bool IsShown {
        private set;
        get;
    }

    internal abstract void Init ();

    internal void ShowTab () {
        if ( IsShown ) {
            return;
        }

        IsShown = true;
        gameObject.SetActive( true );

        ApplyActionShowTab ();
    }

    protected abstract void ApplyActionShowTab ();

    internal void HideForce () {
        IsShown = false;
        gameObject.SetActive( false );

        ApplyActionHideTab();
    }

    internal void HideTab () {
        if ( !IsShown ) {
            return;
        }

        IsShown = false;
        gameObject.SetActive( false );

        ApplyActionHideTab();
    }

    protected abstract void ApplyActionHideTab ();
}