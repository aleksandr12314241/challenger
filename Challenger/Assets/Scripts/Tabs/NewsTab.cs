﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class NewsTab : Tab {
    [SerializeField]
    RectTransform newsContentRectTransform;
    [SerializeField]
    TimelineChallenge challengeEntrey;
    [SerializeField]
    TimelineExecution executeEntry;

    List<PostInfo> posts = new List<PostInfo>();
    VerticalLayoutGroup verticalLayoutGroupContent;

    [Inject]
    NetworkController networkController;
    [Inject]
    Profile profile;
    [Inject]
    ServController servController;
    [Inject]
    readonly TimelineChallenge.Factory challengeFactory;
    [Inject]
    readonly TimelineExecution.Factory executionFactory;

    internal override void Init() {
        newsContentRectTransform.sizeDelta = new Vector2( newsContentRectTransform.sizeDelta.x, 0f );
    }

    protected override void ApplyActionShowTab() {
        //NetworkManager.instance.Send( "getActualNews", null, ShowActualNews );
    }

    protected override void ApplyActionHideTab() {

    }

    public void StartSurfChallenger() {
        ShowActualNews();
    }

    void ShowActualPosts( ChallengeServ[] challenges, ExecutionServ [] executions ) {
        int postsCount = challenges.Length + executions.Length;
        int indexChallenge = 0;
        int indexExecuton = 0;
        float additionalSizeEntry = 0;

        for( int i = 0; i < postsCount; i++ ) {
            if( indexChallenge >= challenges.Length ) {
                TimelineExecution executeEntryTemp = CreatePostExecution( executions[indexExecuton] );
                executeEntryTemp.transform.SetAsFirstSibling();
                additionalSizeEntry += executeEntryTemp.MyRectTransform.sizeDelta.y;

                indexExecuton++;
                continue;
            }
            else if( indexExecuton >= executions.Length ) {
                TimelineChallenge challengeEntreyTemp = CreatePostChallenge( challenges[indexChallenge], true );
                challengeEntreyTemp.transform.SetAsFirstSibling();
                additionalSizeEntry += challengeEntreyTemp.MyRectTransform.sizeDelta.y;

                indexChallenge++;
                continue;
            }

            if( challenges[indexChallenge].creationDate < executions[indexExecuton].creationDate ) {
                TimelineChallenge challengeEntreyTemp = CreatePostChallenge( challenges[indexChallenge], true );
                challengeEntreyTemp.transform.SetAsFirstSibling();
                additionalSizeEntry += challengeEntreyTemp.MyRectTransform.sizeDelta.y;

                indexChallenge++;
            }
            else {
                TimelineExecution executeEntryTemp = CreatePostExecution( executions[indexExecuton] );
                executeEntryTemp.transform.SetAsFirstSibling();
                additionalSizeEntry += executeEntryTemp.MyRectTransform.sizeDelta.y;

                indexExecuton++;
            }
        }

        //newsContentRectTransform.sizeDelta += new Vector2( 0f, additionalSizeEntry + verticalLayoutGroupContent.spacing );
    }

    TimelineChallenge CreatePostChallenge( ChallengeServ challenge, bool setAsFirst = false ) {
        TimelineChallenge challengeEntreyTemp = challengeFactory.Create();
        challengeEntreyTemp.transform.SetParent( newsContentRectTransform, false );
        challengeEntreyTemp.transform.SetAsFirstSibling();

        challengeEntreyTemp.Init( challenge );

        if( setAsFirst ) {
            posts.Add( challengeEntreyTemp );
        }
        else {
            posts.Insert( 0, challengeEntreyTemp );
        }

        return challengeEntreyTemp;
    }

    TimelineExecution CreatePostExecution( ExecutionServ execution ) {
        TimelineExecution executeEntryTemp = executionFactory.Create();
        executeEntryTemp.transform.SetParent( newsContentRectTransform, false );
        executeEntryTemp.transform.SetAsFirstSibling();

        executeEntryTemp.Init( execution );

        posts.Add( executeEntryTemp );

        //if ( setAsFirst ) {
        //    posts.Add( executeEntryTemp );
        //}
        //else {
        //    posts.Insert( 0, executeEntryTemp );
        //}

        return executeEntryTemp;
    }

    public void OnAddNewChallenge( ChallengeResponseDto challenge ) {
        ChallengeServ challengeServ = servController.GetChallengeServ( challenge );
        TimelineChallenge challengeEntreyTemp = CreatePostChallenge( challengeServ );
    }

    public void OnAddNewExecution( ExecutionResponseDto execution ) {
        ExecutionServ executionServ = servController.GetExecutionServ( execution );
        TimelineExecution challengeEntreyTemp = CreatePostExecution( executionServ );
    }

    internal void OnUpdateProfile( UserResponseDto userResponseDto ) {
        List<PostInfo> postsInfo = posts.FindAll( x => x.Author.id == userResponseDto.id );
        UserServ userServ = servController.GetUserServ( userResponseDto );

        for( int i = 0; i < postsInfo.Count; i++ ) {
            postsInfo[i].UpdateAuthorInfo( userServ );
        }
    }

    void ShowActualNews() {
        networkController.Get( "challengeandexecution", ( response ) => {
            ChallengesAndExecutionsResponseDto challengesAndExecutionsResponseDto =
                JsonUtility.FromJson<ChallengesAndExecutionsResponseDto>( response );

            ChallengeServ[] challengesServ = new ChallengeServ[challengesAndExecutionsResponseDto.challenges.Count];
            for( int i = 0; i < challengesAndExecutionsResponseDto.challenges.Count; i++ ) {
                challengesServ[i] = servController.GetChallengeServ( challengesAndExecutionsResponseDto.challenges[i] );
            }

            ExecutionServ[] executionsServ = new ExecutionServ[challengesAndExecutionsResponseDto.executions.Count];

            for( int i = 0; i < challengesAndExecutionsResponseDto.executions.Count; i++ ) {
                executionsServ[i] = servController.GetExecutionServ( challengesAndExecutionsResponseDto.executions[i] );
            }

            ShowActualPosts( challengesServ, executionsServ );
        } );
    }
}
