﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class GlobalFindTab: Tab {
    [SerializeField]
    RectTransform contentRectTransform;
    [SerializeField]
    ChallengeEntitySmall challengeEntreySmallPrf;
    [SerializeField]
    ChallengeEntityMiddle challengeEntreyMiddlePrf;
    [SerializeField]
    RectTransform challengeInfoPanelRectTransform;
    [SerializeField]
    RectTransform challengersSVRectTransform;
    [SerializeField]
    RectTransform challengersContent;
    [SerializeField]
    ChallengeInfoPanelInGlobalNews challengeInfoPanelInGlobalNews;

    float timeShowChallengePanel;
    float startChallengeInfoPanelY;
    float endChallengeInfoPanelY;
    float startChallengeSVY;
    float endChallengeSVY;

    LayoutElement layoutElement;

    List<ChallengeEntitySmall> posts;

    RectTransform challengeEntreySmallContainer;
    ChallengeEntitySmall currentChallengeEntreySmall;

    internal override void Init() {
        CreateNewChallengeEntreySmallContainer();
        challengeInfoPanelInGlobalNews.Init();
        challengeInfoPanelInGlobalNews.OnCloseChallengeInfo += OnCloseChallengeInfo;

        timeShowChallengePanel = 0.2f;
        startChallengeInfoPanelY = 700f;
        endChallengeInfoPanelY = -196f;
        startChallengeSVY = 0;
        endChallengeSVY = -855f;

        challengeInfoPanelRectTransform.anchoredPosition = new Vector2( challengersSVRectTransform.anchoredPosition.x, startChallengeInfoPanelY );
    }

    protected override void ApplyActionShowTab() {
        ShowActualGlobalNews_Temp();
    }

    protected override void ApplyActionHideTab() {

    }

    void ShowActualPosts( ChallengeServ[] challenges ) {
        int postsCount = challenges.Length;// + globalNewsPosts.postsChallengeMiddleData.Length;
        int indexSmall = 0;
        int indexMiddle = 0;
        float additionalSizeEntry = 0;

        for( int i = 0; i < challenges.Length; i++ ) {
            if( i % 3 == 0 ) {
                ChallengeEntityMiddle challengeEntreyMiddleTemp =
                    CreateChallengeEntreyMiddle( challenges[i] );
                additionalSizeEntry += challengeEntreyMiddleTemp.MyRectTransform.sizeDelta.y;
                continue;
            }

            ChallengeEntitySmall challengeEntreySmallTemp = CreateChallengeEntreySmall( challenges[indexSmall] );
            challengeEntreySmallTemp.transform.SetAsLastSibling();
            additionalSizeEntry += challengeEntreySmallTemp.MyRectTransform.sizeDelta.y;
        }

        //for( int i = 0; i < postsCount; i++ ) {
        //    if( indexSmall >= globalNewsPosts.postsChallengeSmallData.Length ) {
        //        ChallengeEntityMiddle challengeEntreyMiddle = CreateChallengeEntreyMiddle( globalNewsPosts.postsChallengeMiddleData[indexMiddle] );
        //        additionalSizeEntry += challengeEntreyMiddle.MyRectTransform.sizeDelta.y;

        //        indexMiddle++;
        //        continue;
        //    }
        //    else if( indexMiddle >= globalNewsPosts.postsChallengeMiddleData.Length ) {
        //        ChallengeEntitySmall challengeEntreySmallTemp = CreateChallengeEntreySmall( globalNewsPosts.postsChallengeSmallData[indexSmall] );
        //        additionalSizeEntry += challengeEntreySmallTemp.MyRectTransform.sizeDelta.y;

        //        indexSmall++;
        //        continue;
        //    }

        //    if( globalNewsPosts.postsChallengeMiddleData[indexMiddle].postTime < globalNewsPosts.postsChallengeSmallData[indexSmall].postTime &&
        //            challengeEntreySmallContainer.childCount >= 3 ) {
        //        ChallengeEntityMiddle challengeEntreyMiddleTemp =
        //            CreateChallengeEntreyMiddle( globalNewsPosts.postsChallengeMiddleData[indexMiddle] );
        //        additionalSizeEntry += challengeEntreyMiddleTemp.MyRectTransform.sizeDelta.y;

        //        indexMiddle++;
        //    }
        //    else {
        //        ChallengeEntitySmall challengeEntreySmallTemp = CreateChallengeEntreySmall( globalNewsPosts.postsChallengeSmallData[indexSmall] );
        //        challengeEntreySmallTemp.transform.SetAsLastSibling();
        //        additionalSizeEntry += challengeEntreySmallTemp.MyRectTransform.sizeDelta.y;

        //        indexSmall++;
        //    }
        //}
    }

    ChallengeEntitySmall CreateChallengeEntreySmall( ChallengeServ challenge ) {
        if( challengeEntreySmallContainer.childCount >= 3 ) {
            CreateNewChallengeEntreySmallContainer();
        }
        ChallengeEntitySmall challengeEntreyTemp = Instantiate( challengeEntreySmallPrf, challengeEntreySmallContainer );
        challengeEntreyTemp.Init( challenge );
        posts.Add( challengeEntreyTemp );
        challengeEntreyTemp.OnClickChallengeEntreySmall += OnClickChallengeEntreySmall;

        challengeEntreyTemp.MyRectTransform.anchoredPosition = new Vector2( -261f + ( challengeEntreySmallContainer.childCount - 1 ) * 261f, 0f );
        return challengeEntreyTemp;
    }

    ChallengeEntityMiddle CreateChallengeEntreyMiddle( ChallengeServ challenge ) {
        ChallengeEntityMiddle challengeEntreyTemp = Instantiate( challengeEntreyMiddlePrf, contentRectTransform );
        challengeEntreyTemp.Init( challenge );
        posts.Add( challengeEntreyTemp );
        challengeEntreyTemp.OnClickChallengeEntreySmall += OnClickChallengeEntreySmall;

        return challengeEntreyTemp;
    }

    void CreateNewChallengeEntreySmallContainer() {
        GameObject challengeEntreySmallContainerGo = new GameObject();
        challengeEntreySmallContainer = challengeEntreySmallContainerGo.AddComponent<RectTransform>();
        challengeEntreySmallContainer.SetParent( contentRectTransform, false );
        challengeEntreySmallContainer.sizeDelta =
            new Vector2( challengeEntreyMiddlePrf.MyRectTransform.sizeDelta.x, challengeEntreySmallPrf.MyRectTransform.sizeDelta.y );
        challengeEntreySmallContainer.localScale = Vector3.one;
        challengeEntreySmallContainer.anchorMin = new Vector2( 0.5f, 1f );
        challengeEntreySmallContainer.anchorMax = new Vector2( 0.5f, 1f );
    }

    void OnClickChallengeEntreySmall( ChallengeEntitySmall challengeEntreySmall ) {
        if( currentChallengeEntreySmall == null ) {
            challengeInfoPanelRectTransform.DOKill();
            challengeInfoPanelRectTransform.gameObject.SetActive( true );
            challengeInfoPanelRectTransform.DOAnchorPosY( endChallengeInfoPanelY, timeShowChallengePanel );

            challengeEntreySmall.ShowChoose();
            currentChallengeEntreySmall = challengeEntreySmall;

            challengersSVRectTransform.DOAnchorPosY( endChallengeSVY, timeShowChallengePanel );

            float executorsContentOffset = startChallengeInfoPanelY - endChallengeInfoPanelY;
            challengersContent.sizeDelta =
                new Vector2( challengersContent.sizeDelta.x, challengersContent.sizeDelta.y + executorsContentOffset );
            challengersContent.anchoredPosition =
                new Vector2( challengersContent.anchoredPosition.x, -challengeEntreySmall.GetPosY() / 2f );

            challengeInfoPanelInGlobalNews.StartShow( challengeEntreySmall );

            //executorsContent.DOSizeDelta( new Vector2( executorsContent.sizeDelta.x, executorsContent.sizeDelta.y + executorsContentOffset ),
            //                              timeShowExecutorsPanel );
            //executorsContent.DOAnchorPosY( executorsContent.sizeDelta.y + executorsContentOffset, timeShowExecutorsPanel );
        }
        else if( currentChallengeEntreySmall == challengeEntreySmall ) {
            OnCloseChallengeInfo();
        }
        else {
            challengeInfoPanelRectTransform.DOKill();

            challengeInfoPanelRectTransform.DOAnchorPosY( startChallengeInfoPanelY, timeShowChallengePanel ).OnComplete( () => {
                challengeInfoPanelInGlobalNews.StartShow( challengeEntreySmall );
                challengeInfoPanelRectTransform.DOAnchorPosY( endChallengeInfoPanelY, timeShowChallengePanel );
            } );

            currentChallengeEntreySmall.HideChoose();
            currentChallengeEntreySmall = challengeEntreySmall;
            currentChallengeEntreySmall.ShowChoose();
        }
    }

    void OnCloseChallengeInfo() {
        challengeInfoPanelRectTransform.DOKill();
        currentChallengeEntreySmall.HideChoose();

        challengeInfoPanelRectTransform.DOAnchorPosY( startChallengeInfoPanelY, timeShowChallengePanel ).OnComplete( () => {
            challengeInfoPanelRectTransform.gameObject.SetActive( false );
        } );

        challengersSVRectTransform.DOAnchorPosY( startChallengeSVY, timeShowChallengePanel );

        float executorsContentOffset = startChallengeInfoPanelY - endChallengeInfoPanelY;
        challengersContent.sizeDelta =
            new Vector2( challengersContent.sizeDelta.x, challengersContent.sizeDelta.y - executorsContentOffset );
        challengersContent.anchoredPosition =
            new Vector2( challengersContent.anchoredPosition.x, -currentChallengeEntreySmall.GetPosY() / 2f );

        currentChallengeEntreySmall = null;

        //executorsContent.DOSizeDelta( new Vector2( executorsContent.sizeDelta.x, executorsContent.sizeDelta.y - executorsContentOffset ),
        //                              timeShowExecutorsPanel );
        //executorsContent.DOAnchorPosY( executorsContent.sizeDelta.y - executorsContentOffset, timeShowExecutorsPanel );
    }

    //TODO: Temps
    void ShowActualGlobalNews_Temp() {
        if( posts != null ) {
            return;
        }

        posts = new List<ChallengeEntitySmall>();
        //ShowActualPosts(s);
    }
}
