﻿using UnityEngine;
using Zenject;

[CreateAssetMenu( fileName = "Challenge Settings Installer", menuName = "Challenger/Challenge Settings Installer" )]
public class ChallengeSettingsInstaller : ScriptableObjectInstaller<ChallengeSettingsInstaller> {
    //
    [SerializeField]
    public ChallengerPrefabsDataSet challengerPrefabsDataSet;
    [SerializeField]
    public CommonSpritesDataSet commonSpritesDataSet;

    public override void InstallBindings() {
        Container.BindInstances( challengerPrefabsDataSet );
        Container.BindInstances( commonSpritesDataSet );
        //Container.BindInstances( _characterVisualDataSet );
        //Container.BindInstances( _fieldObjPoolDataSet );
        //Container.BindInstances( _inventoryDataSet );
    }
}
