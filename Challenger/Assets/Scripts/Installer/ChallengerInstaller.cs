﻿using Zenject;
using UnityEngine;

public class ChallengerInstaller : MonoInstaller {
    [Inject]
    ChallengerPrefabsDataSet challengerPrefabsDataSet = null;

    public override void InstallBindings() {
        InstalObjects();

        InitSignal();
    }

    void InstalObjects() {
        Container.Bind<NewsTab>().FromComponentInHierarchy().AsSingle();
        Container.Bind<ProfileTab>().FromComponentInHierarchy().AsSingle();
        Container.Bind<ShopTab>().FromComponentInHierarchy().AsSingle();

        Container.Bind<CreateChallengeWindow>().FromComponentInHierarchy().AsSingle();
        Container.Bind<CreateExecutionWindow>().FromComponentInHierarchy().AsSingle();
        Container.Bind<LoginWindow>().FromComponentInHierarchy().AsSingle();
        Container.Bind<RegistrationWindow>().FromComponentInHierarchy().AsSingle();
        Container.Bind<SaveSystem>().FromComponentInHierarchy().AsSingle();
        Container.Bind<NetworkController>().FromComponentInHierarchy().AsSingle();
        Container.Bind<ChallengeInfoWindow>().FromComponentInHierarchy().AsSingle();
        Container.Bind<ExecutionInfoWindow>().FromComponentInHierarchy().AsSingle();
        Container.Bind<PreChooseWinnerWindow>().FromComponentInHierarchy().AsSingle();
        Container.Bind<ChooseWinnerWindow>().FromComponentInHierarchy().AsSingle();
        Container.Bind<ChangeProfileWindow>().FromComponentInHierarchy().AsSingle();

        Container.Bind<SpriteLoader>().AsSingle();
        Container.Bind<Profile>().AsSingle();
        Container.Bind<ServController>().AsSingle();

        Container.BindFactory<TimelineChallenge, TimelineChallenge.Factory>().FromComponentInNewPrefab( challengerPrefabsDataSet.timelineChallenge );

        Container.BindFactory<TimelineExecution, TimelineExecution.Factory>().FromComponentInNewPrefab( challengerPrefabsDataSet.timelineExecution );

        Container.BindFactory<ExecutionSmallInChallengeInfo, ExecutionSmallInChallengeInfo.Factory>()
        .FromComponentInNewPrefab( challengerPrefabsDataSet.executionSmallInChallengeInfo );

        Container.BindFactory<Comment, Comment.Factory>().FromComponentInNewPrefab( challengerPrefabsDataSet.commentPrf );

        Container.BindFactory<ExecutionEntreySmallPreWin, ExecutionEntreySmallPreWin.Factory>()
        .FromComponentInNewPrefab( challengerPrefabsDataSet.executionEntreySmallPreWin );

        Container.BindFactory<ExecutorEntreySmallWin, ExecutorEntreySmallWin.Factory>()
        .FromComponentInNewPrefab( challengerPrefabsDataSet.executionEntreySmallWin );

        Container.BindFactory<ChallengeEntitySmall, ChallengeEntitySmall.Factory>()
        .FromComponentInNewPrefab( challengerPrefabsDataSet.challengeEntitySmall );

        Container.BindFactory<ExecutionEntitySmall, ExecutionEntitySmall.Factory>()
        .FromComponentInNewPrefab( challengerPrefabsDataSet.executionEntitySmall );

    }

    void InitSignal() {
        SignalBusInstaller.Install( Container );

        Container.DeclareSignal<OnAddNewChallenge>();
        Container.BindSignal<OnAddNewChallenge>().ToMethod<NewsTab>( ( x, s ) => x.OnAddNewChallenge( s.challenge ) ).FromResolve();

        Container.DeclareSignal<OnRegistrationSucces>();
        Container.BindSignal<OnRegistrationSucces>().ToMethod<LoginWindow>( ( x, a ) => x.LoginAfterRegistration( a ) ).FromResolve();
        Container.BindSignal<OnRegistrationSucces>().ToMethod<SaveSystem>( ( x, a ) => x.SaveUsernameAndPassword( a ) ).FromResolve();

        Container.DeclareSignal<OnLoginSuccess>();
        Container.BindSignal<OnLoginSuccess>().ToMethod<SaveSystem>( ( x, s ) => x.SaveToken( s.token ) ).FromResolve();

        Container.DeclareSignal<OnSetProfile>();
        Container.BindSignal<OnSetProfile>().ToMethod<Profile>( ( x, s ) => x.SetMainProfile( s ) ).FromResolve();
        Container.BindSignal<OnSetProfile>().ToMethod<NetworkController>( ( x, s ) => x.SetToken( s.token ) ).FromResolve();
        Container.BindSignal<OnSetProfile>().ToMethod<LoginWindow>( ( x, s ) => x.StartSurfChallenger() ).FromResolve();
        Container.BindSignal<OnSetProfile>().ToMethod<NewsTab>( ( x, s ) => x.StartSurfChallenger() ).FromResolve();
        Container.BindSignal<OnSetProfile>().ToMethod<ServController>( ( x, s ) => x.OnSetProfile( s ) ).FromResolve();

        Container.DeclareSignal<OnUpdateProfile>();
        Container.BindSignal<OnUpdateProfile>().ToMethod<Profile>( ( x, s ) => x.OnUpdateProfile( s.userResponseDto ) ).FromResolve();
        Container.BindSignal<OnUpdateProfile>().ToMethod<ServController>( ( x, s ) => x.OnUpdateProfile( s.userResponseDto ) ).FromResolve();
        Container.BindSignal<OnUpdateProfile>().ToMethod<ProfileTab>( ( x, s ) => x.OnUpdateProfile( s.userResponseDto ) ).FromResolve();
        Container.BindSignal<OnUpdateProfile>().ToMethod<NewsTab>( ( x, s ) => x.OnUpdateProfile( s.userResponseDto ) ).FromResolve();

        Container.DeclareSignal<OnAutoLogin>();
        Container.BindSignal<OnAutoLogin>().ToMethod<LoginWindow>( ( x, a ) => x.AutoLogin( a ) ).FromResolve();

        Container.DeclareSignal<OnAddNewExecution>();
        Container.BindSignal<OnAddNewExecution>().ToMethod<NewsTab>( ( x, a ) => x.OnAddNewExecution( a.executionResponseDto ) ).FromResolve();

        Container.DeclareSignal<OnAddNewChallengeComment>();
        Container.BindSignal<OnAddNewChallengeComment>().ToMethod<ServController>( ( x, a ) => x.OnAddNewChallengeComment( a.comment ) ).FromResolve();
        Container.BindSignal<OnAddNewChallengeComment>().ToMethod<ChallengeInfoWindow>( ( x, a ) => x.OnAddNewChallengeComment( a.comment ) ).FromResolve();

        Container.DeclareSignal<OnAddNewExecutionComment>();
        Container.BindSignal<OnAddNewExecutionComment>().ToMethod<ServController>( ( x, a ) => x.OnAddNewExecutionComment( a.comment ) ).FromResolve();
        Container.BindSignal<OnAddNewExecutionComment>().ToMethod<ExecutionInfoWindow>( ( x, a ) => x.OnAddNewExecutionComment( a.comment ) ).FromResolve();

        Container.DeclareSignal<OnUpdateExecution>();
        Container.BindSignal<OnUpdateExecution>().ToMethod<ServController>( ( x, a ) => x.OnUpdateExecution( a.executionResponseDto ) ).FromResolve();

        Container.DeclareSignal<OnEndChallenge>();
        Container.BindSignal<OnEndChallenge>().ToMethod<ServController>( ( x, a ) => x.OnEndChallenge( a.endChallengeResponseDto ) ).FromResolve();
        Container.BindSignal<OnEndChallenge>().ToMethod<PreChooseWinnerWindow>( ( x, a ) => x.OnEndChallenge() ).FromResolve();
    }
}
