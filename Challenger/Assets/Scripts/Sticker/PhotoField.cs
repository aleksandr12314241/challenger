﻿using UnityEngine;
using System;

public class PhotoField : MonoBehaviour {
    internal Action OnMouseEnterAction = () => { };
    internal Action OnMouseExitAction = () => { };

    [SerializeField]
    BoxCollider2D boxCollider;
    [SerializeField]
    RectTransform maskImage;
    [SerializeField]
    DragStickerOnPhotoField dragStickerOnPhotoFieldPrf;
    [SerializeField]
    RectTransform photoStickerRectTransform;

    private void OnMouseEnter () {
        OnMouseEnterAction();
    }

    private void OnMouseExit () {
        OnMouseExitAction();
    }

    internal StickerOnPhotoFieldEditor AddSticker ( DragSticker dragSticker, Vector2 stickerPos ) {
        DragStickerOnPhotoField dragStickerTemp = Instantiate( dragStickerOnPhotoFieldPrf, photoStickerRectTransform );
        dragStickerTemp.Init( dragSticker.StickerGameObjectData );

        StickerOnPhotoFieldEditor stickerGameObject =
            Instantiate( dragSticker.StickerGameObjectData.stickerOnPhotoFieldEditor, dragStickerTemp.transform );
        dragStickerTemp.transform.position = stickerPos;
        stickerGameObject.Init( dragStickerTemp );

        return stickerGameObject;
    }
}
