﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class TrashField : MonoBehaviour {
    internal Action OnMouseEnterAction = () => { };
    internal Action OnMouseExitAction = () => { };

    [SerializeField]
    RectTransform iconRectTransform;
    [SerializeField]
    BoxCollider2D boxCollider2D;

    float timeAnim = 0.2f;

    private void OnMouseEnter () {
        iconRectTransform.DOScale( new Vector3( 1.1f, 1.1f, 1f ), timeAnim );

        OnMouseEnterAction();
    }

    private void OnMouseExit () {
        iconRectTransform.DOScale( new Vector3( 1f, 1f, 1f ), timeAnim );

        OnMouseExitAction();
    }

    internal void ShowTrashField () {
        gameObject.SetActive( true );
    }

    internal void HideTrashField () {
        gameObject.SetActive( false );
    }
}
