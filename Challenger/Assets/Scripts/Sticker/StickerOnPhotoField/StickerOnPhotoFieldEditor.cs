﻿using UnityEngine;

public abstract class StickerOnPhotoFieldEditor : MonoBehaviour {
    protected RectTransform myRectTransform;
    internal RectTransform MyRectTransform {
        get {
            return myRectTransform;
        }
    }

    internal DragStickerOnPhotoField DragStickerOnPhotoField {
        private set;
        get;
    }

    internal void Init( DragStickerOnPhotoField dragStickerOnPhotoField ) {
        DragStickerOnPhotoField = dragStickerOnPhotoField;
        myRectTransform = GetComponent<RectTransform>();

        SetStart();
    }

    protected abstract void SetStart();

    internal abstract StickerData GetStickerData();
}
