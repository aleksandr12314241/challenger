﻿using UnityEngine;

public abstract class StickerOnPhotoField : MonoBehaviour {
    protected RectTransform myRectTransform;
    internal RectTransform MyRectTransform {
        get {
            return myRectTransform;
        }
    }

    internal void Init ( StickerData stickerData ) {
        myRectTransform = GetComponent<RectTransform>();

        SetStart( stickerData );
    }

    protected abstract void SetStart ( StickerData stickerData );
}
