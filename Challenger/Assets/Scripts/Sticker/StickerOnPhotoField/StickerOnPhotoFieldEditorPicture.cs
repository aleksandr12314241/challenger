﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickerOnPhotoFieldEditorPicture : StickerOnPhotoFieldEditor {
    protected override void SetStart () {
    }

    internal override StickerData GetStickerData () {
        return new StickerPictureData();
    }
}
