﻿using UnityEngine.UI;

public class StickerOnPhotoFieldText : StickerOnPhotoField {
    Text stickerText;

    protected override void SetStart ( StickerData stickerData ) {
        StickerTextData stickerTextData = stickerData as StickerTextData;

        stickerText = GetComponent<Text>();

        stickerText.font = stickerTextData.font;
        stickerText.text = stickerTextData.textString;
    }
}
