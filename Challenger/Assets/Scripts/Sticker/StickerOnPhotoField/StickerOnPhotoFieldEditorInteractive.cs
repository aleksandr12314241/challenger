﻿
public class StickerOnPhotoFieldEditorInteractive : StickerOnPhotoFieldEditor {
    protected override void SetStart () {
    }

    internal override StickerData GetStickerData () {
        return new StickerInteractiveData();
    }
}
