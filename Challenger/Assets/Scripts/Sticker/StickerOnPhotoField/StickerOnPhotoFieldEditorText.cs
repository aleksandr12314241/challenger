﻿using UnityEngine;
using UnityEngine.UI;

public class StickerOnPhotoFieldEditorText : StickerOnPhotoFieldEditor {
    TouchScreenKeyboard keyboard;
    string text = "";
    Text inputText;

    protected override void SetStart () {
        DragStickerOnPhotoField.OnPointerUpAction += PointerUpAction;

        inputText = GetComponentInParent<Text>();
        text = inputText.text;
    }

    internal override StickerData GetStickerData () {
        return new StickerTextData() {
            font = inputText.font,
            textString = inputText.text
        };
    }

    private void OnDestroy () {
        DragStickerOnPhotoField.OnPointerUpAction -= PointerUpAction;
    }

    void Update() {
        if ( keyboard != null && keyboard.status == TouchScreenKeyboard.Status.Done ) {
            text = keyboard.text;
            if ( text == "" ) {
                text = "Введите текст";
            }
            inputText.text = text;
            myRectTransform.sizeDelta = new Vector2( inputText.preferredWidth, myRectTransform.sizeDelta.y );
        }
    }

    void PointerUpAction () {
        keyboard = TouchScreenKeyboard.Open( text, TouchScreenKeyboardType.Default );
    }
}
