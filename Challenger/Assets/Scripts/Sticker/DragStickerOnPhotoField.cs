﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class DragStickerOnPhotoField : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler,
    IPointerClickHandler, IPointerDownHandler, IPointerUpHandler {
    internal Action OnPointerUpAction = () => { };
    internal Action<DragStickerOnPhotoField> OnBeginDragAction = ( DragStickerOnPhotoField arg ) => { };
    internal Action<DragStickerOnPhotoField> OnEndDragAction = ( DragStickerOnPhotoField  arg ) => { };

    int touchCount;
    float orthoZoomSpeed = 0.005f;        // The rate of change of the orthographic size in orthographic mode.
    bool doubleTouch = false;

    internal StickerGameObjectData StickerGameObjectData {
        private set;
        get;
    }

    internal void Init ( StickerGameObjectData stickerGameObjectData ) {
        StickerGameObjectData = stickerGameObjectData;
        doubleTouch = false;
    }

    public void OnBeginDrag ( PointerEventData eventData ) {
        transform.SetAsLastSibling();
        //startPos = transform.position;

        OnBeginDragAction( this );
    }

    public void OnDrag ( PointerEventData eventData ) {
        transform.position = Camera.main.ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, 1 ) );

        if ( Input.touchCount == 2 ) {
            doubleTouch = true;

            //zoom
            Touch touchZero = Input.GetTouch( 0 );
            Touch touchOne = Input.GetTouch( 1 );

            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

            float prevTouchDeltaMag = ( touchZeroPrevPos - touchOnePrevPos ).magnitude;
            float touchDeltaMag = ( touchZero.position - touchOne.position ).magnitude;

            float deltaMagnitudeDiff = ( prevTouchDeltaMag - touchDeltaMag ) / 2f;

            Vector2 movePosition = ( touchZero.deltaPosition + touchOne.deltaPosition ) / 2f;

            float zoomTemp = transform.localScale.x;
            zoomTemp -= deltaMagnitudeDiff * orthoZoomSpeed;

            if ( zoomTemp > 2f ) {
                zoomTemp = 2f;
            }
            else if ( zoomTemp < 0.5f ) {
                zoomTemp = 0.5f;
            }

            transform.localScale = new Vector3( zoomTemp, zoomTemp, 1 );

            var prevPos1 = touchZero.position - touchZero.deltaPosition; // Generate previous frame's finger positions
            var prevPos2 = touchOne.position - touchOne.deltaPosition;

            Vector3 diff = prevPos2 - prevPos1;
            float angle = ( Mathf.Atan2( diff.y, diff.x ) );
            transform.rotation = Quaternion.Euler( 0f, 0f, Mathf.Rad2Deg * angle );
        }
    }

    public void OnEndDrag ( PointerEventData eventData ) {
        //transform.SetParent( myParent, false );
        //transform.position = startPos;

        OnEndDragAction( this );
    }

    public void OnPointerClick ( PointerEventData eventData ) {
    }

    public void OnPointerDown ( PointerEventData eventData ) {
    }

    public void OnPointerUp ( PointerEventData eventData ) {
        if ( doubleTouch != true ) {
            OnPointerUpAction();
        }

        doubleTouch = false;
    }
}
