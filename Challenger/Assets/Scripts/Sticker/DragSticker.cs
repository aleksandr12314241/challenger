﻿using UnityEngine;
using UnityEngine.EventSystems;
using System;

public class DragSticker : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler {
    internal Action<DragSticker> OnBeginDragAction = ( DragSticker args ) => { };
    internal Action<DragSticker> OnEndDragAction = ( DragSticker args ) => { };

    //RectTransform dragContainer;
    Transform myParent;

    Vector2 startPos;
    internal StickerGameObjectData StickerGameObjectData {
        private set;
        get;
    }

    internal string StickerId {
        private set;
        get;
    }

    internal bool IsBeginDrag {
        private set;
        get;
    }

    GameObject stickerGameObject;

    internal void Init( StickerGameObjectData stickerGameObjectData ) {
        StickerGameObjectData = stickerGameObjectData;
        StickerId = stickerGameObjectData.id;

        stickerGameObject = Instantiate( stickerGameObjectData.stickerGo, transform );
        myParent =  transform.parent;
    }

    public void OnBeginDrag ( PointerEventData eventData ) {
        //transform.SetParent( dragContainer, false );
        startPos = transform.position;

        OnBeginDragAction( this );
    }

    public void OnDrag ( PointerEventData eventData ) {
        transform.position = Camera.main.ScreenToWorldPoint( new Vector3( Input.mousePosition.x, Input.mousePosition.y, 1 ) );
    }

    public void OnEndDrag ( PointerEventData eventData ) {
        //transform.SetParent( myParent, false );
        transform.position = startPos;

        OnEndDragAction( this );
    }
}
