﻿using UnityEngine;

[CreateAssetMenu( fileName = "CommonSpritesDataSet", menuName = "Challenger/CommonSpritesDataSet" )]
public class CommonSpritesDataSet : ScriptableObject {
    [SerializeField]
    Sprite avatarSprite;

    internal Sprite GetAvatarSprite() {
        return avatarSprite;
    }
}
