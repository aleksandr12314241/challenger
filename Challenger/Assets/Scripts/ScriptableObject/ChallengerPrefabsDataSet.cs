﻿using UnityEngine;

[CreateAssetMenu( fileName = "ChallengerPrefabs", menuName = "Challenger/ChallengerPrefabs" )]
public class ChallengerPrefabsDataSet : ScriptableObject {
    [SerializeField]
    public GameObject timelineChallenge;
    [SerializeField]
    public GameObject timelineExecution;
    [SerializeField]
    public GameObject executionSmallInChallengeInfo;
    [SerializeField]
    public Comment commentPrf;
    [SerializeField]
    public ExecutionEntreySmallPreWin executionEntreySmallPreWin;
    [SerializeField]
    public ExecutorEntreySmallWin executionEntreySmallWin;
    [SerializeField]
    public ChallengeEntitySmall challengeEntitySmall;
    [SerializeField]
    public ExecutionEntitySmall executionEntitySmall;
}
