﻿using UnityEngine;

[CreateAssetMenu( fileName = "StickerTextDataSet", menuName = "Create StickerTextDataSet" )]
public class StickerDataSet : ScriptableObject {
    [SerializeField]
    StickerGameObjectData[] stickerGameObjects;

    internal StickerGameObjectData GetStickerGameObjectById ( string id ) {
        for ( int i = 0; i < stickerGameObjects.Length; i++ ) {
            if ( stickerGameObjects[i].id == id ) {
                return stickerGameObjects[i];
            }
        }

        return null;
    }
}

[System.Serializable]
class StickerGameObjectData {
    [SerializeField]
    internal string id;
    [SerializeField]
    internal GameObject stickerGo;
    [SerializeField]
    internal StickerOnPhotoFieldEditor stickerOnPhotoFieldEditor;
    [SerializeField]
    internal StickerOnPhotoField stickerOnPhotoField;
}