﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StickerTextData : StickerData {
    internal string textString;
    internal Font font;
}
