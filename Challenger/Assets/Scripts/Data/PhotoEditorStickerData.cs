﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PhotoEditorStickerData {
    internal string stickerId;
    internal Vector2 stickerPos;
    internal float stickerRotation;
}
