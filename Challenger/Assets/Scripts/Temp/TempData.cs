﻿using System.Collections.Generic;
using UnityEngine;

public class TempData : MonoBehaviour {
    //public static TempData instance = null; // Экземпляр объекта

    //internal ProfileData profileData;
    //internal PostChallengeSmallData[] postChallengeSmallDatas;
    //internal PostExecutorSmallData[] postExecutorSmallDatas;
    //internal PostChallengeData[] postsChallengeDataTemp;
    //internal PostExecutorData[] postsExecutorDataTemp;
    //internal CommentsData commentsData;
    //internal PostExecutorSmallDataInChallenge[] executorSmallDataInChallenge;
    //internal PostChallengeSmallData[] postsChallengeSmallData;
    //internal PostChallengeMiddleData[] postsChallengeMiddleData;
    //internal ExecutionData[] executionData;
    //internal NotificationLikeData[] notificationLikeDatas;
    //internal NotificationCommentData[] notificationCommentDatas;
    //internal NotificationSubscriberData[] notificationSubscriberDatas;
    //internal NotificationCallOnChallengeData[] notificationCallOnChallengeDatas;
    //internal NotificationWinChallengeData[] notificationWinChallengeDatas;

    //private void Awake () {
    //    if ( instance == null ) { // Экземпляр менеджера был найден
    //        instance = this; // Задаем ссылку на экземпляр объекта
    //    }
    //    else if ( instance == this ) { // Экземпляр объекта уже существует на сцене
    //        Destroy( gameObject ); // Удаляем объект
    //    }

    //    DontDestroyOnLoad( gameObject );

    //    InitProfileData();
    //    InitPostChallengeSmallData();
    //    InitPostExecutorSmallData();
    //    InitChallengeData();
    //    InitExecutorData();
    //    InitCommentsData();
    //    InitExecutorSmallDataInChallenge();
    //    InitChallengeSmallData();
    //    InitChallengeMiddleData();
    //    InitExecutionData();
    //    InitNotificationLikeDatas();
    //    InitNotificationCommentDatas();
    //    InitNotificationSubscriberDatas();
    //    InitNotificationCallOnChallengeDatas();
    //    InitNotificationWinChallengeDatas();
    //}

    //void InitProfileData () {
    //    profileData = new ProfileData() {
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0",
    //        aboutMeText = "Привет я такой то секой то",
    //        coinCount = 100,
    //        respectPoints = 100,
    //        subscribedCount = 3,
    //        subscribersCount = 4,

    //        stickersId = new HashSet<string>() {
    //            "sticker_picture_1",
    //            "sticker_picture_2",
    //        },
    //        framesId = new HashSet<string>() {

    //        }
    //    };

    //    profileData.avatarImage = Resources.Load<Sprite>( profileData.avatarId );
    //}

    //void InitPostChallengeSmallData () {
    //    postChallengeSmallDatas = new PostChallengeSmallData[1];
    //    postChallengeSmallDatas[0] = new PostChallengeSmallData() {
    //        id = 0,
    //        postId = 0,
    //        description = "Лучшая фотография с вашей вечеринки",
    //        postTime = 10f,
    //        endChallengeTime = 12f + 3600f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_0"
    //        }
    //    };

    //    for ( int i = 0; i < postChallengeSmallDatas.Length; i++ ) {
    //        postChallengeSmallDatas[i].postContentData.contentImage =
    //            Resources.Load<Sprite>( postChallengeSmallDatas[i].postContentData.contentImageId );
    //    }
    //}

    //void InitPostExecutorSmallData () {
    //    postExecutorSmallDatas = new PostExecutorSmallData[5];
    //    postExecutorSmallDatas[0] = new PostExecutorSmallData() {
    //        id = 0,
    //        postId = 0,
    //        description = "На розовой гитаре",
    //        postTime = 12f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_0"
    //        },
    //        challengeId = 4,
    //        challengeDescription = "На каких необычных музыкальных инструментах иглаи вы?"
    //    };
    //    postExecutorSmallDatas[1] = new PostExecutorSmallData() {
    //        id = 2,
    //        postId = 2,
    //        description = "",
    //        postTime = 24f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_2"
    //        },
    //        challengeId = 3,
    //        challengeDescription = "Что самое экстримальное делали вы?"
    //    };
    //    postExecutorSmallDatas[2] = new PostExecutorSmallData() {
    //        id = 4,
    //        postId = 4,
    //        description = "",
    //        postTime = 24f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_4"
    //        },
    //        challengeId = 2,
    //        challengeDescription = "Делимся вашим рабочим местом"
    //    };
    //    postExecutorSmallDatas[3] = new PostExecutorSmallData() {
    //        id = 6,
    //        postId = 6,
    //        description = "Пицца",
    //        postTime = 24f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_6"
    //        },
    //        challengeId = 1,
    //        challengeDescription = "Лучшее что вы готовили"
    //    };
    //    postExecutorSmallDatas[4] = new PostExecutorSmallData() {
    //        id = 8,
    //        postId = 8,
    //        description = "Самая весёлая пати",
    //        postTime = 24f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_8"
    //        },
    //        challengeId = 0,
    //        challengeDescription = "Лучшая фотография с вашей вечеринки"
    //    };

    //    for ( int i = 0; i < postExecutorSmallDatas.Length; i++ ) {
    //        postExecutorSmallDatas[i].postContentData.contentImage =
    //            Resources.Load<Sprite>( postExecutorSmallDatas[i].postContentData.contentImageId );
    //    }
    //}

    //void InitChallengeData () {
    //    //  ==========================================  челлендж
    //    postsChallengeDataTemp = new PostChallengeData[5];
    //    postsChallengeDataTemp[0] = new PostChallengeData() {
    //        postId = 0,
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = false,
    //        postTime = 10f,
    //        description = "Лучшая фотография с вашей вечеринки",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Challenge/Ch_Content_0"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            1, 3, 4
    //        },
    //        mainComment = new CommentData() {
    //            userId = 1,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "Вау, мой комментарий",
    //            time = 12f,
    //            likeUser = new List<long>() {
    //                0, 3, 4
    //            }
    //        },
    //        //challengeState = ChallengeState.WishParticipate,
    //        endChallengeTime = 12f + 3600f
    //    };
    //    postsChallengeDataTemp[1] = new PostChallengeData() {
    //        postId = 0,
    //        userId = 1,
    //        userName = "carol_cook",
    //        avatarId = "Avatars/avatar_1",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 14f,
    //        description = "Лучшее что вы готовили",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Challenge/Ch_Content_1"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            6, 8, 10
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "Спасибо за рецепт",
    //            time = 12f,
    //            likeUser = new List<long>() {
    //                1, 3, 1
    //            }
    //        },
    //        //challengeState = ChallengeState.WishParticipate,
    //        endChallengeTime = 14f + 360f
    //    };

    //    postsChallengeDataTemp[2] = new PostChallengeData() {
    //        postId = 2,
    //        userId = 2,
    //        userName = "oliver_cunningham",
    //        avatarId = "Avatars/avatar_2",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 18f,
    //        description = "Делимся вашим рабочим местом",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Challenge/Ch_Content_2"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            1, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "Кем ты работаешь?)",
    //            time = 18f,
    //            likeUser = new List<long>() {
    //                6, 6, 10
    //            }
    //        },
    //        //challengeState = ChallengeState.WishParticipate,
    //        endChallengeTime = 18f + 360f
    //    };

    //    postsChallengeDataTemp[3] = new PostChallengeData() {
    //        postId = 3,
    //        userId = 3,
    //        userName = "peter_clark",
    //        avatarId = "Avatars/avatar_3",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 23f,
    //        description = "Что самое экстримальное делали вы?",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Challenge/Ch_Content_3"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = null,
    //        //challengeState = ChallengeState.WishParticipate,
    //        endChallengeTime = 23f + 360f
    //    };

    //    postsChallengeDataTemp[4] = new PostChallengeData() {
    //        postId = 4,
    //        userId = 4,
    //        userName = "madison_hill",
    //        avatarId = "Avatars/avatar_4",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 28f,
    //        description =
    //            "На каких необычных музыкальных инструментах иглаи вы? На каких необычных музыкальных инструментах иглаи вы? На каких необычных музыкальных инструментах иглаи вы?",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Challenge/Ch_Content_4"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = null,
    //        //challengeState = ChallengeState.WishParticipate,
    //        endChallengeTime = 28f + 360f
    //    };

    //    for ( int i = 0; i < postsChallengeDataTemp.Length; i++ ) {
    //        postsChallengeDataTemp[i].avatarImage = Resources.Load<Sprite>( postsChallengeDataTemp[i].avatarId );

    //        for ( int j = 0; j < postsChallengeDataTemp[i].postsContentData.Length; j++ ) {
    //            postsChallengeDataTemp[i].postsContentData[j].contentImage =
    //                Resources.Load<Sprite>( postsChallengeDataTemp[i].postsContentData[j].contentImageId );
    //        }

    //        if ( postsChallengeDataTemp[i].mainComment != null ) {
    //            postsChallengeDataTemp[i].mainComment.avatarImage = Resources.Load<Sprite>( postsChallengeDataTemp[i].mainComment.avatarId );
    //        }
    //    }
    //}

    //internal PostChallengeData GetPostChallengeDataById ( long postId ) {
    //    for ( int i = 0; i < postsChallengeDataTemp.Length; i++ ) {
    //        if ( postsChallengeDataTemp[i].postId == postId ) {
    //            return postsChallengeDataTemp[i];
    //        }
    //    }
    //    return null;
    //}

    //void InitExecutorData () {
    //    // ========================================== исполнители
    //    postsExecutorDataTemp = new PostExecutorData[10];
    //    // ------------  challenge_4
    //    postsExecutorDataTemp[0] = new PostExecutorData() {
    //        postId = 0,
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 12f,
    //        description = "На розовой гитаре",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_0"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "У меня была такая же",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 6, 10
    //            }
    //        },
    //        challengeId = 4,
    //        challengeDescription = "На каких необычных музыкальных инструментах иглаи вы?",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_4"
    //        }
    //    };
    //    postsExecutorDataTemp[1] = new PostExecutorData() {
    //        postId = 1,
    //        userId = 1,
    //        userName = "carol_cook",
    //        avatarId = "Avatars/avatar_1",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 12f,
    //        description = "Гитара-АК47",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_1"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "Где покупал?",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 6, 10
    //            }
    //        },
    //        challengeId = 4,
    //        challengeDescription = "На каких необычных музыкальных инструментах иглаи вы?",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_4"
    //        }
    //    };
    //    // ------------  challenge_3
    //    postsExecutorDataTemp[2] = new PostExecutorData() {
    //        postId = 2,
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 24f,
    //        description = "",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_2"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "=О",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 6, 10
    //            }
    //        },
    //        challengeId = 3,
    //        challengeDescription = "Что самое экстримальное делали вы?",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_3"
    //        }
    //    };
    //    postsExecutorDataTemp[3] = new PostExecutorData() {
    //        postId = 3,
    //        userId = 1,
    //        userName = "carol_cook",
    //        avatarId = "Avatars/avatar_1",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 25f,
    //        description = "",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_3"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "о_О",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 10
    //            }
    //        },
    //        challengeId = 3,
    //        challengeDescription = "Что самое экстримальное делали вы?",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_3"
    //        }
    //    };
    //    // ------------  challenge_2
    //    postsExecutorDataTemp[4] = new PostExecutorData() {
    //        postId = 4,
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 24f,
    //        description = "",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_4"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "=О",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 6, 10
    //            }
    //        },
    //        challengeId = 2,
    //        challengeDescription = "Делимся вашим рабочим местом",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_2"
    //        }
    //    };
    //    postsExecutorDataTemp[5] = new PostExecutorData() {
    //        postId = 5,
    //        userId = 1,
    //        userName = "carol_cook",
    //        avatarId = "Avatars/avatar_1",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 25f,
    //        description = "",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_5"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "о_О",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 10
    //            }
    //        },
    //        challengeId = 2,
    //        challengeDescription = "Делимся вашим рабочим местом",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_2"
    //        }
    //    };
    //    // ------------  challenge_1
    //    postsExecutorDataTemp[6] = new PostExecutorData() {
    //        postId = 6,
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 24f,
    //        description = "Пицца",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_6"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "Выглядит аппетитно",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 6, 10
    //            }
    //        },
    //        challengeId = 1,
    //        challengeDescription = "Лучшее что вы готовили",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_1"
    //        }
    //    };
    //    postsExecutorDataTemp[7] = new PostExecutorData() {
    //        postId = 7,
    //        userId = 1,
    //        userName = "carol_cook",
    //        avatarId = "Avatars/avatar_1",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 25f,
    //        description = "Рыба",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_7"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "Это рыба или мясо?",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 10
    //            }
    //        },
    //        challengeId = 1,
    //        challengeDescription = "Лучшее что вы готовили",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_1"
    //        }
    //    };
    //    // ------------  challenge_0
    //    postsExecutorDataTemp[8] = new PostExecutorData() {
    //        postId = 8,
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 24f,
    //        description = "Самая весёлая пати",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_8"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "Выглядит аппетитно",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 6, 10
    //            }
    //        },
    //        challengeId = 0,
    //        challengeDescription = "Лучшая фотография с вашей вечеринки",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_0"
    //        }
    //    };
    //    postsExecutorDataTemp[9] = new PostExecutorData() {
    //        postId = 9,
    //        userId = 1,
    //        userName = "carol_cook",
    //        avatarId = "Avatars/avatar_1",
    //        //avatarImage = Resources.Load( avatarId ) as Sprite,
    //        isSubscriber = true,
    //        postTime = 25f,
    //        description = "Вечеринка СССР",
    //        postsContentData = new PostContentData[1] {
    //            new PostContentData() {
    //                contentImageId = "ContentImages/Executor/Ex_Content_9"
    //            }
    //        },
    //        likeUser = new List<long>() {
    //            4, 1, 8, 1, 5
    //        },
    //        mainComment = new CommentData() {
    //            userId = 6,
    //            //userName = написать имя,
    //            avatarId = "Avatars/avatar_1",
    //            commentText = "Это рыба или мясо?",
    //            time = 13f,
    //            likeUser = new List<long>() {
    //                6, 10
    //            }
    //        },
    //        challengeId = 0,
    //        challengeDescription = "Лучшая фотография с вашей вечеринки",
    //        challengeContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_0"
    //        }
    //    };

    //    for ( int i = 0; i < postsExecutorDataTemp.Length; i++ ) {
    //        postsExecutorDataTemp[i].avatarImage = Resources.Load<Sprite>( postsExecutorDataTemp[i].avatarId );

    //        for ( int j = 0; j < postsExecutorDataTemp[i].postsContentData.Length; j++ ) {
    //            postsExecutorDataTemp[i].postsContentData[j].contentImage =
    //                Resources.Load<Sprite>( postsExecutorDataTemp[i].postsContentData[j].contentImageId );
    //        }

    //        if ( postsExecutorDataTemp[i].mainComment != null ) {
    //            postsExecutorDataTemp[i].mainComment.avatarImage = Resources.Load<Sprite>( postsExecutorDataTemp[i].mainComment.avatarId );
    //        }

    //        postsExecutorDataTemp[i].challengeContentData.contentImage =
    //            Resources.Load<Sprite>( postsExecutorDataTemp[i].challengeContentData.contentImageId );
    //    }
    //}

    //internal PostExecutorData GetPostExecutorDataById ( long postId ) {
    //    for ( int i = 0; i < postsExecutorDataTemp.Length; i++ ) {
    //        if ( postsExecutorDataTemp[i].postId == postId ) {
    //            return postsExecutorDataTemp[i];
    //        }
    //    }
    //    return null;
    //}

    //void InitCommentsData () {
    //    commentsData = new CommentsData() {
    //        commentsData = new List<CommentData>() {
    //            new CommentData() {
    //                commentId = "comment_1",
    //                userId = 1,
    //                userName = "carol_cook",
    //                avatarId = "Avatars/avatar_1",
    //                //avatarImage = Resources.Load( avatarId ) as Sprite,
    //                commentText = "Прикольно",
    //                likeUser = new List<long>() {
    //                    6, 10
    //                },
    //                time = 15f
    //            },
    //            new CommentData() {
    //                commentId = "comment_2",
    //                userId = 1,
    //                userName = "oliver_cunningham",
    //                avatarId = "Avatars/avatar_2",
    //                //avatarImage = Resources.Load( avatarId ) as Sprite,
    //                commentText = "Необычно",
    //                likeUser = new List<long>() {
    //                    6, 10
    //                },
    //                time = 19f
    //            },
    //            new CommentData() {
    //                commentId = "comment_3",
    //                userId = 3,
    //                userName = "peter_clark",
    //                avatarId = "Avatars/avatar_3",
    //                //avatarImage = Resources.Load( avatarId ) as Sprite,
    //                commentText = "Вау!!!",
    //                likeUser = new List<long>() {
    //                    6, 10
    //                },
    //                time = 21f
    //            },
    //            new CommentData() {
    //                commentId = "comment_4",
    //                userId = 4,
    //                userName = "madison_hill",
    //                avatarId = "Avatars/avatar_4",
    //                //avatarImage = Resources.Load( avatarId ) as Sprite,
    //                commentText = "Ничего не понятно",
    //                likeUser = new List<long>() {
    //                    6, 10
    //                },
    //                time = 24f
    //            }
    //        }
    //    };

    //    for ( int i = 0; i < commentsData.commentsData.Count; i++ ) {
    //        commentsData.commentsData[i].avatarImage = Resources.Load<Sprite>( commentsData.commentsData[i].avatarId );
    //    }
    //}

    //void InitExecutorSmallDataInChallenge () {
    //    executorSmallDataInChallenge = new PostExecutorSmallDataInChallenge[10];
    //    executorSmallDataInChallenge[0] = new PostExecutorSmallDataInChallenge() {
    //        id = 0,
    //        postId = 0,
    //        challengeId = 4,
    //        description = "На розовой гитаре",
    //        postTime = 13f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_0"
    //        }
    //    };
    //    executorSmallDataInChallenge[1] = new PostExecutorSmallDataInChallenge() {
    //        id = 1,
    //        postId = 1,
    //        challengeId = 4,
    //        description = "Гитара-АК47",
    //        postTime = 13f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_1"
    //        }
    //    };
    //    executorSmallDataInChallenge[2] = new PostExecutorSmallDataInChallenge() {
    //        id = 2,
    //        postId = 2,
    //        challengeId = 3,
    //        description = "",
    //        postTime = 24f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_2"
    //        }
    //    };
    //    executorSmallDataInChallenge[3] = new PostExecutorSmallDataInChallenge() {
    //        id = 3,
    //        postId = 3,
    //        challengeId = 3,
    //        description = "",
    //        postTime = 25f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_3"
    //        }
    //    };
    //    executorSmallDataInChallenge[4] = new PostExecutorSmallDataInChallenge() {
    //        id = 4,
    //        postId = 4,
    //        challengeId = 2,
    //        description = "",
    //        postTime = 24f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_4"
    //        }
    //    };
    //    executorSmallDataInChallenge[5] = new PostExecutorSmallDataInChallenge() {
    //        id = 5,
    //        postId = 5,
    //        challengeId = 2,
    //        description = "",
    //        postTime = 25f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_5"
    //        }
    //    };
    //    executorSmallDataInChallenge[6] = new PostExecutorSmallDataInChallenge() {
    //        id = 6,
    //        postId = 6,
    //        challengeId = 2,
    //        description = "Пицца",
    //        postTime = 24f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_6"
    //        }
    //    };
    //    executorSmallDataInChallenge[7] = new PostExecutorSmallDataInChallenge() {
    //        id = 7,
    //        postId = 7,
    //        challengeId = 1,
    //        description = "Рыба",
    //        postTime = 25f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_7"
    //        }
    //    };
    //    executorSmallDataInChallenge[8] = new PostExecutorSmallDataInChallenge() {
    //        id = 8,
    //        postId = 8,
    //        challengeId = 0,
    //        description = "Самая весёлая пати",
    //        postTime = 25f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_8"
    //        }
    //    };
    //    executorSmallDataInChallenge[9] = new PostExecutorSmallDataInChallenge() {
    //        id = 9,
    //        postId = 9,
    //        challengeId = 0,
    //        description = "Вечеринка СССР",
    //        postTime = 13f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Executor/Ex_Content_9"
    //        }
    //    };

    //    for ( int i = 0; i < executorSmallDataInChallenge.Length; i++ ) {
    //        PostExecutorSmallDataInChallenge postExecutorSmallDataInChallenge = executorSmallDataInChallenge[i];
    //        postExecutorSmallDataInChallenge.postContentData.contentImage =
    //            Resources.Load<Sprite>( postExecutorSmallDataInChallenge.postContentData.contentImageId );
    //    }
    //}

    //public ExecutorsSmallDataInChallenge GetExecutorsSmallDataInChallenge ( long challengeId ) {
    //    ExecutorsSmallDataInChallenge executorsSmallDataInChallenge = new ExecutorsSmallDataInChallenge();

    //    for ( int i = 0; i < executorSmallDataInChallenge.Length; i++ ) {
    //        if ( executorSmallDataInChallenge[i].challengeId != challengeId ) {
    //            continue;
    //        }

    //        executorsSmallDataInChallenge.postExecutorSmallDataInChallenge.Add( executorSmallDataInChallenge[i] );
    //    }

    //    return executorsSmallDataInChallenge;
    //}

    //void InitChallengeSmallData () {
    //    postsChallengeSmallData = new PostChallengeSmallData[5];
    //    postsChallengeSmallData[0] = new PostChallengeSmallData() {
    //        id = 0,
    //        postId = 0,
    //        description = "Лучшая фотография с вашей вечеринки",
    //        postTime = 10f,
    //        endChallengeTime = 12f + 3600f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_0"
    //        }
    //    };

    //    postsChallengeSmallData[1] = new PostChallengeSmallData() {
    //        id = 1,
    //        postId = 1,
    //        description = "Лучшее что вы готовили",
    //        postTime = 14f,
    //        endChallengeTime = 14f + 360f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_1"
    //        }
    //    };

    //    postsChallengeSmallData[2] = new PostChallengeSmallData() {
    //        id = 2,
    //        postId = 2,
    //        description = "Делимся вашим рабочим местом",
    //        postTime = 18f,
    //        endChallengeTime = 18f + 360f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_2"
    //        }
    //    };

    //    postsChallengeSmallData[3] = new PostChallengeSmallData() {
    //        id = 3,
    //        postId = 3,
    //        //description="Что самое экстримальное делали вы?",
    //        description = "Что самое экстримальное делали вы ? as dsad sad asd dsadsadad",
    //        postTime = 23f,
    //        endChallengeTime = 23f + 360f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_3"
    //        }
    //    };

    //    postsChallengeSmallData[4] = new PostChallengeSmallData() {
    //        id = 4,
    //        postId = 4,
    //        description =
    //            "На каких необычных музыкальных инструментах иглаи вы? На каких необычных музыкальных инструментах иглаи вы? На каких необычных музыкальных инструментах иглаи вы?",
    //            postTime = 28f,
    //            endChallengeTime = 28f + 360f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_4"
    //        }
    //    };

    //    for ( int i = 0; i < postsChallengeSmallData.Length; i++ ) {
    //        postsChallengeSmallData[i].postContentData.contentImage =
    //            Resources.Load<Sprite>( postsChallengeSmallData[i].postContentData.contentImageId );
    //    }
    //}

    //void InitChallengeMiddleData () {
    //    postsChallengeMiddleData = new PostChallengeMiddleData[5];
    //    postsChallengeMiddleData[0] = new PostChallengeMiddleData() {
    //        id = 0,
    //        postId = 0,
    //        description = "Лучшая фотография с вашей вечеринки",
    //        postTime = 10f,
    //        endChallengeTime = 12f + 3600f,
    //        avatarId = "Avatars/avatar_0",
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_0"
    //        }
    //    };

    //    postsChallengeMiddleData[1] = new PostChallengeMiddleData() {
    //        id = 1,
    //        postId = 1,
    //        description = "Лучшее что вы готовили",
    //        postTime = 14f,
    //        endChallengeTime = 14f + 360f,
    //        avatarId = "Avatars/avatar_1",
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_1"
    //        }
    //    };

    //    postsChallengeMiddleData[2] = new PostChallengeMiddleData() {
    //        id = 2,
    //        postId = 2,
    //        description = "Делимся вашим рабочим местом",
    //        postTime = 18f,
    //        endChallengeTime = 18f + 360f,
    //        avatarId = "Avatars/avatar_2",
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_2"
    //        }
    //    };

    //    postsChallengeMiddleData[3] = new PostChallengeMiddleData() {
    //        id = 3,
    //        postId = 3,
    //        description = "Что самое экстримальное делали вы?",
    //        postTime = 23f,
    //        endChallengeTime = 23f + 360f,
    //        avatarId = "Avatars/avatar_3",
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_3"
    //        }
    //    };

    //    postsChallengeMiddleData[4] = new PostChallengeMiddleData() {
    //        id = 4,
    //        postId = 4,
    //        description =
    //            "На каких необычных музыкальных инструментах иглаи вы? На каких необычных музыкальных инструментах иглаи вы? На каких необычных музыкальных инструментах иглаи вы?",
    //            postTime = 28f,
    //            endChallengeTime = 28f + 360f,
    //            avatarId = "Avatars/avatar_4",
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_4"
    //        }
    //    };

    //    for ( int i = 0; i < postsChallengeSmallData.Length; i++ ) {
    //        postsChallengeMiddleData[i].avatarImage = Resources.Load<Sprite>( postsChallengeMiddleData[i].avatarId );
    //        postsChallengeMiddleData[i].postContentData.contentImage =
    //            Resources.Load<Sprite>( postsChallengeMiddleData[i].postContentData.contentImageId );
    //    }
    //}

    //internal GlobalNewsPostsChallengeData GetGlobalNewsPostsChallengeData () {
    //    GlobalNewsPostsChallengeData globalNewsPostsChallengeData = new GlobalNewsPostsChallengeData();
    //    globalNewsPostsChallengeData.postsChallengeSmallData = new PostChallengeSmallData[5];

    //    for ( int i = 0; i < postsChallengeSmallData.Length; i++ ) {
    //        globalNewsPostsChallengeData.postsChallengeSmallData[i] = postsChallengeSmallData[i];
    //    }

    //    globalNewsPostsChallengeData.postsChallengeMiddleData = new PostChallengeMiddleData[5];
    //    for ( int i = 0; i < postsChallengeMiddleData.Length; i++ ) {
    //        globalNewsPostsChallengeData.postsChallengeMiddleData[i] = postsChallengeMiddleData[i];
    //    }

    //    return globalNewsPostsChallengeData;
    //}

    //void InitExecutionData () {
    //    executionData = new ExecutionData[5];
    //    executionData[0] = new ExecutionData() {
    //        postId = 0,
    //        description = "Лучшая фотография с вашей вечеринки",
    //        postTime = 10f,
    //        endChallengeTime = 12f + 3600f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_0"
    //        }
    //    };

    //    executionData[1] = new ExecutionData() {
    //        postId = 1,
    //        description = "Лучшее что вы готовили",
    //        postTime = 14f,
    //        endChallengeTime = 14f + 360f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_1"
    //        }
    //    };

    //    executionData[2] = new ExecutionData() {
    //        postId = 2,
    //        description = "Делимся вашим рабочим местом",
    //        postTime = 18f,
    //        endChallengeTime = 18f + 360f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_2"
    //        }
    //    };

    //    executionData[3] = new ExecutionData() {
    //        postId = 3,
    //        description = "Что самое экстримальное делали вы?",
    //        postTime = 23f,
    //        endChallengeTime = 23f + 360f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_3"
    //        }
    //    };

    //    executionData[4] = new ExecutionData() {
    //        postId = 4,
    //        description =
    //            "На каких необычных музыкальных инструментах иглаи вы? На каких необычных музыкальных инструментах иглаи вы? На каких необычных музыкальных инструментах иглаи вы?",
    //            postTime = 28f,
    //            endChallengeTime = 28f + 360f,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_4"
    //        }
    //    };

    //    for ( int i = 0; i < executionData.Length; i++ ) {
    //        executionData[i].postContentData.contentImage =
    //            Resources.Load<Sprite>( executionData[i].postContentData.contentImageId );
    //    }
    //}

    //void InitNotificationLikeDatas () {
    //    notificationLikeDatas = new NotificationLikeData[3];
    //    notificationLikeDatas[0] = new NotificationLikeData() {
    //        notificationId = "notification_like_0",
    //        notoficationTime = 14d,
    //        userId = 1,
    //        userName = "carol_cook",
    //        avatarId = "Avatars/avatar_1",
    //        postId = 0,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_0"
    //        }
    //    };

    //    notificationLikeDatas[1] = new NotificationLikeData() {
    //        notificationId = "notification_like_1",
    //        notoficationTime = 20d,
    //        userId = 1,
    //        userName = "oliver_cunningham",
    //        avatarId = "Avatars/avatar_2",
    //        postId = 4,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_4"
    //        }
    //    };

    //    notificationLikeDatas[2] = new NotificationLikeData() {
    //        notificationId = "notification_like_2",
    //        notoficationTime = 22d,
    //        userId = 3,
    //        userName = "peter_clark",
    //        avatarId = "Avatars/avatar_3",
    //        postId = 4,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_4"
    //        }
    //    };

    //    for ( int i = 0; i < notificationLikeDatas.Length; i++ ) {
    //        notificationLikeDatas[i].avatarImage = Resources.Load<Sprite>( notificationLikeDatas[i].avatarId );
    //        notificationLikeDatas[i].postContentData.contentImage = Resources.Load<Sprite>( notificationLikeDatas[i].postContentData.contentImageId );
    //    }
    //}

    //void InitNotificationCommentDatas () {
    //    notificationCommentDatas = new NotificationCommentData[3];
    //    notificationCommentDatas[0] = new NotificationCommentData() {
    //        notificationId = "notification_comment_0",
    //        notoficationTime = 16d,
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0",
    //        postId = 1,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_1"
    //        },
    //        commentText = "прикольно"
    //    };

    //    notificationCommentDatas[1] = new NotificationCommentData() {
    //        notificationId = "notification_comment_1",
    //        notoficationTime = 17d,
    //        userId = 4,
    //        userName = "madison_hill",
    //        avatarId = "Avatars/avatar_4",
    //        postId = 2,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_2"
    //        },
    //        commentText = "это будет интересно"
    //    };

    //    notificationCommentDatas[2] = new NotificationCommentData() {
    //        notificationId = "notification_comment_2",
    //        notoficationTime = 25d,
    //        userId = 3,
    //        userName = "peter_clark",
    //        avatarId = "Avatars/avatar_3",
    //        postId = 3,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_3"
    //        },
    //        commentText = "=D"
    //    };

    //    for ( int i = 0; i < notificationCommentDatas.Length; i++ ) {
    //        notificationCommentDatas[i].avatarImage = Resources.Load<Sprite>( notificationCommentDatas[i].avatarId );
    //        notificationCommentDatas[i].postContentData.contentImage = Resources.Load<Sprite>
    //                ( notificationCommentDatas[i].postContentData.contentImageId );
    //    }
    //}

    //void InitNotificationSubscriberDatas () {
    //    notificationSubscriberDatas = new NotificationSubscriberData[2];
    //    notificationSubscriberDatas[0] = new NotificationSubscriberData() {
    //        notificationId = "notification_subscriber_0",
    //        notoficationTime = 18d,
    //        userId = 3,
    //        userName = "peter_clark",
    //        avatarId = "Avatars/avatar_3"
    //    };

    //    notificationSubscriberDatas[1] = new NotificationSubscriberData() {
    //        notificationId = "notification_subscriber_1",
    //        notoficationTime = 23d,
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0"
    //    };

    //    for ( int i = 0; i < notificationSubscriberDatas.Length; i++ ) {
    //        notificationSubscriberDatas[i].avatarImage = Resources.Load<Sprite>( notificationSubscriberDatas[i].avatarId );
    //    }
    //}

    //void InitNotificationCallOnChallengeDatas () {
    //    notificationCallOnChallengeDatas = new NotificationCallOnChallengeData[2];
    //    notificationCallOnChallengeDatas[0] = new NotificationCallOnChallengeData() {
    //        notificationId = "notification_call_on_challenge_0",
    //        notoficationTime = 18d,
    //        userId = 0,
    //        userName = "aron_ellis",
    //        avatarId = "Avatars/avatar_0",
    //        postId = 1,
    //        challengeDescription = "Лучшее что вы готовили",
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_1"
    //        },
    //        postTime = 0d,
    //        endChallengeTime = 30d,
    //        challengeState = ChallengeState.WishParticipate
    //    };

    //    notificationCallOnChallengeDatas[1] = new NotificationCallOnChallengeData() {
    //        notificationId = "notification_call_on_challenge_1",
    //        notoficationTime = 25d,
    //        userId = 4,
    //        userName = "madison_hill",
    //        avatarId = "Avatars/avatar_4",
    //        postId = 2,
    //        challengeDescription = "Делимся вашим рабочим местом",
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_2"
    //        },
    //        postTime = 10d,
    //        endChallengeTime = 50d,
    //        challengeState = ChallengeState.WishParticipate
    //    };

    //    for ( int i = 0; i < notificationCallOnChallengeDatas.Length; i++ ) {
    //        notificationCallOnChallengeDatas[i].avatarImage = Resources.Load<Sprite>( notificationCallOnChallengeDatas[i].avatarId );
    //        notificationCallOnChallengeDatas[i].postContentData.contentImage =
    //            Resources.Load<Sprite>( notificationCallOnChallengeDatas[i].postContentData.contentImageId );
    //    }
    //}

    //void InitNotificationWinChallengeDatas () {
    //    notificationWinChallengeDatas = new NotificationWinChallengeData[1];
    //    notificationWinChallengeDatas[0] = new NotificationWinChallengeData() {
    //        notificationId = "notification_win_challenge_1",
    //        notoficationTime = 26d,
    //        postId = 0,
    //        winPlace = 2,
    //        coinsCount = 30,
    //        postContentData = new PostContentData() {
    //            contentImageId = "ContentImages/Challenge/Ch_Content_0"
    //        }
    //    };

    //    for ( int i = 0; i < notificationWinChallengeDatas.Length; i++ ) {
    //        notificationWinChallengeDatas[i].postContentData.contentImage =
    //            Resources.Load<Sprite>( notificationWinChallengeDatas[i].postContentData.contentImageId );
    //    }
    //}
}
