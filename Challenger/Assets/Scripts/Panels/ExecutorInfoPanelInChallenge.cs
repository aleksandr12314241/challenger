﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ExecutorInfoPanelInChallenge : MonoBehaviour {
    public Action OnCloseExecutorInfoPanelInChallenge = () => { };

    [SerializeField]
    Button closeButton;
    [SerializeField]
    ExecutionInfo executorInfo;

    internal void Init() {
        closeButton.onClick.AddListener( StartClose );
        closeButton.enabled = false;
    }

    internal void StartShow( ExecutionSmallInChallengeInfo executorSmallInChallengeInfo ) {
        closeButton.enabled = true;

        executorInfo.Init( executorSmallInChallengeInfo.Execution );
    }

    void StartClose() {
        OnCloseExecutorInfoPanelInChallenge();
        closeButton.enabled = false;
    }
}
