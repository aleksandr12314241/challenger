﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class JoinButtonPanel : MonoBehaviour {
    [SerializeField]
    Button joinToChallengeButton;

    //TODO:
    CreateExecutionWindow createExecutionWindow;
    PreChooseWinnerWindow preChooseWinnerWindow;

    Image buttonImage;
    Text buttonText;

    ChallengeState challengeState;

    string challengeDescription;

    [Inject]
    Profile profile;

    ChallengeServ challenge;

    // Note that in this case we can't use a constructor
    [Inject]
    public void Construct() {
    }

    void Init() {
        createExecutionWindow = FindObjectOfType<CreateExecutionWindow>();
        preChooseWinnerWindow = FindObjectOfType<PreChooseWinnerWindow>();

        joinToChallengeButton.onClick.AddListener( ChallengeButtonClick );

        buttonImage = ( Image ) joinToChallengeButton.targetGraphic;
        buttonText = joinToChallengeButton.GetComponentInChildren<Text>();
    }

    ChallengeState GetChallengeState( ChallengeServ challenge ) {
        if( challenge.author.id == profile.Id ) {
            return ChallengeState.ChooseWinners;
        }
        else {
            ExecutionServ executionServ = challenge.executions.Find( x => x.author.id == profile.Id );

            if( executionServ != null ) {
                return ChallengeState.Participate;
            }
            return ChallengeState.WishParticipate;
        }
    }

    internal void Init( ChallengeServ challenge ) {
        Init();

        this.challenge = challenge;

        challengeState = GetChallengeState( challenge );
        challengeDescription = challenge.description;
    }

    internal void Init( ExecutionServ executionServ ) {
        Init();

        this.challenge = executionServ.challenge;

        challengeState = GetChallengeState( challenge );
        challengeDescription = challenge.description;
    }

    //internal void Init ( PostChallengeData postChallengeData ) {
    //    Init();

    //    this.postChallengeData = postChallengeData;

    //    challengeState = GetChallengeState( postChallengeData );
    //    challengeDescription = postChallengeData.description;
    //}

    internal void Init( NotificationCallOnChallengeServ notificationCallOnChallengeData ) {
        Init();

        challengeState = notificationCallOnChallengeData.challengeState;
        challengeDescription = notificationCallOnChallengeData.challengeDescription;
    }

    void ChallengeButtonClick() {
        switch( challengeState ) {
        case ChallengeState.WishParticipate:
            createExecutionWindow.StartShow( challenge.id, challengeDescription );
            break;
        case ChallengeState.ChooseWinners:
            preChooseWinnerWindow.StartShow( challenge );
            break;
        }
    }

    internal void CheckChallengeButtonState() {
        switch( challengeState ) {
        case ChallengeState.WishParticipate:
            joinToChallengeButton.gameObject.SetActive( true );

            buttonImage.color = new Color( 2f, 109f, 184f, 255f ) / 255f;
            buttonText.color = Color.white;
            buttonText.text = "Подать заявку";

            break;
        case ChallengeState.Participate:
            joinToChallengeButton.gameObject.SetActive( false );
            break;
        case ChallengeState.ChooseWinners:
            joinToChallengeButton.gameObject.SetActive( true );

            buttonImage.color = new Color( 184f, 2f, 42f, 255f ) / 255f;
            buttonText.color = Color.white;

            buttonText.text = "Выбрать победителей";
            break;
        }
    }
}
