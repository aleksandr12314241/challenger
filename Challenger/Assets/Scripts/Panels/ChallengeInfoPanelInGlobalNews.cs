﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ChallengeInfoPanelInGlobalNews : MonoBehaviour {
    public Action OnCloseChallengeInfo = () => { };

    [SerializeField]
    Button closeButton;
    [SerializeField]
    ChallengeInfo challengeInfo;

    TimelineDescriptionPostInfo timelineDescriptionPostInfo;
    ChallengeInfoWindow challengeEntreyInfoWindow;

    ChallengeServ challenge;

    internal void Init() {
        challengeEntreyInfoWindow = FindObjectOfType<ChallengeInfoWindow>();

        closeButton.onClick.AddListener( StartClose );
        closeButton.enabled = false;
        timelineDescriptionPostInfo = ( TimelineDescriptionPostInfo ) challengeInfo.DescriptionPostInfo;
        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick += ShowChallengeWindow;
    }

    internal void StartShow( ChallengeEntitySmall challengeEntreySmall ) {
        closeButton.enabled = true;

        this.challenge = challengeEntreySmall.challenge;

        challengeInfo.Init( this.challenge );
    }

    void StartClose() {
        OnCloseChallengeInfo();
        closeButton.enabled = false;
    }

    internal void ShowChallengeWindow() {
        challengeEntreyInfoWindow.StartShow( this.challenge );
    }

    private void OnDestroy() {
        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick -= ShowChallengeWindow;
    }
}
