﻿using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System;

public class PhotoEditorPanel : MonoBehaviour {
    internal Action<bool, float, float> OnShowMoreStikerClick = ( bool isShow, float time, float offsetHeight ) => { };
    internal Action<bool> OnChoosePhoto = ( bool isChoosePhoto ) => { };

    [SerializeField]
    Button addPhotoButton;
    [SerializeField]
    Button changeAddPhotoButton;
    [SerializeField]
    Button moreStickerButton;
    [SerializeField]
    Button inShopStickerButton;
    [SerializeField]
    ScrollRect stickerScrollView;
    [SerializeField]
    RectTransform stickerScrollViewsRectTransform;
    [SerializeField]
    Image frameButtonOutlone;
    [SerializeField]
    GameObject photoMaskGameObject;
    [SerializeField]
    Image photoImage;
    [SerializeField]
    Button[] tubButtons;
    [SerializeField]
    PhotoField photoField;
    [SerializeField]
    DragSticker dragSticker;

    RectTransform inShopStickerRectTransform;

    ChoosePhotoAndVideoWindow choosePhotoAndVideoWindow;

    bool isShowMoreStickers;
    float stickerScrollViewsHeight;
    float inShopStickerPosY;
    float timeAnimSticker;

    bool isOverOnPhotoField;

    internal void Init () {
        inShopStickerRectTransform = inShopStickerButton.GetComponent<RectTransform>();

        addPhotoButton.onClick.AddListener( ShowChoosePhotoAndVideoWindow );
        changeAddPhotoButton.onClick.AddListener( ShowChoosePhotoAndVideoWindow );
        moreStickerButton.onClick.AddListener( ShowMoreStikerClick );

        stickerScrollViewsHeight = 160f;
        inShopStickerPosY = inShopStickerRectTransform.anchoredPosition.y;
        timeAnimSticker = 0.20f;

        photoField.OnMouseEnterAction += MouseEnterAction;
        photoField.OnMouseExitAction += MouseExitAction;
        dragSticker.OnEndDragAction += EndDragAction;
    }

    internal void StartShow () {
        frameButtonOutlone.gameObject.SetActive( true );
        photoMaskGameObject.gameObject.SetActive( false );
        photoImage.sprite = null;
    }

    void ShowChoosePhotoAndVideoWindow () {
        choosePhotoAndVideoWindow.StartShow();

        choosePhotoAndVideoWindow = FindObjectOfType<ChoosePhotoAndVideoWindow>();

        choosePhotoAndVideoWindow.OnChoosePhoto -= ChoosePhoto;
        choosePhotoAndVideoWindow.OnChoosePhoto += ChoosePhoto;
    }

    void ChoosePhoto ( Sprite photoSprite ) {
        if ( photoSprite == null ) {
            frameButtonOutlone.gameObject.SetActive( true );
            photoMaskGameObject.gameObject.SetActive( false );
            photoImage.sprite = null;
        }
        else {
            frameButtonOutlone.gameObject.SetActive( false );
            photoMaskGameObject.gameObject.SetActive( true );
            photoImage.sprite = photoSprite;
            photoImage.SetNativeSize();
        }
    }

    void ShowMoreStikerClick () {
        float endSizeDeltaY = stickerScrollViewsHeight;

        if ( isShowMoreStickers ) {
            isShowMoreStickers = false;

            stickerScrollViewsRectTransform.DOKill();
            stickerScrollViewsRectTransform.DOSizeDelta(
                new Vector2( stickerScrollViewsRectTransform.sizeDelta.x, stickerScrollViewsHeight ), timeAnimSticker );

            inShopStickerRectTransform.DOKill();
            inShopStickerRectTransform.DOAnchorPosY( inShopStickerPosY, timeAnimSticker );
        }
        else {
            isShowMoreStickers = true;
            endSizeDeltaY = stickerScrollViewsHeight * 3f;

            stickerScrollViewsRectTransform.DOKill();
            stickerScrollViewsRectTransform.DOSizeDelta(
                new Vector2( stickerScrollViewsRectTransform.sizeDelta.x, endSizeDeltaY ), timeAnimSticker );

            inShopStickerRectTransform.DOKill();
            inShopStickerRectTransform.DOAnchorPosY( inShopStickerPosY - stickerScrollViewsHeight * 2, timeAnimSticker );
        }

        OnShowMoreStikerClick( isShowMoreStickers, timeAnimSticker, -( stickerScrollViewsHeight - endSizeDeltaY ) );
    }

    void MouseEnterAction () {
        isOverOnPhotoField = true;
    }

    void MouseExitAction () {
        isOverOnPhotoField = false;
    }

    void EndDragAction ( DragSticker dragSticker ) {
        if ( !isOverOnPhotoField ) {
            return;
        }

        isOverOnPhotoField = false;
        photoField.AddSticker( dragSticker, Vector2.zero );
    }
}
