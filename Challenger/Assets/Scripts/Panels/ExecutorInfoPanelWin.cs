﻿using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class ExecutorInfoPanelWin: MonoBehaviour {
    public event Action<int> OnSetAcceptIndex = ( int args ) => { };

    [SerializeField]
    ExecutionInfoWithChallengeInfo executorInfo;
    [SerializeField]
    Button[] crownsButton;
    [SerializeField]
    RectTransform frameRectTransform;
    [SerializeField]
    Button acceptButton;

    RectTransform[] crownsRectTransform;

    int currentAcceptIndex = 0;
    float timeAnimFrame = 0.2f;

    internal void Init() {
        crownsRectTransform = new RectTransform[crownsButton.Length];
        for( int i = 0; i < crownsButton.Length; i++ ) {
            int indexCrown = i;
            crownsButton[i].onClick.AddListener( () => {
                SetCrown( indexCrown );
            } );
            crownsRectTransform[i] = crownsButton[i].GetComponent<RectTransform>();
        }

        SetDefaultExecutorInfoPanelWin();

        acceptButton.onClick.AddListener( AcceptCrownClick );
    }

    internal void SetDefaultExecutorInfoPanelWin() {
        int currentIndex = 0;
        frameRectTransform.anchoredPosition =
            new Vector2( crownsRectTransform[currentIndex].anchoredPosition.x, frameRectTransform.anchoredPosition.y );
    }

    void SetCrown( int indexAccept ) {
        frameRectTransform.DOAnchorPosX( crownsRectTransform[indexAccept].anchoredPosition.x, timeAnimFrame );

        currentAcceptIndex = indexAccept;
    }

    void AcceptCrownClick() {
        OnSetAcceptIndex( currentAcceptIndex );
    }
}