﻿using UnityEngine;
using UnityEngine.UI;
using System;
using DG.Tweening;

public class ExecutorInfoPanelPreWin : MonoBehaviour {
    public event Action<int> OnSetCrown = ( int  args ) => { };
    [SerializeField]
    ExecutionInfo executionInfo;
    [SerializeField]
    Button[] crownsButton;
    [SerializeField]
    RectTransform frameRectTransform;

    RectTransform[] crownsRectTransform;

    int currentIndex = 0;
    float timeAnimFrame = 0.2f;

    internal void Init() {
        crownsRectTransform = new RectTransform[crownsButton.Length];
        for( int i = 0; i < crownsButton.Length; i++ ) {
            int indexCrown = i;
            crownsButton[i].onClick.AddListener( () => {
                ClickCrown( indexCrown );
            } );
            crownsRectTransform[i] = crownsButton[i].GetComponent<RectTransform>();
        }

        SetDefaultExecutorInfoPanelPreWin();
    }

    internal void SetExecutionInfo( ExecutionServ executionServ ) {
        executionInfo.SetPostData( executionServ );

        if( executionServ.tempWinPlace >= 0 ) {
            SetCrown( executionServ.tempWinPlace + 1 );
        }
        else {
            SetDefaultExecutorInfoPanelPreWin();
        }
    }

    void SetDefaultExecutorInfoPanelPreWin() {
        int currentIndex = 0;
        frameRectTransform.anchoredPosition =
            new Vector2( crownsRectTransform[currentIndex].anchoredPosition.x, frameRectTransform.anchoredPosition.y );
    }

    void SetCrown( int indexCrown ) {
        frameRectTransform.DOAnchorPosX( crownsRectTransform[indexCrown].anchoredPosition.x, timeAnimFrame );

        currentIndex = indexCrown;
    }

    void ClickCrown( int indexCrown ) {
        SetCrown( indexCrown );

        OnSetCrown( indexCrown );
    }
}
