﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ChoosePhotoPanel : MonoBehaviour {
    public event Action<ChoosePhotoPanel> OnPhotoClick = ( ChoosePhotoPanel  arg ) => { };

    [SerializeField]
    Button photoButton;
    [SerializeField]
    Image photoImage;
    [SerializeField]
    Image chooseIconImage;

    internal void Init () {
        photoButton.onClick.AddListener( PhotoClick );
    }

    //GET
    internal Sprite GetSpritePhoto () {
        return photoImage.sprite;
    }

    void PhotoClick () {
        OnPhotoClick( this );
    }

    internal void ChoosePhoto () {
        chooseIconImage.gameObject.SetActive( true );
    }

    internal void UnChoosePhoto () {
        chooseIconImage.gameObject.SetActive( false );
    }
}
