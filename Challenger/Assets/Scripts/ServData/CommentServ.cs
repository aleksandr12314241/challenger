﻿using System;

public class CommentServ {
    public long id;

    public string text;
    public DateTime creationDate;

    public long postId;
    public PostType postType;

    public UserServ author;

    public CommentServ( ChallengeCommentResponseDto comment ) {
        id = comment.id;
        text = comment.text;
        creationDate = comment.creationDate;
        postId = comment.challengeId;
    }

    public CommentServ( ExecutionCommentResponseDto comment ) {
        id = comment.id;
        text = comment.text;
        creationDate = comment.creationDate;
        postId = comment.executionId;
    }
}
