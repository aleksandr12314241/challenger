﻿
public class UserServ {
    public long id;
    public string username;
    public string filename;

    public UserServ( OnSetProfile profile ) {
        id = profile.id;
        username = profile.username;
    }

    public UserServ( UserResponseDto user ) {
        id = user.id;
        username = user.username;
        filename = user.filename;
    }
}
