﻿using System.Collections.Generic;
using System;

public class ExecutionServ: ChallengeAndExecutionServ {

    public ChallengeServ challenge;

    public short tempWinPlace;
    public short winPlace;

    public List<ContentServ> executionContents;
    public List<CommentServ> executionComments;

    public ExecutionServ( ExecutionResponseDto execution ) : base( execution ) {
        id = execution.id;
        description = execution.description;
        creationDate = Convert.ToDateTime( execution.creationDate );

        tempWinPlace = execution.tempWinPlace;
        winPlace = execution.winPlace;
    }
}
