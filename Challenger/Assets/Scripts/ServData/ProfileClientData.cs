﻿using System.Collections.Generic;
using System;
using UnityEngine;

public class ProfileClientData : MonoBehaviour {
    public static ProfileClientData instance = null; // Экземпляр объекта

    internal Action OnAddExecutionInChallengeData = () => {};

    internal List<string> stickers;

    private void Awake() {
        if( instance == null ) {  // Экземпляр менеджера был найден
            instance = this; // Задаем ссылку на экземпляр объекта
            Init();
        }
    }

    void Init() {
        stickers = new List<string>() {
            "sticker_picture_0",
            "sticker_picture_1",
            "sticker_text_0"
        };
    }
}
