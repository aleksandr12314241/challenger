﻿using Zenject;
using UnityEngine;
using System;

public class Profile {
    private long id;
    private string username;
    private string token;


    public long Id {
        get {
            return id;
        }
    }

    public void SetMainProfile( OnSetProfile onSetProfile ) {
        id = onSetProfile.id;
        username = onSetProfile.username;
        token = onSetProfile.token;
    }

    internal void OnUpdateProfile( UserResponseDto userResponseDto ) {
        username = userResponseDto.username;
    }
}
