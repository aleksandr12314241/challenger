﻿using UnityEngine;

public class NotificationCallOnChallengeServ : NotificationServ {
    internal long postId;
    internal long userId;
    internal string userName;
    internal string avatarId;
    internal Sprite avatarImage;
    internal string challengeDescription;
    internal double postTime;
    internal double endChallengeTime;
    internal ChallengeState challengeState;
    internal PostContentData postContentData;
}
