﻿using UnityEngine;

public class NotificationCommentServ : NotificationServ {
    internal long postId;
    internal long userId;
    internal string userName;
    internal string avatarId;
    internal Sprite avatarImage;
    internal PostContentData postContentData;
    internal string commentText;
}
