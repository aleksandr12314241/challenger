﻿using UnityEngine;

public class NotificationLikeServ : NotificationServ {
    internal long postId;
    internal long userId;
    internal string userName;
    internal string avatarId;
    internal Sprite avatarImage;
    internal PostContentData postContentData;
}
