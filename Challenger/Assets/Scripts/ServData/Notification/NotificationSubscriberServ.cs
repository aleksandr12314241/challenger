﻿using UnityEngine;

public class NotificationSubscriberServ : NotificationServ {
    public long userId;
    public string userName;
    public string avatarId;
    public Sprite avatarImage;
}
