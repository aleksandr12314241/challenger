﻿using System.Collections.Generic;
using System;

public class ChallengeServ : ChallengeAndExecutionServ {
    public DateTime endChallengeDate;
    public DateTime nowServerTime;
    public List<ExecutionServ> executions;

    public ChallengeServ( ChallengeResponseDto challenge ): base( challenge ) {

        endChallengeDate = Convert.ToDateTime( challenge.endChallengeDate );
        nowServerTime = Convert.ToDateTime( challenge.nowServerTime );

        executions = new List<ExecutionServ>();

        //challengeContents = new List<ContentServ>();
        //for( int i = 0; i < challenge.challengeContents.Count; i++ ) {
        //    challengeContents.Add( new ContentServ( challenge.challengeContents[i] ) );
        //}

        //challengeComments = new List<CommentServ>();
        //for( int i = 0; i < challenge.challengeComments.Count; i++ ) {
        //    challengeComments.Add( new CommentServ( challenge.challengeComments[i] ) );
        //}
    }

    public ChallengeServ( ChallengeMiniResponseDto challengeMini ) : base( challengeMini ) {

        endChallengeDate = challengeMini.endChallengeDate;
        executions = new List<ExecutionServ>();

        //challengeContents = new List<ContentServ>();
        //for( int i = 0; i < challenge.challengeContents.Count; i++ ) {
        //    challengeContents.Add( new ContentServ( challenge.challengeContents[i] ) );
        //}

        //challengeComments = new List<CommentServ>();
        //for( int i = 0; i < challenge.challengeComments.Count; i++ ) {
        //    challengeComments.Add( new CommentServ( challenge.challengeComments[i] ) );
        //}
    }

    internal void TryAddExecutionServ( ExecutionServ executionServ ) {
        ExecutionServ executionServOld = executions.Find( x => x.id == executionServ.id );
        if( executionServOld != null ) {
            return;
        }

        executions.Add( executionServ );
    }
}
