﻿using UnityEngine;

public class PostContentData {
    //TODO: путь к аватарке
    public string contentImageId;
    public Sprite contentImage;
    public string fullFilename;
    public string shortFilename;
    //прочие параметры, такие ка кнаклейки и надписи на контенте
}
