﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ContentServ {
    public string filename;
    public Sprite photo;

    public ContentServ( ChallengeContentResponseDto challenge ) {
        filename = challenge.filename;
    }

    public ContentServ( ExecutionContentResponseDto execution ) {
        filename = execution.filename;
    }
}
