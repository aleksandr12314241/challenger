﻿using System.Collections.Generic;
using System.Linq;

public class ServController {
    UserServ me;
    HashSet<UserServ> users = new HashSet<UserServ>();
    HashSet<ChallengeServ> challenges = new HashSet<ChallengeServ>();
    HashSet<ExecutionServ> executions = new HashSet<ExecutionServ>();

    internal UserServ GetUserServ( UserResponseDto user ) {
        UserServ userServ = users.Where( x => x.id == user.id ).FirstOrDefault();

        if( userServ != null ) {
            return userServ;
        }

        userServ = new UserServ( user );

        return userServ;
    }

    //TODO: получить первый челлендж========
    internal ChallengeServ GetFirstChallengeServ() {
        if( challenges.Count < 0 ) {
            return null;
        }

        return challenges.First();
    }
    //========

    internal ChallengeServ GetChallengeServ( ChallengeResponseDto challenge ) {
        ChallengeServ challengeServ = challenges.Where( x => x.id == challenge.id ).FirstOrDefault();

        if( challengeServ != null ) {
            UpdateExecutionForChallenge( challengeServ, challenge.executions );
            UpdateCommentsForChallenge( challengeServ, challenge.challengeComments );
            return challengeServ;
        }

        challengeServ = new ChallengeServ( challenge ) {
            author = GetUserServ( challenge.author )
        };

        challenges.Add( challengeServ );

        UpdateCommentsForChallenge( challengeServ, challenge.challengeComments );
        UpdateExecutionForChallenge( challengeServ, challenge.executions );

        return challengeServ;
    }

    internal ChallengeServ GetChallengeServ( ChallengeMiniResponseDto challenge ) {
        ChallengeServ challengeServ = challenges.Where( x => x.id == challenge.id ).FirstOrDefault();

        if( challengeServ != null ) {
            return challengeServ;
        }

        challengeServ = new ChallengeServ( challenge ) {
            author = GetUserServ( challenge.author )
        };

        challenges.Add( challengeServ );

        return challengeServ;
    }

    internal ExecutionServ GetExecutionServ( ExecutionResponseDto execution ) {
        ExecutionServ executionServ = executions.Where( x => x.id == execution.id ).FirstOrDefault();

        if( executionServ != null ) {
            UpdateCommentsForExecution( executionServ, execution.executionComments );

            return executionServ;
        }

        ChallengeServ challengeServ = GetChallengeServ( execution.challengeMini );

        executionServ = new ExecutionServ( execution ) {
            author = GetUserServ( execution.author )
        };

        executions.Add( executionServ );
        executionServ.challenge = challengeServ;

        challengeServ.TryAddExecutionServ( executionServ );

        UpdateCommentsForExecution( executionServ, execution.executionComments );

        return executionServ;
    }

    internal ExecutionServ GetExecutionServ( ChallengeServ challengeServ, ExecutionResponseDto execution ) {
        ExecutionServ executionServ = executions.Where( x => x.id == execution.id ).FirstOrDefault();

        if( executionServ != null ) {
            UpdateCommentsForExecution( executionServ, execution.executionComments );
            return executionServ;
        }

        executionServ = new ExecutionServ( execution ) {
            author = GetUserServ( execution.author )
        };

        executions.Add( executionServ );
        executionServ.challenge = challengeServ;
        challengeServ.TryAddExecutionServ( executionServ );

        UpdateCommentsForExecution( executionServ, execution.executionComments );

        return executionServ;
    }

    private void UpdateCommentsForExecution( ExecutionServ executionServ, List<ExecutionCommentResponseDto> comments ) {
        for( int i = 0; i < comments.Count; i++ ) {
            CommentServ commentServ = executionServ.comments.Where( x => x.id == comments[i].id ).FirstOrDefault();
            if( commentServ != null ) {
                continue;
            }

            commentServ = GetCommentServ( comments[i] );
            executionServ.comments.Add( commentServ );
        }
    }

    internal void OnSetProfile( OnSetProfile profile ) {
        me = new UserServ( profile );
    }

    private void UpdateCommentsForChallenge( ChallengeServ challengeServ, List<ChallengeCommentResponseDto> comments ) {
        for( int i = 0; i < comments.Count; i++ ) {
            CommentServ commentServ = challengeServ.comments.Where( x => x.id == comments[i].id ).FirstOrDefault();
            if( commentServ != null ) {
                continue;
            }

            commentServ = GetCommentServ( comments[i] );
            challengeServ.comments.Add( commentServ );
        }
    }

    internal void OnUpdateProfile( UserResponseDto userResponseDto ) {
        me.username = userResponseDto.username;
        me.filename = userResponseDto.filename;

        UserServ userServ = users.Where( x => x.id == userResponseDto.id ).FirstOrDefault();
        if( userServ == null ) {
            return;
        }

        userServ.username = userResponseDto.username;
        userServ.filename = userResponseDto.filename;
    }

    private void UpdateExecutionForChallenge( ChallengeServ challengeServ, List<ExecutionResponseDto> executions ) {
        for( int i = 0; i < executions.Count; i++ ) {
            GetExecutionServ( challengeServ, executions[i] );
        }
    }

    internal CommentServ GetCommentServ( ChallengeCommentResponseDto comment ) {
        return new CommentServ( comment ) {
            postType = PostType.challenge,

            author = GetUserServ( comment.author )
        };
    }

    internal CommentServ GetCommentServ( ExecutionCommentResponseDto comment ) {
        return new CommentServ( comment ) {
            postType = PostType.execution,

            author = GetUserServ( comment.author )
        };
    }

    internal void OnEndChallenge( EndChallengeResponseDto endChallengeResponseDto ) {
        ChallengeServ challengeServ = challenges.Where( x => x.id == endChallengeResponseDto.challengeId ).FirstOrDefault();

        if( challengeServ == null ) {
            return;
        }

        ExecutionServ executionFirst = challengeServ.executions.Find( x => x.id == endChallengeResponseDto.firstExecutionId );
        executionFirst.winPlace = 0;

        ExecutionServ executionSecond = challengeServ.executions.Find( x => x.id == endChallengeResponseDto.secondExecutionId );
        executionSecond.winPlace = 1;

        ExecutionServ executionThird = challengeServ.executions.Find( x => x.id == endChallengeResponseDto.thirdExecutionId );
        executionThird.winPlace = 2;
    }

    internal void OnAddNewChallengeComment( CommentServ commentServ ) {
        ChallengeServ challengeServ = challenges.Where( x => x.id == commentServ.postId ).FirstOrDefault();
        if( challengeServ == null ) {
            return;
        }

        challengeServ.comments.Add( commentServ );
    }

    internal void OnAddNewExecutionComment( CommentServ commentServ ) {
        ExecutionServ executionServ = executions.Where( x => x.id == commentServ.postId ).FirstOrDefault();
        if( executionServ == null ) {
            return;
        }

        executionServ.comments.Add( commentServ );
    }

    internal void OnUpdateExecution( ExecutionResponseDto executionResponseDto ) {
        ExecutionServ executionServ = executions.Where( x => x.id == executionResponseDto.id ).FirstOrDefault();

        if( executionServ == null ) {
            return;
        }

        executionServ.tempWinPlace = executionResponseDto.tempWinPlace;
        executionServ.winPlace = executionResponseDto.winPlace;
    }
}
