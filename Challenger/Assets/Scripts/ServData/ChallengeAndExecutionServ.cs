﻿using System.Collections.Generic;
using System;

public class ChallengeAndExecutionServ {
    public long id;
    public string description;
    public DateTime creationDate;
    public UserServ author;
    public List<ContentServ> contents;
    public List<CommentServ> comments;

    public ChallengeAndExecutionServ( ChallengeResponseDto challenge ) {
        id = challenge.id;
        description = challenge.description;
        creationDate = Convert.ToDateTime( challenge.creationDate );

        contents = new List<ContentServ>();
        for( int i = 0; i < challenge.challengeContents.Count; i++ ) {
            contents.Add( new ContentServ( challenge.challengeContents[i] ) );
        }

        comments = new List<CommentServ>();
    }

    public ChallengeAndExecutionServ( ChallengeMiniResponseDto challengeMini ) {
        id = challengeMini.id;
        description = challengeMini.description;
        creationDate = challengeMini.creationDate;

        contents = new List<ContentServ>();
        contents.Add( new ContentServ( challengeMini.challengeContent ) );

        comments = new List<CommentServ>();
    }

    public ChallengeAndExecutionServ( ExecutionResponseDto execution ) {
        id = execution.id;
        description = execution.description;
        creationDate = Convert.ToDateTime( execution.creationDate );

        contents = new List<ContentServ>();
        for( int i = 0; i < execution.executionContents.Count; i++ ) {
            contents.Add( new ContentServ( execution.executionContents[i] ) );
        }

        comments = new List<CommentServ>();
    }
}
