﻿using UnityEngine;
using System.IO;
using System.Text;
using Zenject;

public class SaveSystem : MonoBehaviour {
    [Inject]
    SignalBus signalBus;

    Save save;

    public Save GetSave {
        get {
            return save;
        }
    }

    private void Start() {
        MainSerializer();
    }

    private void MainSerializer() {
        string savePath = GetPathSaves();

        save = new Save();

        if( true ) {
            //if( !File.Exists( savePath ) ) {
            save.username = "";
            save.password = "";
            save.token = "";

            SaveResult( save );
        }
        else {
            save = ReadSave();

            //networkController.SetToken( save.token );

            signalBus.Fire( new OnAutoLogin() {
                username = save.username,
                password = save.password
            } );
        }
    }

    public void SaveUsernameAndPassword( OnRegistrationSucces onRegistrationSucces ) {
        save.username = onRegistrationSucces.username;
        save.password = onRegistrationSucces.password;

        SaveResult( save );
    }

    internal void SaveToken( string token ) {
        save.token = token;
        SaveResult( save );
    }

    private void SaveResult( Save save ) {
        return;

        //        string savePath = GetPathSaves();
        //        string json = JsonConvert.SerializeObject( newPlayer );

        //        byte[] bytes = Encoding.UTF8.GetBytes( json );
        //        string hex = BitConverter.ToString( bytes );

        //#if UNITY_EDITOR
        //        string savePathDecipher = Application.dataPath+"/Data/saves_decipher.json";
        //        File.WriteAllText( savePathDecipher, json, Encoding.UTF8 );
        //#endif

        string savePath = GetPathSaves();
        string json = JsonUtility.ToJson( save );

        string savePathDecipher = Application.dataPath + "/Data/saves_decipher.json";
        File.WriteAllText( savePathDecipher, json, Encoding.UTF8 );
    }

    private Save ReadSave() {
        string savePath = GetPathSaves();
        string jsonString = File.ReadAllText( savePath );
        return JsonUtility.FromJson<Save>( jsonString );
    }

    private string GetPathSaves() {

        string savePath;
#if UNITY_EDITOR
        savePath = Path.Combine( Application.dataPath, "Data/saves.json" );
#elif UNITY_ANDROID||UNITY_IOS
        savePath = Path.Combine( Application.persistentDataPath, "saves.json" );
#else
        savePath = Path.Combine( Application.persistentDataPath, "saves.json" );
#endif
        return savePath;
    }
}
