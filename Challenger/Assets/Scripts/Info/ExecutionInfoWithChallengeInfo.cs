﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ExecutionInfoWithChallengeInfo : ExecutionInfo {
    [SerializeField]
    Button challengeButton;
    [SerializeField]
    Image challengePhotoImage;
    [SerializeField]
    Text challengeDescription;

    [Inject]
    SpriteLoader spriteLoader;

    ChallengeInfoWindow challengeEntreyInfoWindow;

    internal override void Init( ExecutionServ execution ) {
        base.Init( execution );

        challengeEntreyInfoWindow = FindObjectOfType<ChallengeInfoWindow>();

        challengeButton.onClick.AddListener( ShowChallengeInfoWindow );

        challengeDescription.text = execution.challenge.description;
        if( execution.challenge.contents != null && execution.challenge.contents[0] != null ) {
            GetRemoteTexture( execution.challenge.contents[0].filename );
        }
    }

    void GetRemoteTexture( string filePath ) {
        if( filePath == null || filePath == "" ) {
            return;
        }

        spriteLoader.LoadSpriteContent( filePath, ( sprite ) => {
            challengePhotoImage.sprite = sprite;
        } );
    }

    void ShowChallengeInfoWindow() {
        challengeEntreyInfoWindow.StartShow( execution.challenge );
    }
}
