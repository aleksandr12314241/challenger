﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;
using System.Threading.Tasks;
//using System;

public abstract class PostInfo: MonoBehaviour {
    [SerializeField]
    protected RectTransform myRectTransform;
    [SerializeField]
    protected Button followButton;
    [SerializeField]
    protected Image followDoneImage;
    [SerializeField]
    protected Button likeButton;
    [SerializeField]
    protected Image likeFillImage;
    [SerializeField]
    protected Text likeCountText;
    [SerializeField]
    protected RectTransform likeRectTransform;
    [SerializeField]
    protected Button commentButton;
    [SerializeField]
    protected RectTransform commentRectTransform;
    [SerializeField]
    protected Image avatarImage;
    [SerializeField]
    protected Text nameUserText;
    [SerializeField]
    protected RectTransform contentPanelRectTransform;
    [SerializeField]
    protected PanelEntryContentField[] contentPanelArray;
    [SerializeField]
    RectTransform contentLinesPanelRectTransform;
    [SerializeField]
    protected Image[] contentPanelLinesArray;
    [SerializeField]
    protected DescriptionPostInfo descriptionPostInfo;

    [Inject]
    protected NetworkController networkController;
    [Inject]
    private SpriteLoader spriteLoader;
    [Inject]
    protected SignalBus signalBus;
    [Inject]
    protected ServController servController;

    TouchScreenKeyboard keyboard;

    private UserServ author;

    internal RectTransform MyRectTransform {
        get {
            return myRectTransform;
        }
    }
    internal DescriptionPostInfo DescriptionPostInfo {
        get {
            return descriptionPostInfo;
        }
    }
    internal UserServ Author {
        get {
            return author;
        }
    }

    internal virtual void Init( ChallengeAndExecutionServ challengeAndExecution ) {
        SetPostData( challengeAndExecution );

        SetHeightPanel();
    }

    internal void SetPostData( ChallengeAndExecutionServ challengeAndExecution ) {
        author = challengeAndExecution.author;

        descriptionPostInfo.Init();

        commentButton.onClick.RemoveAllListeners();
        commentButton.onClick.AddListener( CommentClick );

        //avatarImage.sprite = challengeAndExecutorData.avatarImage;
        nameUserText.text = author.username;

        SetRemoteTextureAvatar( author.filename );

        if( challengeAndExecution.contents == null || challengeAndExecution.contents[0] == null ) {
            contentPanelRectTransform.gameObject.SetActive( false );
            contentPanelRectTransform.sizeDelta = new Vector2( contentPanelRectTransform.sizeDelta.x, 0f );
        }
        else {
            contentPanelRectTransform.gameObject.SetActive( true );

            contentPanelRectTransform.sizeDelta = new Vector2( contentPanelRectTransform.sizeDelta.x, 415f );
            contentPanelArray[0].Init( challengeAndExecution.contents[0] );

            if( challengeAndExecution.contents[0].filename != null ) {
                SetRemoteTextureContent( challengeAndExecution.contents[0].filename );
            }
        }

        descriptionPostInfo.SetDescriptionText( challengeAndExecution.description );
    }

    void SetRemoteTextureAvatar( string filePath ) {
        spriteLoader.LoadSpriteAvatar( filePath, ( sprite ) => {
            avatarImage.sprite = sprite;
        } );
    }

    void SetRemoteTextureContent( string filePath ) {

        spriteLoader.LoadSpriteContent( filePath, ( sprite ) => {
            contentPanelArray[0].Init( sprite );
        } );
    }

    protected abstract void SetHeightPanel();

    protected void LikeClick() {
        if( likeFillImage.gameObject.activeSelf ) {
            likeFillImage.gameObject.SetActive( false );
        }
        else {
            likeFillImage.gameObject.SetActive( true );
        }
    }

    void CommentClick() {
#if UNITY_EDITOR
        SendComment( "test_" + Random.Range( 0, 100 ) );
#else
        keyboard = TouchScreenKeyboard.Open( "", TouchScreenKeyboardType.Default, false, false, false, true );
#endif
    }

    protected void SetFollowButtonState( bool isSubscriber ) {
        followButton.gameObject.SetActive( !isSubscriber );
        followDoneImage.gameObject.SetActive( isSubscriber );
    }

    void Update() {
        if( keyboard != null && keyboard.status == TouchScreenKeyboard.Status.Done ) {
            string commentStr = keyboard.text;
            if( commentStr != null && commentStr != "" ) {
                SendComment( commentStr );
            }
            keyboard = null;
        }
    }

    protected abstract void SendComment( string commentStr );

    internal void UpdateAuthorInfo( UserServ userServ ) {
        SetRemoteTextureAvatar( userServ.filename );
    }
}
