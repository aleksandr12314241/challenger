﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class ChallengeInfo : PostInfo {
    [SerializeField]
    JoinButtonPanel joinButtonPanel;
    [SerializeField]
    RectTransform joinToChallengeRectTransform;
    [SerializeField]
    Timer leftTimeTimer;
    [SerializeField]
    RectTransform leftTimeRectTransform;

    ChallengeServ challenge;

    internal virtual void Init( ChallengeServ challenge ) {
        this.challenge = challenge;

        base.Init( challenge );

        joinButtonPanel.Init( challenge );

        this.challenge = challenge;

        followButton.onClick.AddListener( FollowClick );

        likeButton.onClick.AddListener( LikeClickChallenge );

        if( challenge.endChallengeDate > challenge.nowServerTime ) {
            TimeSpan restTime = challenge.endChallengeDate - challenge.nowServerTime;

            DateTime dateTimeEnd = DateTime.Now.AddSeconds( restTime.TotalSeconds );
            leftTimeTimer.StartTimer( dateTimeEnd );
        }

        joinButtonPanel.CheckChallengeButtonState();
    }

    protected override void SetHeightPanel() {
        float joinToChallengeOffsetPosY = 72f;
        float leftTimeOffsetPosY = 55f;
        float myRecttransformOffsetPosY = 154f;

        contentPanelRectTransform.anchoredPosition =
            new Vector2( contentPanelRectTransform.anchoredPosition.x,
                         descriptionPostInfo.DescriptionRectTransform.anchoredPosition.y - descriptionPostInfo.DescriptionRectTransform.sizeDelta.y - 5f );

        float startLikePosY = contentPanelRectTransform.anchoredPosition.y - contentPanelRectTransform.sizeDelta.y - 5f;

        joinToChallengeRectTransform.anchoredPosition =
            new Vector2( joinToChallengeRectTransform.anchoredPosition.x, startLikePosY - joinToChallengeOffsetPosY );
        leftTimeRectTransform.anchoredPosition = new Vector2( leftTimeRectTransform.anchoredPosition.x, startLikePosY - leftTimeOffsetPosY );
        likeRectTransform.anchoredPosition = new Vector2( likeRectTransform.anchoredPosition.x, startLikePosY );
        commentRectTransform.anchoredPosition = new Vector2( commentRectTransform.anchoredPosition.x, startLikePosY );

        myRectTransform.sizeDelta = new Vector2( myRectTransform.sizeDelta.x, ( startLikePosY * -1 ) + myRecttransformOffsetPosY );
    }

    void FollowClick() {
        //TODO: отбработка клика подписки
        //подписаться
        //postChallengeData.isSubscriber = !postChallengeData.isSubscriber;
        //SetFollowButtonState( postChallengeData.isSubscriber );
    }

    protected void LikeClickChallenge() {
        if( likeFillImage.gameObject.activeSelf ) {
            likeFillImage.gameObject.SetActive( false );
        }
        else {
            likeFillImage.gameObject.SetActive( true );
        }
    }

    protected override void SendComment( string commentStr ) {
        WWWForm form = new WWWForm();
        form.AddField( "id", challenge.id.ToString() );
        form.AddField( "text", commentStr );

        networkController.Post( "challengecomment", form,
        action: ( response ) => {
            ChallengeCommentResponseDto requese = JsonUtility.FromJson<ChallengeCommentResponseDto>( response );

            CommentServ commentServ = servController.GetCommentServ( requese );

            signalBus.Fire( new OnAddNewChallengeComment() {
                comment = commentServ
            } );
        },
        actionError: ( responseError ) => {
            Debug.Log( "error new challenge comment" );
        } );
    }
}
