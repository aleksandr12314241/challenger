﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class TimelineDescriptionPostInfo : DescriptionPostInfo {
    public event Action OnDetailsChallengeButtonClick = () => { };

    [SerializeField]
    Button detailsChallengeButton;

    internal override void Init () {
        detailsChallengeButton.onClick.AddListener( DetailsChallengeButtonClick );
    }

    internal override void SetDescriptionText ( string challengeAndExecutorDataDescription ) {
        float maxTextDeltaSizeY = 105f;
        float descriptionTextSizeDelta;

        string additionalString = " Подробнее...";
        string ellipsisString = "...";

        string descriptionString = challengeAndExecutorDataDescription + additionalString;
        descriptionText.text = descriptionString;

        if ( descriptionText.preferredHeight > maxTextDeltaSizeY ) {
            for ( int i = 0; i < descriptionString.Length; i++ ) {
                string tempString =
                    challengeAndExecutorDataDescription.Substring( 0, challengeAndExecutorDataDescription.Length - i * 2 );
                descriptionText.text = tempString + ellipsisString + additionalString;

                if ( descriptionText.preferredHeight <= maxTextDeltaSizeY ) {
                    descriptionString = tempString + ellipsisString + "<color=blue>" + additionalString + "</color>";
                    break;
                }
            }
        }
        else {
            descriptionString = challengeAndExecutorDataDescription + "<color=blue>" + additionalString + "</color>";
        }

        descriptionText.text = descriptionString;
        descriptionTextSizeDelta = descriptionText.preferredHeight;
        descriptionRectTransform.sizeDelta = new Vector2( descriptionRectTransform.sizeDelta.x, descriptionTextSizeDelta );
    }

    void DetailsChallengeButtonClick () {
        OnDetailsChallengeButtonClick();
    }
}
