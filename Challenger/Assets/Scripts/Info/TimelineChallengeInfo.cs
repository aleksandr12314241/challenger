﻿using UnityEngine;
using UnityEngine.UI;

public class TimelineChallengeInfo : ChallengeInfo {
    [SerializeField]
    Button detailsChallengeButton;

    ChallengeInfoWindow challengeEntreyInfoWindow;
    TimelineDescriptionPostInfo timelineDescriptionPostInfo;

    internal override void Init( ChallengeServ challenge ) {
        base.Init( challenge );

        challengeEntreyInfoWindow = FindObjectOfType<ChallengeInfoWindow>();

        timelineDescriptionPostInfo = ( TimelineDescriptionPostInfo ) descriptionPostInfo;
        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick += ShowDetailsChallenge;
    }


    internal void ShowDetailsChallenge() {
        //challengeEntreyInfoWindow.StartShow( challengeStateTemp );
    }

    private void OnDestroy() {
        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick -= ShowDetailsChallenge;
    }
}
