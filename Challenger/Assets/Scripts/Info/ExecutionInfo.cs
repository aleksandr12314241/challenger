﻿using UnityEngine;


public class ExecutionInfo: PostInfo {

    protected ExecutionServ execution;
    //internal ExecutionServ Execution {
    //    get {
    //        return execution;
    //    }
    //}

    internal virtual void Init( ExecutionServ execution ) {
        base.Init( execution );

        this.execution = execution;
    }

    protected override void SetHeightPanel() {
        float myRecttransformOffsetPosY = 79f;

        float startLikePosY =
            descriptionPostInfo.DescriptionRectTransform.anchoredPosition.y - descriptionPostInfo.DescriptionRectTransform.sizeDelta.y - 25f;
        likeRectTransform.anchoredPosition = new Vector2( likeRectTransform.anchoredPosition.x, startLikePosY );
        commentRectTransform.anchoredPosition = new Vector2( commentRectTransform.anchoredPosition.x, startLikePosY );
        myRectTransform.sizeDelta = new Vector2( myRectTransform.sizeDelta.x, ( startLikePosY * -1 ) + myRecttransformOffsetPosY );
    }

    protected override void SendComment( string commentStr ) {
        WWWForm form = new WWWForm();
        form.AddField( "id", execution.id.ToString() );
        form.AddField( "text", commentStr );

        networkController.Post( "executioncomment", form,
        action: ( response ) => {
            ExecutionCommentResponseDto requese = JsonUtility.FromJson<ExecutionCommentResponseDto>( response );

            CommentServ commentServ = servController.GetCommentServ( requese );

            signalBus.Fire( new OnAddNewExecutionComment() {
                comment = commentServ
            } );
        },
        actionError: ( responseError ) => {
            Debug.Log( "error new challenge comment" );
        } );
    }
}
