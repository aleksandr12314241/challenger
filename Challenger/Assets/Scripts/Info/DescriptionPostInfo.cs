﻿using UnityEngine;
using UnityEngine.UI;

public class DescriptionPostInfo : MonoBehaviour {
    [SerializeField]
    protected Text descriptionText;
    [SerializeField]
    protected RectTransform descriptionRectTransform;

    internal Text DescriptionText {
        get {
            return descriptionText;
        }
    }

    internal RectTransform DescriptionRectTransform {
        get {
            return descriptionRectTransform;
        }
    }

    internal virtual void Init () {
    }

    internal virtual void SetDescriptionText ( string challengeAndExecutorDataDescription ) {
        descriptionRectTransform.sizeDelta = new Vector2( descriptionRectTransform.sizeDelta.x, descriptionText.preferredHeight );
        descriptionText.text = challengeAndExecutorDataDescription;
    }
}
