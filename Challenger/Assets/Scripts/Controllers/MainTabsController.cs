﻿using UnityEngine;
using System;

namespace Common {
    public class MainTabsController: MonoBehaviour {
        internal event Action<int> OnOpenTab = ( int param ) => { };

        [SerializeField]
        TabAndButton[] tabs;

        private int openTab = -1;

        internal int OpenTab {
            get {
                return openTab;
            }
        }

#if UNITY_EDITOR
        private void OnValidate () {
            if ( tabs.Length <= 0 ) {
                Debug.LogError( "|TabsContainer|<OnValidate> массив табо пустой" );
                return;
            }

            for ( int i = 0; i < tabs.Length; i++ ) {
                if ( tabs[i].Button == null ) {
                    Debug.LogError( "|TabsContainer|<OnValidate>  у " + i + "ого таба кнопка =  null" );
                    return;
                }
                else if ( tabs[ i ].Tab == null ) {
                    Debug.LogError( "|TabsContainer|<OnValidate>  у " + i + "ого таба таб =  null" );
                    return;
                }
            }
        }
#endif

        void Start () {
            for ( var i = 0; i < tabs.Length; i++ ) {
                var index = i;
                tabs[i].Tab.Init();
            }

            for ( var i = 0; i < tabs.Length; i++ ) {
                var index = i;
                tabs[ i ].Button.tabButton.onClick.AddListener( () => {
                    ShowTab( index );
                } );
            }

            for ( int i = 0; i < tabs.Length; i++ ) {
                tabs[i].Init();
            }

            openTab = 0;
            tabs[openTab].ShowTab();
            for ( int i = 0; i < tabs.Length; i++ ) {
                if ( openTab == i ) {
                    continue;
                }

                tabs[i].HideForceTab();
            }
        }

        private void ShowTab ( int indexTab ) {

            if ( tabs.Length < 0 ) {
                Debug.Log( "|TabsContainer|<OnValidate> массивом  = " + tabs.Length + " пуст " );
                return;
            }

            if ( tabs.Length <= indexTab ) {
                Debug.Log( "|TabsContainer|<OnValidate> слишком большой индекс = " + indexTab +
                           " по сравнениею с массивом  = " + tabs.Length );
                return;
            }

            openTab = indexTab;
            for ( int i = 0; i < tabs.Length; i++ ) {
                if ( tabs[i] == null ) {
                    Debug.Log( "|TabsContainer|<OnValidate> экземпляр класса _tabs[" + i + "]  = null" );
                    return;
                }

                if ( tabs[i].Tab == null ) {
                    Debug.Log( "|TabsContainer|<OnValidate> экземпляр класса _tabs[" + i + "].Tab  = null" );
                    return;
                }

                if ( indexTab == i ) {
                    tabs[i].ShowTab();
                }
                else {
                    tabs[i].HideTab();
                }
            }
            OnOpenTab( indexTab );
        }

        void OnAllowDailyPuzzle () {
            tabs[1].Button.ActivateButton();
        }

        void OnAllowShop () {
            tabs[3].Button.ActivateButton();
        }

        [System.Serializable]
        private class TabAndButton {
            [SerializeField]
            internal Tab Tab;
            [SerializeField]
            internal TabButton Button;
            [SerializeField]
            internal GameObject TabUpper;

            internal void Init () {
                Button.Init();
            }

            internal void ShowTab () {
                Tab.ShowTab();
                Button.ShowTab();
                TabUpper.gameObject.SetActive( true );
            }

            internal void HideTab () {
                Tab.HideTab();
                Button.HideTab();
                TabUpper.gameObject.SetActive( false );
            }

            internal void HideForceTab () {
                Tab.HideForce();
                Button.HideTab();
                TabUpper.gameObject.SetActive( false );
            }
        }
    }
}


