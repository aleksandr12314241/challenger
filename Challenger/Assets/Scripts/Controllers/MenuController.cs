﻿using UnityEngine;

public class MenuController : MonoBehaviour {
    [SerializeField]
    Sprite[] crownsSprites;
    [SerializeField]
    StickerDataSet stickerDataSet;

    internal Sprite[] CrownsSprites {
        get {
            return crownsSprites;
        }
    }

    internal StickerDataSet StickerDataSet {
        get {
            return stickerDataSet;
        }
    }
}
