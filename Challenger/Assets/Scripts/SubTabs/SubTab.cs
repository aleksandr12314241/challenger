﻿using UnityEngine;

public abstract class SubTab : MonoBehaviour {
    [SerializeField]
    protected RectTransform contentRectTransform;
    [SerializeField]
    protected GameObject backBlockGameObject;

    protected float widthScreen;
    protected float timeShowSubTab;

    bool isShow;

    internal virtual void StartInit () {
        Init();
    }
    protected void Init () {
        Canvas mainCanvas = FindObjectOfType<Canvas>();
        RectTransform canvasRectTransform = mainCanvas.GetComponent<RectTransform>();
        widthScreen = canvasRectTransform.rect.width;
        timeShowSubTab = 0.15f;
    }

    internal virtual void StartShow () {
        Show();
    }
    protected void Show () {
        isShow = true;
        contentRectTransform.gameObject.SetActive( isShow );
        backBlockGameObject.gameObject.SetActive( isShow );
    }

    internal virtual void StartHide () {
        Hide();
    }
    protected void Hide () {
        isShow = false;
        contentRectTransform.gameObject.SetActive( isShow );
        backBlockGameObject.gameObject.SetActive( isShow );
    }
}
