﻿using System;

public class ErrorArgs {
    public DateTime timestamp;
    public int status;
    public string error;
    public string message;
    public string path;
}
