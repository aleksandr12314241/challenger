﻿public class RegistrationResponseDto {
    public long id;

    public string username;
    public string password;

    public RegistrationRequestStatus status;
}
