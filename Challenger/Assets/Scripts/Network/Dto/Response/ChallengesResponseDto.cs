﻿using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ChallengesResponseDto {
    public List<ChallengeResponseDto> challenges;
}
