﻿using System;

[Serializable]
public class EndChallengeResponseDto {
    public long challengeId;
    public long firstExecutionId;
    public long secondExecutionId;
    public long thirdExecutionId;
}
