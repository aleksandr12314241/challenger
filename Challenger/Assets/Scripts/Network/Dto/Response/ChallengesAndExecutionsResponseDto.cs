﻿using System.Collections.Generic;
using System;

[Serializable]
public class ChallengesAndExecutionsResponseDto {
    public List<ChallengeResponseDto> challenges;
    public List<ExecutionResponseDto> executions;
}
