﻿using System.Collections.Generic;
using System;

[Serializable]
public class ExecutionResponseDto {
    public long id;
    public string description;

    public string creationDate;

    public short tempWinPlace;
    public short winPlace;

    //public LocalDateTime creationDate;
    public UserResponseDto author;
    public ChallengeMiniResponseDto challengeMini;
    public List<ExecutionContentResponseDto> executionContents;
    public List<ExecutionCommentResponseDto> executionComments;
}
