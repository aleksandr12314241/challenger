﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class ExecutionCommentResponseDto {
    public long id;
    public string text;
    public UserResponseDto author;
    public DateTime creationDate;
    public long executionId;
}

