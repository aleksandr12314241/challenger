﻿using System;

[Serializable]
public class UserResponseDto {
    public long id;
    public string username;
    public string filename;
}
