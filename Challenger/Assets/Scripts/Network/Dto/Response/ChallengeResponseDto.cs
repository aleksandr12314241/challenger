﻿using System;
using System.Collections.Generic;

[Serializable]
public class ChallengeResponseDto {
    public long id;
    public string description;

    public string creationDate;
    public string endChallengeDate;
    public string nowServerTime;

    public UserResponseDto author;
    public List<ChallengeContentResponseDto> challengeContents;
    public List<ChallengeCommentResponseDto> challengeComments;
    public List<ExecutionResponseDto> executions;
}
