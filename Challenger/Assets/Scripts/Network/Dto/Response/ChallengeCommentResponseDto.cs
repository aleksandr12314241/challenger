﻿using System;

[Serializable]
public class ChallengeCommentResponseDto {
    public long id;
    public string text;
    public UserResponseDto author;
    public DateTime creationDate;
    public long challengeId;
}
