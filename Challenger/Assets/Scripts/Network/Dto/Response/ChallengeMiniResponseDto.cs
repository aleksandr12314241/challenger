﻿using System;

[Serializable]
public class ChallengeMiniResponseDto {
    public long id;
    public string description;
    public DateTime creationDate;
    public DateTime endChallengeDate;
    public UserResponseDto author;
    public ChallengeContentResponseDto challengeContent;
}
