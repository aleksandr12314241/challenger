﻿using System;

[Serializable]
public class ProfileResponseDto {
    public UserResponseDto userResponseDto;
    public ChallengesAndExecutionsResponseDto challengesAndExecutionsResponseDto;
}
