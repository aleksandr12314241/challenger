﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class NetworkManager: MonoBehaviour {
    public static NetworkManager instance;

    private void Awake () {

        if ( instance == null ) {
            instance = this;
        }
        else if ( instance == this ) {
            Destroy( gameObject );
        }

        DontDestroyOnLoad( gameObject );

        InitializeManager();
    }

    private void InitializeManager () {
        /* TODO: Здесь мы будем проводить инициализацию */
    }

    public void Send ( string rpcMethod, object args ) {

    }

    //TODO
    public void Send ( string rpcMethod, object args, Action callBack ) {
        callBack();
    }
}
