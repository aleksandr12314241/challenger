﻿using UnityEngine;
using UnityEngine.UI;

public class ExecutionEntry : MonoBehaviour {
    [SerializeField]
    Image contentImage;
    [SerializeField]
    JoinButtonPanel joinButtonPanel;
    [SerializeField]
    Timer leftTimer;
    [SerializeField]
    TimelineDescriptionPostInfo timelineDescriptionPostInfo;

    ChallengeInfoWindow challengeInfoWindow;
    CreateExecutionWindow createExecutionWindow;

    ExecutionServ executionServ;

    ChallengeServ challengeServ;

    internal void Init( ExecutionServ executionServ ) {
        this.executionServ = executionServ;

        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick += OnDetailsChallengeButtonClick;

        joinButtonPanel.Init( executionServ );
        challengeInfoWindow = FindObjectOfType<ChallengeInfoWindow>();
        createExecutionWindow = FindObjectOfType<CreateExecutionWindow>();

        SetChallengeData();
    }

    void SetChallengeData() {
        //TODO:
        //contentImage.sprite = executionData.postContentData.contentImage;
        timelineDescriptionPostInfo.SetDescriptionText( challengeServ.description );
    }

    void OnDetailsChallengeButtonClick() {
        challengeInfoWindow.StartShow( challengeServ );
    }
}
