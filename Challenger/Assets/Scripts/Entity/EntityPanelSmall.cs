﻿using UnityEngine;
using UnityEngine.UI;

public abstract class EntityPanelSmall : MonoBehaviour {
    [SerializeField]
    RectTransform myRectTransform;
    [SerializeField]
    protected Button challengeEntreySmallButton;
    [SerializeField]
    protected Image photoImage;
    [SerializeField]
    protected Image backPhotoImage;
    [SerializeField]
    protected Text description;
    [SerializeField]
    protected Text challengeDescription;

    internal RectTransform MyRectTransform {
        get {
            return myRectTransform;
        }
    }

    internal virtual void Init() {
    }

    protected abstract void EntreyPanelSmallClick();
}
