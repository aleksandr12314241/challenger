﻿using UnityEngine;
using UnityEngine.UI;
using System;

public class TopPlaceEntry : MonoBehaviour {
    event Action OnAddPhotoClick = () => { };

    [SerializeField]
    Button addPhotoButton;
    [SerializeField]
    InputField presentText;
    [SerializeField]
    Text countCoinsText;
    [SerializeField]
    Image photoImage;
    [SerializeField]
    GameObject photoMaskGameObject;
    [SerializeField]
    Image photoIconImage;

    int indexTopPlace;

    internal void Init ( int indexTopPlace ) {
        addPhotoButton.onClick.AddListener( AddPhotoClick );
        this.indexTopPlace = indexTopPlace;
    }

    void AddPhotoClick () {
        OnAddPhotoClick();
    }
}
