﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ExecutionSmallInChallengeInfo : EntityPanelSmall {
    public event Action<ExecutionSmallInChallengeInfo> OnExecutionEntityPanelSmallClick = ( ExecutionSmallInChallengeInfo arg ) => { };

    [SerializeField]
    Image frameImage;

    [Inject]
    protected SpriteLoader spriteLoader;

    protected ExecutionServ execution;
    public ExecutionServ Execution {
        get {
            return execution;
        }
    }

    internal virtual void Init( ExecutionServ execution ) {
        this.execution = execution;

        base.Init();

        challengeEntreySmallButton.onClick.AddListener( EntreyPanelSmallClick );

        spriteLoader.LoadSpriteContent( execution.contents[0].filename, ( sprite ) => {
            photoImage.sprite = sprite;
            backPhotoImage.sprite = sprite;
        } );

        description.text = execution.description;
        challengeDescription.text = execution.challenge.description;
    }

    protected override void EntreyPanelSmallClick() {
        OnExecutionEntityPanelSmallClick( this );
    }

    internal void ShowChoose() {
        frameImage.gameObject.SetActive( true );
    }

    internal void HideChoose() {
        frameImage.gameObject.SetActive( false );
    }

    public class Factory : PlaceholderFactory<ExecutionSmallInChallengeInfo> {
    }
}
