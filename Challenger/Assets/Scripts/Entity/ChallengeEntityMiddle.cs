﻿using UnityEngine;
using UnityEngine.UI;

public class ChallengeEntityMiddle : ChallengeEntitySmall {
    [SerializeField]
    Image avatarImage;

    ChallengeInfoWindow challengeInfoWindow;

    internal override void Init( ChallengeServ challenge ) {
        base.Init( challenge );

        challengeInfoWindow = FindObjectOfType<ChallengeInfoWindow>();

        //avatarImage.sprite = postChallengeMiddleData.avatarImage;
        //descriptionText.cachedTextGenerator.characterCountVisible
    }

    internal override float GetPosY() {
        return MyRectTransform.anchoredPosition.y;
    }
}
