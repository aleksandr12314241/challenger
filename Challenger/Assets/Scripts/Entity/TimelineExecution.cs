﻿using Zenject;
using UnityEngine;

public class TimelineExecution : ExecutionInfoWithChallengeInfo {

    [Inject]
    Profile profile;

    TimelineDescriptionPostInfo timelineDescriptionPostInfo;
    ExecutionInfoWindow executorInfoWindow;

    internal override void Init( ExecutionServ execution ) {
        base.Init( execution );

        executorInfoWindow = FindObjectOfType<ExecutionInfoWindow>();

        timelineDescriptionPostInfo = ( TimelineDescriptionPostInfo ) descriptionPostInfo;
        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick += ShowExecutorWindow;
    }


    internal void ShowExecutorWindow() {
        executorInfoWindow.StartShow( execution );
    }

    private void OnDestroy() {
        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick -= ShowExecutorWindow;
    }

    public class Factory : PlaceholderFactory<TimelineExecution> {
    }
}
