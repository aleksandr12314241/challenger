﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Zenject;

public class ExecutionEntreySmallPreWin: ExecutionSmallInChallengeInfo {
    public event Action<ExecutionEntreySmallPreWin> OnEntreyPanelSmallWinClick = ( ExecutionEntreySmallPreWin args ) => { };

    [SerializeField]
    Image crownImage;

    [Inject]
    NetworkController networkController;
    [Inject]
    SignalBus signalBus;

    MenuController menuController;
    int place;

    internal override void Init( ExecutionServ execution ) {
        this.execution = execution;

        //base.Init( execution );

        place = execution.tempWinPlace;

        menuController = FindObjectOfType<MenuController>();

        challengeEntreySmallButton.onClick.AddListener( EntreyPanelSmallClick );

        spriteLoader.LoadSpriteContent( execution.contents[0].filename, ( sprite ) => {
            photoImage.sprite = sprite;
            backPhotoImage.sprite = sprite;
        } );

        description.text = execution.description;
        challengeDescription.text = execution.challenge.description;

        if( execution.tempWinPlace != -1 ) {
            crownImage.sprite = menuController.CrownsSprites[execution.tempWinPlace];
            crownImage.gameObject.SetActive( true );
        }
    }

    protected override void EntreyPanelSmallClick() {
        OnEntreyPanelSmallWinClick( this );
    }

    internal void RemoveCrownType() {
        crownImage.gameObject.SetActive( false );
        place = -1;
    }

    internal void SetCrownType( int place ) {
        if( this.place == place ) {
            this.place = -1;
            crownImage.gameObject.SetActive( false );

        }
        else {
            this.place = place;
            crownImage.gameObject.SetActive( true );
            crownImage.sprite = menuController.CrownsSprites[place];
        }

        ExecutionEditRequestDto executionEditRequestDto = new ExecutionEditRequestDto() {
            tempWinPlace = ( short )this.place,
            winPlace = execution.winPlace
        };

        string json = JsonUtility.ToJson( executionEditRequestDto );

        networkController.Put( "execution/" + Execution.id, executionEditRequestDto, ( response ) => {
            ExecutionResponseDto executionResponseDto = JsonUtility.FromJson<ExecutionResponseDto>( response );

            signalBus.Fire( new OnUpdateExecution() {
                executionResponseDto = executionResponseDto
            } );
        } );
    }

    public class Factory : PlaceholderFactory<ExecutionEntreySmallPreWin> {
    }
}
