﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BuyFramePanelEntry : MonoBehaviour {
    [SerializeField]
    Button buyButton;
    [SerializeField]
    Image avatarPhotoImage;
    [SerializeField]
    Image frameImage;
    [SerializeField]
    Text priceText;

    internal void Init() {
        buyButton.onClick.AddListener( BuyClick );
    }

    void BuyClick() {

    }
}
