﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class Comment : MonoBehaviour {
    [SerializeField]
    RectTransform myRectTransform;
    [SerializeField]
    Image avatar;
    [SerializeField]
    Text commentText;
    [SerializeField]
    Text timeText;
    [SerializeField]
    Text likeCount;
    [SerializeField]
    Image likeFill;
    [SerializeField]
    Button likeButton;
    [SerializeField]
    Button likeUserButton;
    [SerializeField]
    RectTransform avatarRectTransform;
    [SerializeField]
    RectTransform commentRectTransform;
    [SerializeField]
    RectTransform likeRectTransform;
    [SerializeField]
    RectTransform timeRectTransform;

    [Inject]
    SpriteLoader spriteLoader;

    CommentServ comment;

    internal RectTransform MyRectTransform {
        get {
            return myRectTransform;
        }
    }

    internal void Init( CommentServ comment ) {
        this.comment = comment;

        spriteLoader.LoadSpriteAvatar( comment.author.filename, ( sprite ) => {
            avatar.sprite = sprite;
        } );

        commentText.text = comment.text;

        timeText.text = comment.creationDate.ToString();
        //likeCount.text = commentData.likeUser.Count.ToString();
        //likeCount.text = ( commentData.likeUser.Count + Random.Range( 0, 10 ) ).ToString();
        //TODO: проверка, есть ли мой лайк
        //TODO: рандомное заполнение, если ли лайк или нет
        likeFill.gameObject.SetActive( Random.Range( 0, 2 ) == 0 );

        int maxCountLettersInLine = 35;
        float sizeDeltaYOneLine = 30f;

        float sizeDeltaDescriptionText = sizeDeltaYOneLine *
                                         ( ( ( comment.text.Length ) / maxCountLettersInLine ) + 1 );
        commentRectTransform.sizeDelta = new Vector2( commentRectTransform.sizeDelta.x, sizeDeltaDescriptionText );

        float offsetPosY = 5f;

        RectTransform measuringRectTransform = ( sizeDeltaDescriptionText <= 90f ) ? avatarRectTransform : commentRectTransform;

        likeRectTransform.anchoredPosition = new Vector2( likeRectTransform.anchoredPosition.x,
                measuringRectTransform.anchoredPosition.y - measuringRectTransform.sizeDelta.y - offsetPosY );
        timeRectTransform.anchoredPosition = new Vector2( timeRectTransform.anchoredPosition.x,
                measuringRectTransform.anchoredPosition.y - measuringRectTransform.sizeDelta.y - offsetPosY );
        myRectTransform.sizeDelta = new Vector2( myRectTransform.sizeDelta.x,
                measuringRectTransform.anchoredPosition.y + measuringRectTransform.sizeDelta.y + timeRectTransform.sizeDelta.y + offsetPosY );

        likeButton.onClick.AddListener( LikeClick );
        likeUserButton.onClick.AddListener( LikeUserClick );
    }

    void LikeClick() {
        if( likeFill.gameObject.activeSelf ) {
            likeFill.gameObject.SetActive( false );
        }
        else {
            likeFill.gameObject.SetActive( true );
        }
    }

    void LikeUserClick() {
        //показать окно лакнувших
    }

    public class Factory : PlaceholderFactory<Comment> {
    }
}
