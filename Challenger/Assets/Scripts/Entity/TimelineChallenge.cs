﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class TimelineChallenge : ChallengeInfo {
    ChallengeInfoWindow challengeEntreyInfoWindow;
    TimelineDescriptionPostInfo timelineDescriptionPostInfo;

    ChallengeServ challenge;

    [Inject]
    Profile profile;

    // Note that in this case we can't use a constructor
    [Inject]
    public void Construct() {
    }

    internal override void Init( ChallengeServ challenge ) {
        this.challenge = challenge;

        base.Init( challenge );

        challengeEntreyInfoWindow = FindObjectOfType<ChallengeInfoWindow>();

        timelineDescriptionPostInfo = ( TimelineDescriptionPostInfo ) descriptionPostInfo;
        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick += ShowChallengeWindow;
    }

    internal void ShowChallengeWindow() {
        challengeEntreyInfoWindow.StartShow( challenge );
    }

    private void OnDestroy() {
        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick -= ShowChallengeWindow;
    }

    public class Factory: PlaceholderFactory<TimelineChallenge> {
    }
}
