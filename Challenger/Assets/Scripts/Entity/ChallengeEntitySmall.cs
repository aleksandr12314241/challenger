﻿using UnityEngine;
using System;
using UnityEngine.UI;
using Zenject;

public class ChallengeEntitySmall: EntityPanelSmall {
    internal Action<ChallengeEntitySmall> OnClickChallengeEntreySmall = ( ChallengeEntitySmall args ) => { };

    [SerializeField]
    Image frameImage;
    [SerializeField]
    float maxTextDeltaSizeY = 112f;

    [Inject]
    protected SpriteLoader spriteLoader;

    ChallengeInfoWindow challengeEntreyInfoWindow;
    internal ChallengeServ challenge {
        private set;
        get;
    }

    internal virtual void Init( ChallengeServ challenge ) {
        base.Init();

        this.challenge = challenge;
        challengeEntreyInfoWindow = FindObjectOfType<ChallengeInfoWindow>();

        challengeEntreySmallButton.onClick.AddListener( EntreyPanelSmallClick );
        //photoImage.sprite = postChallengeSmallData.postContentData.contentImage;
        string ellipsisString = "...";
        description.text = challenge.description;
        string descriptionString = "";

        if( description.preferredHeight >= maxTextDeltaSizeY ) {
            for( int i = 0; i < challenge.description.Length; i++ ) {
                string tempString =
                    challenge.description.Substring( 0, challenge.description.Length - i * 2 );
                description.text = tempString + ellipsisString;

                if( description.preferredHeight <= maxTextDeltaSizeY ) {
                    descriptionString = tempString + ellipsisString;
                    break;
                }
            }
        }
        else {
            descriptionString = challenge.description;
        }

        description.text = descriptionString;

        frameImage.gameObject.SetActive( false );

        spriteLoader.LoadSpriteContent( challenge.contents[0].filename, ( sprite ) => {
            photoImage.sprite = sprite;
            backPhotoImage.sprite = sprite;
        } );
    }

    protected override void EntreyPanelSmallClick() {
        OnClickChallengeEntreySmall( this );
    }

    internal void ShowChoose() {
        frameImage.gameObject.SetActive( true );
    }

    internal void HideChoose() {
        frameImage.gameObject.SetActive( false );
    }

    internal virtual float GetPosY() {
        return transform.parent.GetComponent<RectTransform>().anchoredPosition.y;
    }

    public class Factory : PlaceholderFactory<ChallengeEntitySmall> {
    }
}
