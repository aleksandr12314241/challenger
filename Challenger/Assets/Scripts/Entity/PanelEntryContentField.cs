﻿using UnityEngine;
using UnityEngine.UI;

public class PanelEntryContentField : MonoBehaviour {
    [SerializeField]
    Image contentImage;

    internal bool IsEnptyContent {
        get {
            return contentImage.sprite == null;
        }
    }

    internal void Init( ContentServ postContentData ) {
        contentImage.sprite = postContentData.photo;
    }

    internal void Init( Sprite photo ) {
        contentImage.sprite = photo;
    }
}
