﻿using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class ExecutionEntitySmall : ExecutionSmallInChallengeInfo {
    [SerializeField]
    Text descriptionChallenge;

    public class Factory : PlaceholderFactory<ExecutionEntitySmall> {
    }
}
