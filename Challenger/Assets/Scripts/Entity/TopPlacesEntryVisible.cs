﻿using UnityEngine;
using UnityEngine.UI;

public class TopPlacesEntryVisible : MonoBehaviour {
    [SerializeField]
    Button prizePhotoButton;

    PrizeWindow prizeWindow;

    void Start() {
        prizePhotoButton.onClick.AddListener( ShowPrizeWindow );

        prizeWindow = FindObjectOfType<PrizeWindow>();
    }

    void ShowPrizeWindow () {
        prizeWindow.StartShow();
    }
}
