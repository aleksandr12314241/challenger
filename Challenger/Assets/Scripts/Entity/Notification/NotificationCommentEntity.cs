﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationCommentEntity : NotificationEntity {
    [SerializeField]
    Image avatarImage;
    [SerializeField]
    Image contentImage;
    [SerializeField]
    Text notificationText;
    [SerializeField]
    Button infoChallengeButton;

    ChallengeInfoWindow challengeInfoWindow;

    NotificationCommentServ notificationCommentData;


    internal override void Init( NotificationServ notificationData ) {
        base.Init( notificationData );

        challengeInfoWindow = FindObjectOfType<ChallengeInfoWindow>();

        infoChallengeButton.onClick.AddListener( InfoChallengeClick );

        NotificationCommentServ notificationCommentData = notificationData as NotificationCommentServ;
        this.notificationCommentData = notificationCommentData;

        avatarImage.sprite = notificationCommentData.avatarImage;
        contentImage.sprite = notificationCommentData.postContentData.contentImage;
        notificationText.text = notificationCommentData.commentText;
    }

    void InfoChallengeClick() {
        //TODO: доделать функционал позже
        //challengeInfoWindow.StartShow( challengeServ );
    }
}
