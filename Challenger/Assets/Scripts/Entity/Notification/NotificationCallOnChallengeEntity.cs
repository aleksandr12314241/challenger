﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class NotificationCallOnChallengeEntity : NotificationEntity {
    [SerializeField]
    Image avatarPhoto;
    [SerializeField]
    Text userNameText;
    [SerializeField]
    Image contentImage;
    [SerializeField]
    JoinButtonPanel joinButtonPanel;
    [SerializeField]
    Timer leftTimer;
    [SerializeField]
    TimelineDescriptionPostInfo timelineDescriptionPostInfo;

    ChallengeInfoWindow challengeInfoWindow;
    CreateExecutionWindow createExecutionWindow;

    NotificationCallOnChallengeServ notificationCallOnChallengeData;

    internal override void Init( NotificationServ notificationData ) {
        base.Init( notificationData );

        timelineDescriptionPostInfo.OnDetailsChallengeButtonClick += InfoChallengeClick;

        notificationCallOnChallengeData = notificationData as NotificationCallOnChallengeServ;

        joinButtonPanel.Init( notificationCallOnChallengeData );
        challengeInfoWindow = FindObjectOfType<ChallengeInfoWindow>();
        createExecutionWindow = FindObjectOfType<CreateExecutionWindow>();

        SetChallengeData();

        avatarPhoto.sprite = notificationCallOnChallengeData.avatarImage;
        userNameText.text = "вас вызвал <color=blue>" + notificationCallOnChallengeData.userName + "</color>";
        DateTime dateTimeEnd =
            DateTime.Now.AddSeconds( notificationCallOnChallengeData.endChallengeTime - notificationCallOnChallengeData.postTime );
        leftTimer.StartTimer( dateTimeEnd );
    }

    void SetChallengeData() {
        contentImage.sprite = notificationCallOnChallengeData.postContentData.contentImage;
        timelineDescriptionPostInfo.SetDescriptionText( notificationCallOnChallengeData.challengeDescription );
    }

    void InfoChallengeClick() {
        //TODO: доделать функционал позже
        //challengeInfoWindow.StartShow( notificationCallOnChallengeData.postId );
    }
}
