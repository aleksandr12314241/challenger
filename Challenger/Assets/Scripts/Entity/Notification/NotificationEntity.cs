﻿using UnityEngine;

public abstract class NotificationEntity : MonoBehaviour {
    internal NotificationServ NotificationData { private set; get; }

    internal virtual void Init( NotificationServ notificationData ) {
        NotificationData = notificationData;
    }
}
