﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NotificationWinChallengeEntity : NotificationEntity {
    [SerializeField]
    Image contentImage;
    [SerializeField]
    Button infoChallengeButton;
    [SerializeField]
    Text notificationWinTittle;
    [SerializeField]
    Text notificationWinText;
    [SerializeField]
    Image crownImage;

    ChallengeInfoWindow challengeInfoWindow;

    NotificationWinChallengeServ notificationWinChallengeData;
    MenuController menuController;

    internal override void Init( NotificationServ notificationData ) {
        base.Init( notificationData );

        challengeInfoWindow = FindObjectOfType<ChallengeInfoWindow>();
        menuController = FindObjectOfType<MenuController>();
        infoChallengeButton.onClick.AddListener( InfoChallengeClick );

        notificationWinChallengeData = notificationData as NotificationWinChallengeServ;

        contentImage.sprite = notificationWinChallengeData.postContentData.contentImage;
        notificationWinTittle.text = "Урааа!!";
        notificationWinText.text = "вы заняли " + notificationWinChallengeData.winPlace + " место";
        crownImage.sprite = menuController.CrownsSprites[notificationWinChallengeData.winPlace - 1];
    }

    void InfoChallengeClick() {
        //TODO: доделать функционал позже
        //challengeInfoWindow.StartShow( notificationWinChallengeData.postId );
    }
}
