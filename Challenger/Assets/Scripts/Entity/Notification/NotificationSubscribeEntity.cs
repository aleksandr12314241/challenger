﻿using UnityEngine;
using UnityEngine.UI;

public class NotificationSubscribeEntity : NotificationEntity {
    [SerializeField]
    Image avatarImage;
    [SerializeField]
    Button infoUserButton;
    [SerializeField]
    Button avaterUserButton;
    [SerializeField]
    Text notificationText;

    NotificationSubscriberServ notificationSubscriberData;

    internal override void Init( NotificationServ notificationData ) {
        base.Init( notificationData );

        NotificationSubscriberServ notificationSubscriberData = notificationData as NotificationSubscriberServ;
        this.notificationSubscriberData = notificationSubscriberData;

        avatarImage.sprite = notificationSubscriberData.avatarImage;
        notificationText.text = "на вас подписался (ась) <color=blue>" + notificationSubscriberData.userName + "</color>";
    }
}
