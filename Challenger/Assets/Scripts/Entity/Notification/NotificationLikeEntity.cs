﻿using UnityEngine;
using UnityEngine.UI;

public class NotificationLikeEntity : NotificationEntity {
    [SerializeField]
    Image avatarImage;
    [SerializeField]
    Image contentImage;
    [SerializeField]
    Button infoChallengeButton;

    ChallengeInfoWindow challengeInfoWindow;

    NotificationLikeServ notificationLikeData;

    internal override void Init( NotificationServ notificationData ) {
        base.Init( notificationData );

        challengeInfoWindow = FindObjectOfType<ChallengeInfoWindow>();
        infoChallengeButton.onClick.AddListener( InfoChallengeClick );

        NotificationLikeServ notificationLikeData = notificationData as NotificationLikeServ;
        this.notificationLikeData = notificationLikeData;

        avatarImage.sprite = notificationLikeData.avatarImage;
        contentImage.sprite = notificationLikeData.postContentData.contentImage;
    }

    void InfoChallengeClick() {
        //TODO: доделать функцтона попозже
        //challengeInfoWindow.StartShow( notificationLikeData.postId );
    }
}
