﻿using UnityEngine;
using UnityEngine.UI;
using System;
using Zenject;

public class ExecutorEntreySmallWin : ExecutionSmallInChallengeInfo {
    public event Action<ExecutorEntreySmallWin> OnEntreyPanelSmallWinClick = ( ExecutorEntreySmallWin args ) => { };

    [SerializeField]
    Image crownImage;

    MenuController menuController;
    int place;
    internal int Place {
        get {
            return place;
        }
    }

    bool isWin = false;
    internal bool IsWin {
        get {
            return isWin;
        }
    }

    private void Start() {
        place = -1;
        isWin = false;

        menuController = FindObjectOfType<MenuController>();

        challengeEntreySmallButton.onClick.AddListener( EntreyPanelSmallClick );
    }

    internal override void Init( ExecutionServ execution ) {
        this.execution = execution;

        base.Init();

        challengeEntreySmallButton.onClick.AddListener( EntreyPanelSmallClick );

        spriteLoader.LoadSpriteContent( execution.contents[0].filename, ( sprite ) => {
            photoImage.sprite = sprite;
            backPhotoImage.sprite = sprite;
        } );

        description.text = execution.description;

        menuController = FindObjectOfType<MenuController>();

        challengeEntreySmallButton.onClick.AddListener( EntreyPanelSmallClick );

        crownImage.sprite = menuController.CrownsSprites[this.execution.tempWinPlace];

        place = -1;
        isWin = false;
    }

    protected override void EntreyPanelSmallClick() {
        isWin = !isWin;
        crownImage.gameObject.SetActive( isWin );

        OnEntreyPanelSmallWinClick( this );
    }

    internal void RemoveCrownType() {
        isWin = false;

        crownImage.gameObject.SetActive( !isWin );
        place = -1;
    }

    internal void SetCrownType() {
        isWin = true;

        crownImage.gameObject.SetActive( isWin );
    }

    public class Factory : PlaceholderFactory<ExecutorEntreySmallWin> {
    }
}
