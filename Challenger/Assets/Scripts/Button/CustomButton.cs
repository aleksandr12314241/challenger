﻿using UnityEngine.EventSystems;
using UnityEngine.UI;
using System;
using UnityEngine;

public class CustomButton : Button {
    public event Action OnPointerDownAction = () => { };
    public event Action OnPointerUpAction = () => { };

    public override void OnPointerDown ( PointerEventData eventData ) {
        base.OnPointerDown( eventData );

        OnPointerDownAction();
    }

    public override void OnPointerUp ( PointerEventData eventData ) {
        base.OnPointerUp( eventData );

        OnPointerUpAction();
    }

    public override void OnPointerClick ( PointerEventData eventData ) {
        base.OnPointerClick( eventData );

        OnPointerUpAction();
    }

    //public override void OnPointerExit ( PointerEventData eventData ) {
    //    base.OnPointerExit( eventData );
    //    OnPointerUpAction();
    //}
}
