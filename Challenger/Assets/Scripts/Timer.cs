﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Timer : MonoBehaviour {
    internal event Action OnEndTimer = () => { };

    [SerializeField]
    string additionalTimeString;
    [SerializeField]
    Text timeText;

    Coroutine timeUpdate;
    DateTime dateTime;
    bool isGo;

    internal string GetTimeText() {
        return timeText.text;
    }

    internal void StartTimer( DateTime dateTime ) {
        if( !gameObject.activeSelf || !gameObject.activeInHierarchy ) {
            return;
        }

        if( timeUpdate != null ) {
            StopCoroutine( timeUpdate );
            timeUpdate = null;
        }

        i_time = 0;
        this.dateTime = dateTime;
        timeUpdate = StartCoroutine( TimeUpdate() );
    }

    internal void StopTimer() {
        if( timeUpdate != null ) {
            StopCoroutine( timeUpdate );
            timeUpdate = null;
        }
    }

    int i_time = 0;
    IEnumerator TimeUpdate() {
        TimeSpan tempTime = dateTime - DateTime.Now;

        string tempTimeString = "";
        if( tempTime.Days != 0 ) {
            tempTimeString += tempTime.Days.ToString();
            tempTimeString += ":" + tempTime.Hours.ToString() + ":";
        }
        else if( tempTime.Hours != 0 ) {
            tempTimeString += ":" + tempTime.Hours.ToString() + ":";
        }
        tempTimeString += tempTime.Minutes.ToString();
        tempTimeString += ":" + tempTime.Seconds.ToString();

        timeText.text = additionalTimeString + " " + tempTimeString;

        for( ; ; ) {
            yield return new WaitForSeconds( 1f );
            //Debug.Log( i_time );
            i_time++;
            tempTime = dateTime - DateTime.Now;

            tempTimeString = "";
            if( tempTime.Days != 0 ) {
                tempTimeString += tempTime.Days.ToString();
                tempTimeString += ":" + tempTime.Hours.ToString() + ":";
            }
            else if( tempTime.Hours != 0 ) {
                tempTimeString += ":" + tempTime.Hours.ToString() + ":";
            }
            //else {
            //}

            tempTimeString += tempTime.Minutes.ToString();
            tempTimeString += ":" + tempTime.Seconds.ToString();

            timeText.text = additionalTimeString + " " + tempTimeString;

            if( tempTime.TotalSeconds <= 0 ) {
                EndTimer();
            }
        }
    }

    void EndTimer() {
        if( timeUpdate != null ) {
            StopCoroutine( timeUpdate );
        }

        OnEndTimer();
    }
}
