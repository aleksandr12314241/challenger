﻿#if UNITY_EDITOR
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.IO;
using System;

public class LoadContentWindow: EditorWindow {
    const string Title = "Load Photo Window";
    [MenuItem( "Tools/LoadContentWindow" )]
    public static void OpenEditorWindow () {
        win = GetWindow<LoadContentWindow>( true );
        win.titleContent.text = Title;
        win.position = new Rect( 100, 100, 712, 512 );

        //win.Close();
    }

    LoadContentWindowDataset loadContentWindowDataset;
    Texture2D[] textures;
    private int indexGridTextureTemp = -1;
    private int indexGridTexture = -1;
    static LoadContentWindow win;

    Transform tr;

    public static Action<string[]> OnColse = ( string[] paths ) => {};

    void OnEnable () {
        loadContentWindowDataset =
            AssetDatabase.LoadAssetAtPath<LoadContentWindowDataset>( "Assets/Plugins/NativeGallery/Dataset/LoadContentWindowDataset.asset" );

        UpdateTexturesList();
    }

    void OnDisable () {
        Debug.Log( "<LoadContentWindow> closed" );

        if ( indexGridTextureTemp >= 0 && indexGridTextureTemp < textures.Length ) {
            string[] pathTemp = new string[1];
            pathTemp[0] = loadContentWindowDataset.GetPathToFolderWithPhoto() + "/" + textures[indexGridTextureTemp].name;

            Debug.Log( "<LoadContentWindow> image choose " + textures[indexGridTextureTemp].name );

            OnColse( pathTemp );
        }
        else {
            OnColse( null );
        }

        OnColse = ( string[] paths ) => { };
        win = null;
    }

    void OnGUI () {
        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField( loadContentWindowDataset.GetPathToFolderWithPhoto() );
        if ( GUILayout.Button( "сменить" ) ) {
            string path = EditorUtility.OpenFolderPanel( "Load png Textures", "", "" );

            if ( path != null && path != "" ) {
                loadContentWindowDataset.SetPathToFolderWithPhoto( path );
                UpdateTexturesList();
            }
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if ( GUILayout.Button( "обновить" ) ) {
            UpdateTexturesList();

        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        if ( GUILayout.Button( "загрузить" ) ) {
            win.Close();
        }
        if ( GUILayout.Button( "закрыть" ) ) {
            win.Close();
        }
        EditorGUILayout.EndHorizontal();

        EditorGUILayout.BeginHorizontal();
        indexGridTextureTemp = GUI.SelectionGrid( new Rect( Screen.width / 60f, Screen.height / 5f, Screen.width / 1.5f, Screen.height / 4f ),
                               indexGridTextureTemp, textures, 5 );
        EditorGUILayout.EndHorizontal();
    }

    void UpdateTexturesList () {
        var info = new DirectoryInfo( loadContentWindowDataset.GetPathToFolderWithPhoto() );
        var fileInfo = info.GetFiles();

        List<string> fileName = new List<string>();
        for ( int i = 0; i < fileInfo.Length; i++ ) {
            FileInfo file = fileInfo[i];

            int indexStart = file.Name.Length - 3;
            string fileExpand = file.Name.Substring( indexStart ).ToLower();
            if ( fileExpand.Equals( "png" ) || fileExpand.Equals( "jpg" ) ) {
                fileName.Add( file.Name );
            }
        }

        textures = new Texture2D[fileName.Count];
        for ( int i = 0; i < fileName.Count; i++ ) {
            byte[] fileData = File.ReadAllBytes( loadContentWindowDataset.GetPathToFolderWithPhoto() + "/" + fileName[i] );
            textures[i] = new Texture2D( 2, 2 );
            textures[i].LoadImage( fileData ); //..this will auto-resize the texture dimensions.
            textures[i].name = fileName[i];
        }
    }
}
#endif
