﻿#if UNITY_EDITOR
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu( fileName = "LoadContentWindowDataset", menuName = "Challenger/LoadContentWindowDataset", order = 1 )]
public class LoadContentWindowDataset: ScriptableObject {
    [SerializeField]
    private string pathToFolderWithPhoto;

    public void SetPathToFolderWithPhoto ( string pathToFolderWithPhoto ) {
        this.pathToFolderWithPhoto = pathToFolderWithPhoto;

    }
    public string GetPathToFolderWithPhoto (  ) {
        return pathToFolderWithPhoto;
    }
}
#endif