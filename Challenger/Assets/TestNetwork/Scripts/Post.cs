﻿using System;

[Serializable]
public class Post {
    public long id;
    public string text;
    public Content[] contents;

    public long GetId () {
        return id;
    }

    public string GetText () {
        return text;
    }

    public void SetId ( long id ) {
        this.id = id;
    }

    public void SetText ( string text ) {
        this.text = text;
    }

    public Content[] getContents () {
        return contents;
    }

    public void setContents ( Content[] contents ) {
        this.contents = contents;
    }
}
