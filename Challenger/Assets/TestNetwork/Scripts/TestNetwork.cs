﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
using System.Text;
using System;
using System.IO;
using System.Threading.Tasks;

public class TestNetwork : MonoBehaviour {
    [SerializeField]
    Button getAllButton;
    [SerializeField]
    Button getOneButton;
    [SerializeField]
    Button postButton;
    [SerializeField]
    Button putButton;
    [SerializeField]
    Button deleteButton;
    [SerializeField]
    Button loadButton;
    [SerializeField]
    Text outputText;
    [SerializeField]
    Image img;
    [SerializeField]
    RectTransform parentImages;

    [SerializeField]
    Button keyboardButton;
    [SerializeField]
    Button sendButton;
    [SerializeField]
    InputField inputFieldText;

    [SerializeField]
    Button showLogButton;
    [SerializeField]
    Button closeLogButton;
    [SerializeField]
    GameObject logPanel;

    List<Post> posts = new List<Post>();

    string uploadPath;
    TouchScreenKeyboard keyboard;

    // Start is called before the first frame update
    void Start() {
        outputText.text = "";
        //getAllButton.onClick.AddListener( TestGetAll );
        //getOneButton.onClick.AddListener( TestGetOne );
        //postButton.onClick.AddListener( TestPostImage );
        //putButton.onClick.AddListener( TestPut );
        //deleteButton.onClick.AddListener( TestDelete );
        //loadButton.onClick.AddListener( TestLoad );
        //
        //showLogButton.onClick.AddListener( TestShowLog );
        //closeLogButton.onClick.AddListener( TestCloseLog );
        keyboardButton.onClick.AddListener( OpenKeyboard );
        sendButton.onClick.AddListener( SendComment );

        inputFieldText.onEndEdit.AddListener( SendComment );

        AddTextToLog( "start program" );

        //ConfigDataset configDataset = Resources.Load<ConfigDataset>( "Configs/ConfigDataset-dev-local" ) as ConfigDataset;

        //NetworkConfigDataset networkConfigDataset = Resources.Load<NetworkConfigDataset>( "Configs/NetworkConfigDataset" ) as NetworkConfigDataset;

        //uploadPath = networkConfigDataset.GetNetworkConfig().GetUploadPath();
    }

    void OpenKeyboard() {
        keyboard = TouchScreenKeyboard.Open( "", TouchScreenKeyboardType.Default, false, false, false, true );
    }

    void Update() {
        if( keyboard != null && keyboard.status == TouchScreenKeyboard.Status.Done ) {
            string str = keyboard.text;
            AddTextToLog( str );
            keyboard = null;
        }
    }

    void SendComment( string str ) {
        if( inputFieldText.text == "" ) {
            return;
        }

        AddTextToLog( str );
        inputFieldText.text = "";
    }

    void SendComment() {
        if( inputFieldText.text == "" ) {
            return;
        }

        string str = inputFieldText.text;
        AddTextToLog( str );
        inputFieldText.text = "";
    }

    private void TestShowLog() {
        logPanel.SetActive( true );
    }

    private void TestCloseLog() {
        logPanel.SetActive( false );
    }

    void AddTextToLogNoEnter( string togText ) {
        outputText.text += togText;
    }

    void AddTextToLog( string togText ) {
        outputText.text += togText + " \n";
    }

    void TestGetAll() {
        AddTextToLog( "send get all" );

        StartCoroutine( GetAllRequest( "http://localhost:8080/message" ) );
    }

    IEnumerator GetAllRequest( string url ) {
        AddTextToLog( url );
        UnityWebRequest uwr = UnityWebRequest.Get( url );

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.Log( "Error While Sending: " + uwr.error );
        }
        else {
            Debug.Log( "Received: " + uwr.downloadHandler.text );
            AddTextToLog( uwr.downloadHandler.text );

            posts = JsonUtility.FromJson<List<Post>>( uwr.downloadHandler.text );
            Debug.Log( posts.Count );

            for( int i = 0; i < posts.Count; i++ ) {
                Debug.Log( posts[i].GetId() + "   =   " + posts[i].GetText() );

                CreatePost( posts[i] );
            }
        }
    }

    private void TestGetOne() {
        AddTextToLog( "send get one" );

        StartCoroutine( GetOneRequest( "http://localhost:8080/message/0" ) );
    }

    IEnumerator GetOneRequest( string url ) {
        AddTextToLog( url );
        UnityWebRequest uwr = UnityWebRequest.Get( url );

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.Log( "Error While Sending: " + uwr.error );
        }
        else {
            Debug.Log( "Received: " + uwr.downloadHandler.text );
            AddTextToLog( uwr.downloadHandler.text );

            Post post = JsonUtility.FromJson<Post>( uwr.downloadHandler.text );

            Debug.Log( post.GetId() + "  ,  " + post.GetText() );

            if( posts.Find( item => item.GetId() != post.GetId() ) == null ) {
                posts.Add( post );
            }

            Debug.Log( posts.Count );

            for( int i = 0; i < posts.Count; i++ ) {
                Debug.Log( posts[i].GetId() + "   =   " + posts[i].GetText() );
            }
        }
    }

    void TestPost() {
        AddTextToLog( "send post" );

        StartCoroutine( PostRequest( "http://localhost:8080/message" ) );
    }

    IEnumerator PostRequest( string url ) {
        AddTextToLog( url );
        Dictionary<string, string> message =  new Dictionary<string, string>() {
            { "id", "3" },
            { "text", "Third message" }
        };

        Post post = new Post() {
            id = 4,
            text = "4 hello"
        };

        string json = JsonUtility.ToJson( post );

        var uwr = new UnityWebRequest( url, "POST" );
        byte[] bodyRaw = Encoding.UTF8.GetBytes( json );
        uwr.uploadHandler = ( UploadHandler ) new UploadHandlerRaw( bodyRaw );
        uwr.downloadHandler = ( DownloadHandler ) new DownloadHandlerBuffer();
        uwr.SetRequestHeader( "Content-Type", "application/json" );

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.Log( "Error While Sending: " + uwr.error );
        }
        else {
            Debug.Log( "Received: " + uwr.downloadHandler.text );
            AddTextToLog( uwr.downloadHandler.text );

            Post mesasge = JsonUtility.FromJson<Post>( uwr.downloadHandler.text );
            posts.Add( mesasge );

            Debug.Log( posts.Count );
        }
    }

    void TestPostImage() {
        AddTextToLog( "send post image" );

        StartCoroutine( PostRequestImage( "http://localhost:8080/message" ) );
    }

    IEnumerator PostRequestImage( string url ) {
        AddTextToLog( url );

        yield return new WaitForEndOfFrame();

        // Create a texture the size of the screen, RGB24 format
        int width = Screen.width;
        int height = Screen.height;
        Texture2D tex = new Texture2D( width, height, TextureFormat.RGB24, false );
        Debug.Log( tex );
        Debug.Log( tex.isReadable );

        Debug.Log( pathToImage );

        Texture2D texture = NativeGallery.LoadImageAtPath( pathToImage, markTextureNonReadable: false );

        Debug.Log( texture );
        Debug.Log( texture.isReadable );
        Debug.Log( texture.name );

        img.sprite = Sprite.Create( texture, new Rect( 0, 0, texture.width, texture.height ), Vector2.zero );

        byte[] bytes = texture.EncodeToJPG();

        Rect rec = new Rect( 0, 0, texture.width, texture.height );

        WWWForm form = new WWWForm();
        form.AddField( "text", RandomString( 10 ) );
        form.AddBinaryData( "file", bytes );

        var uwr = UnityWebRequest.Post( url, form );

        //Post post = new Post() {
        //    id = 4,
        //    text = "4 hello"
        //};
        //string json = JsonConvert.SerializeObject( post );
        //byte[] data = System.Text.Encoding.UTF8.GetBytes( json );

        //UploadHandlerRaw upHandler = new UploadHandlerRaw( data );
        //upHandler.contentType = "application/json";
        //uwr.uploadHandler = upHandler;

        //List<IMultipartFormSection> formData = new List<IMultipartFormSection>();
        //MultipartFormDataSection a = new MultipartFormDataSection( data );
        //a.contentType  = "application/json";

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.Log( "Error While Sending: " + uwr.error );
        }
        else {
            Debug.Log( "Received: " + uwr.downloadHandler.text );
        }
    }

    private void TestPut() {
        AddTextToLog( "send put" );

        StartCoroutine( PutRequest( "http://localhost:8080/message/0" ) );
    }

    IEnumerator PutRequest( string url ) {
        AddTextToLog( url );

        Post post = new Post() {
            id = 4,
            text = "4 hello"
        };

        string json = JsonUtility.ToJson( post );

        var uwr = new UnityWebRequest( url, "PUT" );
        byte[] bodyRaw = Encoding.UTF8.GetBytes( json );
        uwr.uploadHandler = ( UploadHandler ) new UploadHandlerRaw( bodyRaw );
        //uwr.uploadedBytes
        uwr.downloadHandler = ( DownloadHandler ) new DownloadHandlerBuffer();
        uwr.SetRequestHeader( "Content-Type", "application/json" );

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.Log( "Error While Sending: " + uwr.error );
        }
        else {
            Debug.Log( "Received: " + uwr.downloadHandler.text );
            AddTextToLog( uwr.downloadHandler.text );

            Post mesasge = JsonUtility.FromJson<Post>( uwr.downloadHandler.text );
            posts.Add( mesasge );

            Debug.Log( posts.Count );
        }
    }

    private void TestDelete() {
        AddTextToLog( "send delete" );

        StartCoroutine( DeleteRequest( "http://localhost:8080/message/", 0 ) );
    }

    IEnumerator DeleteRequest( string url, long id ) {
        string uri = url + id;
        AddTextToLog( uri );
        UnityWebRequest uwr = UnityWebRequest.Delete( uri );

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.Log( "Error While Sending: " + uwr.error );
        }
        else {
            Debug.Log( "Received: " );
            Debug.Log( uwr.downloadHandler );

            for( int i = 0; i < posts.Count; i++ ) {
                Debug.Log( posts[i].GetId() + "   =   " + posts[i].GetText() );
            }

            int index = posts.FindIndex( item => item.id == id );
            Debug.Log( index );

            if( index > -1 ) {
                posts.RemoveAt( index );
            }

            Debug.Log( posts.Count );

            for( int i = 0; i < posts.Count; i++ ) {
                Debug.Log( posts[i].GetId() + "   =   " + posts[i].GetText() );
            }
        }
    }

    string pathToImage = null;
    Image _imgTemp_;

    private async void TestLoad() {

        pathToImage = null;
        //NativeGallery.GetImagesFromGallery( ( pathsT ) => {
        //    if ( pathsT != null ) {
        //        for ( int i = 0; i < parentImages.childCount; i++ ) {
        //            Destroy( parentImages.GetChild( i ) );
        //        }
        //        for ( int i = 0; i < pathsT.Length; i++ ) {
        //            AddTextToLog( pathsT[i] );
        //            Texture2D texture = NativeGallery.LoadImageAtPath( pathsT[i] );
        //            Image imgTemp = Instantiate<Image>( img );
        //            imgTemp.sprite = Sprite.Create( texture, new Rect( 0, 0, texture.width, texture.height ), Vector2.zero );
        //            imgTemp.transform.SetParent( parentImages, false );
        //        }
        //    }
        //    else {
        //        AddTextToLog( "!!! non choose file name !!!" );
        //    }
        //}  );

        NativeGallery.GetImageFromGallery( ( path ) => {
            if( path != null ) {

                for( int i = 0; i < parentImages.childCount; i++ ) {
                    Destroy( parentImages.GetChild( i ) );
                }

                AddTextToLog( path );

                Texture2D texture = NativeGallery.LoadImageAtPath( path );

                Debug.Log( texture );
                Debug.Log( texture.name );

                Image imgTemp = Instantiate<Image>( img );

                imgTemp.sprite = Sprite.Create( texture, new Rect( 0, 0, texture.width, texture.height ), Vector2.zero );
                imgTemp.transform.SetParent( parentImages, false );

                pathToImage = path;

                Debug.Log( pathToImage );
            }
            else {
                AddTextToLog( "!!! non choose file name !!!" );
            }
        } );

        return;

    }

    async private void LoadImage( string filePath, Image postImage ) {

        //AddTextToLog( "LoadImage  " + filePath );
        //
        //Texture2D tex = await LoadAsyncTexture2d.GetRemoteTexture( filePath );
        //Rect rec = new Rect( 0, 0, tex.width, tex.height );
        //postImage.sprite = Sprite.Create( tex, new Rect( 0, 0, tex.width, tex.height ), Vector2.zero );
    }

    Image CreatePost( Post post ) {
        Debug.Log( post );
        Debug.Log( post.getContents() );

        if( post.getContents()[0] == null ) {

            return null;
        }

        string resultFilename = uploadPath + "/" + post.getContents()[0].getFilename();
        Debug.Log( resultFilename );

        Texture2D texture = NativeGallery.LoadImageAtPath( resultFilename );
        Image imgTemp = Instantiate<Image>( img );

        imgTemp.sprite = Sprite.Create( texture, new Rect( 0, 0, texture.width, texture.height ), Vector2.zero );
        imgTemp.transform.SetParent( parentImages, false );

        return imgTemp;
    }

    public string RandomString( int size ) {
        StringBuilder builder = new StringBuilder();
        System.Random random = new System.Random();
        char ch;
        for( int i = 0; i < size; i++ ) {
            ch = Convert.ToChar( Convert.ToInt32( Math.Floor( 26 * random.NextDouble() + 65 ) ) );
            builder.Append( ch );
        }

        return builder.ToString();
    }
}
