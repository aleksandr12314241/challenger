﻿using UnityEngine;
using System;
using System.Collections;
using UnityEngine.Networking;
using System.Text;

public class NetworkController : MonoBehaviour {
    NetworkConfigDataset.NetworkConfig networkConfig;

    string token = "";

    private void Awake() {
        Init();
    }

    public void SetToken( string token ) {
        this.token = token;
    }

    private string GetServerToken() {
        return "Bearer_" + token;
    }

    private void Init() {
        NetworkConfigDataset networkConfigDataset = Resources.Load<NetworkConfigDataset>( "Configs/NetworkConfigDataset" ) as NetworkConfigDataset;
        networkConfig = networkConfigDataset.GetNetworkConfig();

    }

    internal string GetFullFilename( string filename ) {
        return networkConfig.GetUploadPath() + filename;
    }

    string GetUrl( string requestMapping ) {
        Debug.Log( networkConfig.GetUrl() + requestMapping );

        return networkConfig.GetUrl() + requestMapping;
    }

    //------- get -------
    public Coroutine Get( string requestMapping, Action<string> action = null, Action<ErrorArgs> actionError = null ) {
        return StartCoroutine( GetRequest( requestMapping, action, actionError ) );
    }

    IEnumerator GetRequest( string requestMapping, Action<string> action, Action<ErrorArgs> actionError ) {
        UnityWebRequest uwr = UnityWebRequest.Get( GetUrl( requestMapping ) );

        Debug.Log( GetUrl( requestMapping ) );
        Debug.Log( token );

        if( token != "" ) {
            uwr.SetRequestHeader( "Authorization", GetServerToken() );
        }

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.LogError( "Error While Sending Get: " + uwr.error );
        }
        else {
            Debug.Log( "Get Received: " + uwr.downloadHandler.text );
            ErrorArgs error = null;
            error = JsonUtility.FromJson<ErrorArgs>( uwr.downloadHandler.text );

            if( error.error != null && error.message != null ) {
                Debug.Log( "error Received: " + uwr.downloadHandler.text );
                if( actionError != null ) {
                    actionError( error );
                }
            }
            else {
                action( uwr.downloadHandler.text );
            }
        }
    }

    //------- post -------
    public Coroutine Post<T> ( string requestMapping, T arg, Action<string> action = null, Action<ErrorArgs> actionError = null ) {
        string json = JsonUtility.ToJson( arg );

        return StartCoroutine( PostRequest( requestMapping, json, action, actionError ) );
    }

    IEnumerator PostRequest( string requestMapping, string json, Action<string> action, Action<ErrorArgs> actionError = null ) {

        var uwr = new UnityWebRequest( GetUrl( requestMapping ), "POST" );
        byte[] bodyRaw = Encoding.UTF8.GetBytes( json );
        uwr.uploadHandler = ( UploadHandler ) new UploadHandlerRaw( bodyRaw );
        uwr.downloadHandler = ( DownloadHandler ) new DownloadHandlerBuffer();
        uwr.SetRequestHeader( "Content-Type", "application/json" );
        if( token != "" ) {
            uwr.SetRequestHeader( "Authorization", GetServerToken() );
        }

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.LogError( "Error While Sending Post Json: " + uwr.error );
        }
        else {
            Debug.Log( "Post Json Received: " + uwr.downloadHandler.text );

            ErrorArgs error = JsonUtility.FromJson<ErrorArgs>( uwr.downloadHandler.text );

            if( error.error != null && error.message != null ) {
                Debug.Log( "error Received: " + uwr.downloadHandler.text );
                actionError( error );
            }
            else {
                action( uwr.downloadHandler.text );
            }
        }
    }

    public Coroutine Post( string requestMapping, WWWForm form, Action<string> action = null, Action<ErrorArgs> actionError = null ) {
        return StartCoroutine( PostRequest( requestMapping, form, action, actionError ) );
    }

    IEnumerator PostRequest( string requestMapping, WWWForm form, Action<string> action, Action<ErrorArgs> actionError = null ) {

        var uwr = UnityWebRequest.Post( GetUrl( requestMapping ), form );

        if( token != "" ) {
            uwr.SetRequestHeader( "Authorization", GetServerToken() );
        }

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.LogError( "Error While Sending Post WWWForm: " + uwr.error );
        }
        else {
            Debug.Log( "Post WWWForm Received: " + uwr.downloadHandler.text );

            ErrorArgs error = JsonUtility.FromJson<ErrorArgs>( uwr.downloadHandler.text );
            if( error.error != null && error.message != null ) {
                Debug.Log( "error Received: " + uwr.downloadHandler.text );
                actionError( error );
            }
            else {
                action( uwr.downloadHandler.text );
            }
        }
    }

    //------- put -------
    public Coroutine Put<T> ( string requestMapping, T arg, Action<string> action = null, Action<ErrorArgs> actionError = null ) {
        string json = JsonUtility.ToJson( arg );

        return StartCoroutine( PutRequest( requestMapping, json, action, actionError ) );
    }

    IEnumerator PutRequest( string requestMapping, string json, Action<string> action, Action<ErrorArgs> actionError = null ) {

        var uwr = new UnityWebRequest( GetUrl( requestMapping ), "PUT" );
        byte[] bodyRaw = Encoding.UTF8.GetBytes( json );
        uwr.uploadHandler = ( UploadHandler ) new UploadHandlerRaw( bodyRaw );
        uwr.downloadHandler = ( DownloadHandler ) new DownloadHandlerBuffer();
        uwr.SetRequestHeader( "Content-Type", "application/json" );

        if( token != "" ) {
            uwr.SetRequestHeader( "Authorization", GetServerToken() );
        }

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.LogError( "Error While Sending Put Json: " + uwr.error );
        }
        else {
            Debug.Log( "Put Json Received: " + uwr.downloadHandler.text );

            ErrorArgs error = JsonUtility.FromJson<ErrorArgs>( uwr.downloadHandler.text );
            if( error.error != null && error.message != null ) {
                Debug.Log( "error Received: " + uwr.downloadHandler.text );
                actionError( error );
            }
            else {
                action( uwr.downloadHandler.text );
            }
        }
    }

    //public Coroutine Put( string requestMapping, WWWForm form, Action<string> action = null ) {
    //    return StartCoroutine( PutRequest( requestMapping, form, action ) );
    //}

    //IEnumerator PutRequest( string requestMapping, WWWForm form, Action<string> action ) {

    //    var uwr = UnityWebRequest.Put( GetUrl( requestMapping ), form );

    //    yield return uwr.SendWebRequest();

    //    if( uwr.isNetworkError ) {
    //        Debug.LogError( "Error While Sending Post WWWForm: " + uwr.error );
    //    }
    //    else {
    //        Debug.Log( "Post WWWForm Received: " + uwr.downloadHandler.text );

    //        action( uwr.downloadHandler.text );
    //    }
    //}

    //------- delete -------
    public Coroutine Delete( string requestMapping, Action<string> action = null, Action<ErrorArgs> actionError = null ) {

        return StartCoroutine( DeleteRequest( requestMapping, action, actionError ) );
    }

    IEnumerator DeleteRequest( string requestMapping, Action<string> action, Action<ErrorArgs> actionError ) {
        UnityWebRequest uwr = UnityWebRequest.Delete( GetUrl( requestMapping ) );

        if( token != "" ) {
            uwr.SetRequestHeader( "Authorization", GetServerToken() );
        }

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError ) {
            Debug.LogError( "Error While Sending Delete: " + uwr.error );
        }
        else {

            Debug.Log( "Delete Received: " + uwr.downloadHandler.text );

            ErrorArgs error = JsonUtility.FromJson<ErrorArgs>( uwr.downloadHandler.text );
            if( error.error != null && error.message != null ) {
                Debug.Log( "error Received: " + uwr.downloadHandler.text );
                actionError( error );
            }
            else {
                action( uwr.downloadHandler.text );
            }
        }
    }

    //------- texture -------
    public Coroutine GetTexture( string url, Action<Texture2D> action = null, Action<ErrorArgs> actionError = null ) {
        return StartCoroutine( GetTextureRequest( url, action, actionError ) );
    }

    IEnumerator GetTextureRequest( string requestMapping, Action<Texture2D> action, Action<ErrorArgs> actionError = null ) {

        UnityWebRequest uwr = UnityWebRequestTexture.GetTexture( requestMapping );

        if( token != "" ) {
            uwr.SetRequestHeader( "Authorization", GetServerToken() );
        }

        yield return uwr.SendWebRequest();

        if( uwr.isNetworkError || uwr.isHttpError ) {
            Debug.LogError( "Error Texture Received " + requestMapping );
        }
        else {
            ErrorArgs error = JsonUtility.FromJson<ErrorArgs>( uwr.downloadHandler.text );
            if( error.error != null && error.message != null ) {
                Debug.Log( "error Received: " + uwr.downloadHandler.text );
                actionError( error );
            }
            else {
                Debug.Log( "Texture Received" );

                Texture2D myTexture = ( ( DownloadHandlerTexture ) uwr.downloadHandler ).texture;

                Texture2D newTex = new Texture2D( myTexture.width, myTexture.height, myTexture.format, true );
                newTex.SetPixels( myTexture.GetPixels() );
                newTex.Apply();

                action( newTex );
            }
        }
    }
}
