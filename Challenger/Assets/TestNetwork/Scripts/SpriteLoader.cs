﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using System.Threading.Tasks;
using UnityEngine.Networking;
using System;

public class SpriteLoader {
    [Inject]
    NetworkController networkController;
    [Inject]
    CommonSpritesDataSet commonSpritesDataSet = null;

    private Dictionary<string, Sprite> sprites = new Dictionary<string, Sprite>();

    internal void LoadSpriteContent( string filePath, Action<Sprite> action ) {
        LoadSpriteContentAsync( filePath, action );
    }

    async void LoadSpriteContentAsync( string filePath, Action<Sprite> action ) {
        if( sprites.ContainsKey( filePath ) ) {
            action( sprites[filePath] );
            return;
        }

        Texture2D tex = await GetRemoteTexture( networkController.GetFullFilename( filePath ) );

        if( tex == null ) {
            action( null );
            return;
        }
        Sprite photoTemp = Sprite.Create( tex, new Rect( 0, 0, tex.width, tex.height ), Vector2.zero );
        if( !sprites.ContainsKey( filePath ) ) {
            sprites.Add( filePath, photoTemp );
        }

        action( photoTemp );
    }

    internal void LoadSpriteAvatar( string filePath, Action<Sprite> action ) {
        LoadSpriteAvatarAsync( filePath, action );
    }
    async void LoadSpriteAvatarAsync( string filePath, Action<Sprite> action ) {
        if( sprites.ContainsKey( filePath ) ) {
            action( sprites[filePath] );
            return;
        }

        Texture2D tex = await GetRemoteTexture( networkController.GetFullFilename( filePath ) );

        if( tex == null ) {
            action( commonSpritesDataSet.GetAvatarSprite() );
            return;
        }
        Sprite photoTemp = Sprite.Create( tex, new Rect( 0, 0, tex.width, tex.height ), Vector2.zero );
        if( sprites.ContainsKey( filePath ) ) {
            action( sprites[filePath] );
            return;
        }
        sprites.Add( filePath, photoTemp );

        action( photoTemp );
    }

    public async Task<Texture2D> GetRemoteTexture( string url ) {

        using( UnityWebRequest www = UnityWebRequestTexture.GetTexture( url ) ) {
            //begin requenst:
            var asyncOp = www.SendWebRequest();

            //await until it's done:
            while( asyncOp.isDone == false ) {
                await Task.Delay( 1000 / 30 ); //30 hertz
            }

            //read results:
            if( www.isNetworkError || www.isHttpError ) {
                //log error:
#if DEBUG
                Debug.Log( $"{ www.error }, URL:{ www.url }" );
#endif

                //nothing to return on error:
                return null;
            }
            else {
                //return valid results:
                return DownloadHandlerTexture.GetContent( www );
            }
        }

    }

    internal void AddLocalSprite( string filePath, Sprite photo ) {
        if( !sprites.ContainsKey( filePath ) ) {
            sprites.Add( filePath, photo );
        }
    }
    //async Task<Sprite> GetRemoteTextureContent( string filePath ) {
    //    Texture2D tex = await LoadAsyncTexture2d.GetRemoteTexture( networkController.GetFullFilename( filePath ) );
    //    return Sprite.Create( tex, new Rect( 0, 0, tex.width, tex.height ), Vector2.zero );
    //}
}
